<?php

return [
    'urls' => [
        'creator_algorithm_url' => env('CREATOR_ALGORITHM_URL'),
        'creator_health_facility_url' => env('CREATOR_HEALTH_FACILITY_URL'),
        'creator_patient_url' => env('CREATOR_PATIENT_URL'),
    ],

    'global' => [
        'study_id' =>env('STUDY_ID'),
        'language' =>env('JSON_LANGUAGE'),
        'ip' => env('MEDAL_DATA_IP'),
    ],

];
