<?php

return [
    'liwi_local_data_url' => 'https://api.bitbucket.org/2.0/repositories/wavemind_swiss/liwi-local-data/commits/',
    'fetch_limit' => '?limit=1',
    'commit_file' => 'all_commits.json',
    'file_number_to_keep' => 5,
];
