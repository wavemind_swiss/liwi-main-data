<?php

return [
    'hide_str' => '###',
    'public_extract_name_flat' => 'export_flat',
    'public_extract_name_separated' => 'export_separated',
    'public_full_extract_name_separated' => 'full_export_separated',
    'patient_discarded_names' => ['test'],
    'header_custom_drug' => 'custom_drugs',
    'header_custom_diagnoses' => 'custom_diagnoses',

    'file_names' => [
        'patients' => 'patients.csv',
        'medical_cases' => 'medical_cases.csv',
        'medical_case_answers' => 'medical_case_answers.csv',
        'nodes' => 'nodes.csv',
        'versions' => 'versions.csv',
        'algorithms' => 'algorithms.csv',
        'activities' => 'activities.csv',
        'diagnoses' => 'diagnoses.csv',
        'custom_diagnoses' => 'custom_diagnoses.csv',
        'diagnosis_references' => 'diagnosis_references.csv',
        'drugs' => 'drugs.csv',
        'custom_drugs' => 'custom_drugs.csv',
        'drug_references' => 'drug_references.csv',
        'managements' => 'managements.csv',
        'management_references' => 'management_references.csv',
        'answer_types' => 'answer_types.csv',
        'formulations' => 'formulations.csv',
        'answers' => 'answers.csv',
        'health_facilities' => 'health_facilities.csv',
        'devices' => 'devices.csv',
    ],

    'folder_separated' => 'separated/',

    'identifiers' => [
        'patient' => [
            'dyn_pat_study_id_patient' => 'id',
            'dyn_pat_first_name' => 'first_name',
            'dyn_pat_middle_name' => 'middle_name',
            'dyn_pat_last_name' => 'last_name',
            'dyn_pat_created_at' => 'created_at',
            'dyn_pat_updated_at' => 'updated_at',
            'dyn_pat_birth_date' => 'birthdate_old',
            'dyn_pat_cor_birth_date' => 'corrected_birthdate',
            'dyn_pat_gender' => 'gender',
            'dyn_pat_local_patient_id' => 'local_patient_id',
            'dyn_pat_consent' => 'consent',
            'dyn_pat_redcap' => 'redcap',
            'dyn_pat_duplicate' => 'duplicate',
            'dyn_pat_other_uid' => 'other_uid',
            'dyn_pat_other_study_id' => 'other_study_id',
            'dyn_pat_other_group_id' => 'other_group_id',
            'dyn_pat_merged_with' => 'merged_with',
            'dyn_pat_merged' => 'merged',
            'dyn_pat_is_drop' => 'is_drop',
            'dyn_pat_status' => 'status',
            'dyn_pat_related_ids' => 'related_ids',
            'dyn_pat_other_id' => 'other_id',
        ],

        'medical_case' => [
            'dyn_mc_id' => 'id',
            'dyn_mc_version_id' => 'version_id',
            'dyn_mc_patient_id' => 'patient_id',
            'dyn_mc_local_medical_case_id' => 'local_medical_case_id',
            'dyn_mc_consent' => 'consent',
            'dyn_mc_isEligible' => 'isEligible',
            'dyn_mc_group_id' => 'group_id',
            'dyn_mc_redcap' => 'redcap',
            'dyn_mc_consultation_date' => 'consultation_date',
            'dyn_mc_birthdate' => 'birthdate',
            'dyn_mc_closedAt' => 'closedAt',
            'dyn_mc_force_close' => 'force_close',
            'dyn_mc_redcap_flag' => 'mc_redcap_flag',
            'dyn_mc_is_drop' => 'is_drop',
            'dyn_mc_duplicate' => 'duplicate',
            'dyn_mc_app_version' => 'app_version',
            'dyn_mc_git_version' => 'git_version',
            'dyn_mc_corrected_bd5ageindays' => 'corrected_bd5ageindays',
            'dyn_mc_blankcase' => 'blankcase',
        ],

        'medical_case_answer' => [
            'dyn_mca_id' => 'id',
            'dyn_mca_medical_case_id' => 'medical_case_id',
            'dyn_mca_answer_id' => 'answer_id',
            'dyn_mca_node_id' => 'node_id',
            'dyn_mca_value' => 'value',
            'dyn_mca_created_at' => 'created_at',
            'dyn_mca_updated_at' => 'updated_at',
        ],

        'node' => [
            'dyn_nod_id' => 'id',
            'dyn_nod_medal_c_id' => 'medal_c_id',
            'dyn_nod_reference' => 'reference',
            'dyn_nod_label' => 'label',
            'dyn_nod_type' => 'type',
            'dyn_nod_category' => 'category',
            'dyn_nod_priority' => 'priority',
            'dyn_nod_stage' => 'stage',
            'dyn_nod_description' => 'description',
            'dyn_nod_formula' => 'formula',
            'dyn_nod_answer_type_id' => 'answer_type_id',
            'dyn_nod_algorithm_id' => 'algorithm_id',
            'dyn_nod_created_at' => 'created_at',
            'dyn_nod_updated_at' => 'updated_at',
            'dyn_nod_is_identifiable' => 'is_identifiable',
            'dyn_nod_display_format' => 'display_format',
        ],

        'version' => [
            'dyn_ver_id' => 'id',
            'dyn_ver_medal_c_id' => 'medal_c_id',
            'dyn_ver_name' => 'name',
            'dyn_ver_algorithm_id' => 'algorithm_id',
            'dyn_ver_created_at' => 'created_at',
            'dyn_ver_updated_at' => 'updated_at',
            'dyn_ver_consent_management' => 'consent_management',
            'dyn_ver_study' => 'study',
            'dyn_ver_is_arm_control' => 'is_arm_control',
        ],

        'algorithm' => [
            'dyn_alg_id' => 'id',
            'dyn_alg_medal_c_id' => 'medal_c_id',
            'dyn_alg_name' => 'name',
            'dyn_alg_created_at' => 'created_at',
            'dyn_alg_updated_at' => 'updated_at',
        ],

        'activity' => [
            'dyn_act_id' => 'id',
            'dyn_act_medical_case_id' => 'medical_case_id',
            'dyn_act_medal_c_id' => 'medal_c_id',
            'dyn_act_step' => 'step',
            'dyn_act_clinician' => 'clinician',
            'dyn_act_mac_address' => 'mac_address',
            'dyn_act_created_at' => 'created_at',
            'dyn_act_updated_at' => 'updated_at',
        ],

        'diagnosis' => [
            'dyn_dia_id' => 'id',
            'dyn_dia_medal_c_id' => 'medal_c_id',
            'dyn_dia_label' => 'label',
            'dyn_dia_diagnostic_id' => 'diagnostic_id',
            'dyn_dia_created_at' => 'created_at',
            'dyn_dia_updated_at' => 'updated_at',
            'dyn_dia_type' => 'type',
            'dyn_dia_version_id' => 'version_id',
        ],

        'custom_diagnosis' => [
            'dyn_cdi_id' => 'id',
            'dyn_cdi_label' => 'label',
            'dyn_cdi_drugs' => 'drugs',
            'dyn_cdi_created_at' => 'created_at',
            'dyn_cdi_updated_at' => 'updated_at',
            'dyn_cdi_medical_case_id' => 'medical_case_id',
        ],

        'diagnosis_reference' => [
            'dyn_dre_id' => 'id',
            'dyn_dre_status' => 'status',
            'dyn_dre_diagnosis_id' => 'diagnosis_id',
            'dyn_dre_medical_case_id' => 'medical_case_id',
        ],

        'drug' => [
            'dyn_dru_id' => 'id',
            'dyn_dru_medal_c_id' => 'medal_c_id',
            'dyn_dru_type' => 'type',
            'dyn_dru_label' => 'label',
            'dyn_dru_description' => 'description',
            'dyn_dru_diagnosis_id' => 'diagnosis_id',
            'dyn_dru_created_at' => 'created_at',
            'dyn_dru_updated_at' => 'updated_at',
            'dyn_dru_is_anti_malarial' => 'is_anti_malarial',
            'dyn_dru_is_antibiotic' => 'is_antibiotic',
            'dyn_dru_duration' => 'duration',
        ],

        'custom_drug' => [
            'dyn_adr_id' => 'id',
            'dyn_adr_diagnosis_id' => 'diagnosis_id',
            'dyn_adr_custom_diagnosis_id' => 'custom_diagnosis_id',
            'dyn_adr_label' => 'Name',
            'dyn_adr_drugs' => 'duration',
            'dyn_adr_created_at' => 'created_at',
            'dyn_adr_updated_at' => 'updated_at',
        ],

        'drug_reference' => [
            'dyn_dre_id' => 'id',
            'dyn_dre_drug_id' => 'drug_id',
            'dyn_dre_diagnosis_id' => 'diagnosis_id',
            'dyn_dre_custom_diagnosis_id' => 'custom_diagnosis_id',
            'dyn_dre_formulation_id' => 'formulation_id',
            'dyn_dre_status' => 'status',
            'dyn_dre_duration' => 'duration',
        ],

        'management' => [
            'dyn_man_id' => 'id',
            'dyn_man_diagnosis_id' => 'diagnosis_id',
            'dyn_man_medal_c_id' => 'medal_c_id',
            'dyn_man_type' => 'type',
            'dyn_man_label' => 'label',
            'dyn_man_description' => 'description',
            'dyn_man_created_at' => 'created_at',
            'dyn_man_updated_at' => 'updated_at',
        ],

        'management_reference' => [
            'dyn_mre_id' => 'id',
            'dyn_mre_diagnosis_id' => 'diagnosis_id',
            'dyn_mre_management_id' => 'management_id',
        ],

        'answer_type' => [
            'dyn_aty_id' => 'id',
            'dyn_aty_value' => 'value',
            'dyn_aty_created_at' => 'created_at',
            'dyn_aty_updated_at' => 'updated_at',
        ],

        'formulation' => [
            'dyn_for_id' => 'id',
            'dyn_for_medication_form' => 'medication_form',
            'dyn_for_administration_route_name' => 'administration_route_name',
            'dyn_for_liquid_concentration' => 'liquid_concentration',
            'dyn_for_dose_form' => 'dose_form',
            'dyn_for_unique_dose' => 'unique_dose',
            'dyn_for_by_age' => 'by_age',
            'dyn_for_minimal_dose_per_kg' => 'minimal_dose_per_kg',
            'dyn_for_maximal_dose_per_kg' => 'maximal_dose_per_kg',
            'dyn_for_maximal_dose' => 'maximal_dose',
            'dyn_for_description' => 'description',
            'dyn_for_doses_per_day' => 'doses_per_day',
            'dyn_for_created_at' => 'created_at',
            'dyn_for_updated_at' => 'updated_at',
            'dyn_for_drug_id' => 'drug_id',
            'dyn_for_administration_route_category' => 'administration_route_category',
            'dyn_for_medal_c_id' => 'medal_c_id',
        ],

        'answer' => [
            'dyn_ans_id' => 'id',
            'dyn_ans_label' => 'label',
            'dyn_ans_medal_c_id' => 'medal_c_id',
            'dyn_ans_node_id' => 'node_id',
            'dyn_ans_created_at' => 'created_at',
            'dyn_ans_updated_at' => 'updated_at',
        ],

        'health_facility' => [
            'dyn_hf_id' => 'id',
            'dyn_hf_group_id' => 'group_id',
            'dyn_hf_name' => 'name',
            'dyn_hf_long' => 'long',
            'dyn_hf_lat' => 'lat',
            'dyn_hf_hf_mode' => 'hf_mode',
            'dyn_hf_country' => 'country',
            'dyn_hf_area' => 'area',
            'dyn_hf_council' => 'council',
            'dyn_hf_is_drop' => 'is_drop',
            'dyn_hf_version_json_id' => 'version_json_id',
            'dyn_hf_created_at' => 'created_at',
            'dyn_hf_updated_at' => 'updated_at',
        ],
        'device' => [
            'dyn_device_id' => 'id',
            'dyn_device_name' => 'name',
            'dyn_device_type' => 'type',
            'dyn_device_mac_address' => 'mac_address',
            'dyn_device_model' => 'model',
            'dyn_device_brand' => 'brand',
            'dyn_device_os' => 'os',
            'dyn_device_os_version' => 'os_version',
            'dyn_device_redirect' => 'redirect',
            'dyn_device_status' => 'status',
            'dyn_device_user_id' => 'user_id',
            'dyn_device_health_facility_id' => 'health_facility_id',
            'dyn_device_last_seen' => 'last_seen',
            'dyn_device_created_at' => 'created_at',
            'dyn_device_updated_at' => 'updated_at',
        ],
    ],

    'flat' => [
        'folder' => 'answers/',

        'identifiers' => [
            'patient' => [
                'dyn_pat_study_id_patient' => 'patient_id',
                'dyn_pat_birth_date' => 'patient_birthdate_old',
                'dyn_cor_pat_birth_date' => 'corrected_patient_birthdate',
                'dyn_pat_gender' => 'patient_gender',
                'dyn_pat_local_patient_id' => 'patient_local_patient_id',
                'dyn_pat_consent' => 'patient_consent',
                'dyn_pat_redcap' => 'patient_redcap',
                'dyn_pat_duplicate' => 'patient_duplicate',
                'dyn_pat_other_uid' => 'patient_other_uid',
                'dyn_pat_other_study_id' => 'patient_other_study_id',
                'dyn_pat_other_group_id' => 'patient_other_group_id',
                'dyn_pat_merged_with' => 'patient_merged_with',
                'dyn_pat_is_drop' => 'patient_is_drop',
                'dyn_pat_merged' => 'patient_merged',
                'dyn_pat_status' => 'patient_status',
                'dyn_pat_related_ids' => 'patient_related_ids',
                'dyn_pat_other_id' => 'patient_other_id',
            ],

            'medical_case' => [
                'dyn_mc_id' => 'medical_case_id',
                'dyn_mc_local_medical_case_id' => 'medical_case_local_id',
                'dyn_mc_consent' => 'medical_case_consent',
                'dyn_mc_isEligible' => 'medical_case_isEligible',
                'dyn_mc_redcap' => 'medical_case_redcap',
                'dyn_mc_consultation_date' => 'medical_case_consultation_date',
                'dyn_mc_birthdate' => 'medical_case_birthdate',
                'dyn_mc_age_in_days' => 'corrected_bd5ageindays',
                'dyn_mc_closedAt' => 'medical_case_closed_at',
                'dyn_mc_force_close' => 'medical_case_force_close',
                'dyn_mc_redcap_flag' => 'medical_case_redcap_flag',
                'dyn_mc_duplicate' => 'medical_case_duplicate',
                'dyn_is_drop' => 'medical_case_is_drop',
                'dyn_mc_app_version' => 'medical_case_app_version',
                'dyn_mc_blank_case' => 'medical_case_blank_case',
            ],

            'health_facility' => [
                'dyn_hfa_id' => 'health_facility_id',
                'dyn_hfa_group_id' => 'health_facility_group_id',
                'dyn_hfa_long' => 'health_facility_longitude',
                'dyn_hfa_lat' => 'health_facility_latitude',
                'dyn_hfa_hf_mode' => 'health_facility_hf_mode',
                'dyn_hfa_name' => 'health_facility_name',
                'dyn_hfa_country' => 'health_facility_country',
                'dyn_hfa_area' => 'health_facility_area',
                'dyn_hfa_is_drop' => 'health_facility_is_drop',
                'dyn_hfa_council' => 'health_facility_council',
            ],

            'device' => [
                'dyn_device_id' => 'device_id',
                'dyn_device_name' => 'device_name',
                'dyn_device_health_facility_id' => 'device_health_facility_id',
                'dyn_device_health_worker' => 'device_health_worker',
                'dyn_device_mac_address' => 'device_mac_address',
                'dyn_device_last_seen' => 'device_last_seen',
            ],

            'version' => [
                'dyn_ver_id' => 'version_id',
                'dyn_ver_medal_c_id' => 'version_medal_c_id',
                'dyn_ver_name' => 'version_name',
                'dyn_ver_consent_management' => 'version_consent_management',
                'dyn_ver_study' => 'version_study',
                'dyn_ver_is_arm_control' => 'version_is_arm_control',
            ],

            'algorithm' => [
                'dyn_alg_id' => 'algorithm_id',
                'dyn_alg_name' => 'algorithm_name',
            ],
        ],
    ],
];
