<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Laravel\Passport\Token;
use Lcobucci\JWT\Parser;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware(['auth:api', 'device.resolve'])->prefix('/v1')->group(function () {
    Route::get('/health-facility-info', 'Api\AuthDeviceController@healthFacilityInfo');
    Route::get('/algorithm', 'Api\AuthDeviceController@algorithm');
    Route::get('/tokeninfo', 'Api\AuthDeviceController@tokeninfo');
    Route::get('/emergency-content', 'Api\AuthDeviceController@emergencyContent');
    Route::post('/device-info', 'Api\AuthDeviceController@storeDeviceInfo');
    Route::post('sync_medical_cases', 'Api\AuthDeviceController@syncMedicalCases');
    Route::post('sync_diagnostics', 'DeviceController@storeDeviceDiagnostics');
});

Route::middleware('auth:api')->get('/protected-api', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/get-client-id', function (Request $request) {
    $bearerToken = $request->bearerToken();
    $parsedJwt = (new Parser())->parse($bearerToken);

    if ($parsedJwt->hasHeader('jti')) {
        $tokenId = $parsedJwt->getHeader('jti');
    } elseif ($parsedJwt->hasClaim('jti')) {
        $tokenId = $parsedJwt->getClaim('jti');
    } else {
        Log::error('Invalid JWT token, Unable to find JTI header');

        return null;
    }
    $client = Token::find($tokenId)->client;

    return $client;
});

// This route needs no Auth For people to see short facility info
Route::get('latest_sync/{health_facility_id}', 'HealthFacilityController@facilityInfo');
