<?php

namespace App\Jobs;

use App\Services\ExportCsvSeparate;
use App\Services\FullExportCsvSeparate;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Madnest\Madzipper\Madzipper;

class ExportSeparated implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Starting separated export');
        $export_file = storage_path('app/export/export_separated.zip');
        $toDate = new DateTime();
        if (File::exists($export_file)) {
            $lastmodified_file = File::lastModified($export_file);
            $lastmodified = DateTime::createFromFormat('U', $lastmodified_file);
            if ($toDate->diff($lastmodified)->h < 5 && $toDate->diff($lastmodified)->d < 1) {
                Log::info('Separated export already done in the last 5 hours, skipping');

                return;
            }
        }

        ini_set('memory_limit', '4096M');
        $extract_file_name = Config::get('csv.public_extract_name_separated');
        $file_from_public = storage_path('app/export/' . $extract_file_name . '.zip');
        $folder = storage_path('app/export/' . Config::get('csv.folder_separated'));
        $fromDate = new DateTime('2020-01-01');
        $zipper = new Madzipper();
        $zipper->make($file_from_public);

        $folder = storage_path('app/export/' . Config::get('csv.folder_separated'));
        File::ensureDirectoryExists($folder);

        // TODO Handle $fromDate, $toDate, and $chunk_key.
        $csv_export = new ExportCsvSeparate(collect(), $fromDate, $toDate, false);
        $csv_export->export();

        $zipper->add($folder);
        $zipper->close();
        File::deleteDirectory($folder);
        Log::info('Separated export done');

        // Full data set export
        $full_extract_file_name = Config::get('csv.public_full_extract_name_separated');
        $full_file_from_public = storage_path('app/export/' . $full_extract_file_name . '.zip');
        $folder_full = storage_path('app/export/' . Config::get('csv.folder_separated'));
        $zipper_full = new Madzipper();
        $zipper_full->make($full_file_from_public);
        File::ensureDirectoryExists($folder_full);

        $csv_export_full = new FullExportCsvSeparate(collect(), $fromDate, $toDate, false);
        $csv_export_full->export();

        $zipper_full->add($folder_full);
        $zipper_full->close();
        File::deleteDirectory($folder_full);
        Log::info('Full data set separated export done');
    }
}
