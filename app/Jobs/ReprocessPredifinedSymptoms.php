<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Answer;
use App\MedicalCase;
use App\Node;
use App\Services\MedicalCaseAnswerLoader;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;


class ReprocessPredifinedSymptoms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $chunk_done = 0;
        $json_success_files = Storage::files('json_success');
        $chunked_jsons = array_chunk($json_success_files, 100);
        $total_chunks = count($chunked_jsons);
        foreach ($chunked_jsons as $key => $chunked_json) {
            foreach ($chunked_json as $json) {
                $filename = array_slice(explode('/', $json), -1)[0];
                $caseData = json_decode(Storage::get("json_success/$filename"), true);
                if ($caseData === null) {
                    continue;
                }
                $patient_json = $caseData['patient'];
                if (empty($patient_json)) {
                    continue;
                }

                $medical_case_id = $caseData['id'];
                $medical_case = MedicalCase::where('local_medical_case_id', $medical_case_id)->first();

                if (empty($medical_case)) {
                    continue;
                }

                // Case answers
                foreach ($caseData['nodes'] as $nodeData) {
                    $algoNode = Node::where('medal_c_id', $nodeData['id'])->first();
                    // Only load answers to QuestionsSequence
                    $medal_c_ids_ps = Node::where('type', 'QuestionsSequence')->where('category', 'predefined_syndrome')->get()->pluck('medal_c_id')->toArray();
                    if ($algoNode && in_array($nodeData['id'], $medal_c_ids_ps)) {
                        $algoNodeAnswer = Answer::where('medal_c_id', $nodeData['answer'])->first();
                        $medicalCaseAnswer = (new MedicalCaseAnswerLoader($nodeData, $medical_case, $algoNode, $algoNodeAnswer))->load();
                    }
                }
            }
            $chunk_done++;
            Log::info("Chunk $chunk_done of $total_chunks done");
        }
    }
}
