<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use ZipArchive;

class ProcessUploadZip implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $path;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $extractDir = Config::get('medal.storage.json_extract_dir');
        Storage::makeDirectory($extractDir);

        $zip = new ZipArchive;
        $res = $zip->open(Storage::disk('local')->path($this->path), ZipArchive::CHECKCONS);
        if ($res !== true) {
            Log::error("Corrupted zip file: $this->path");

            return;
        }

        for ($i = 0; $i < $zip->numFiles; $i++) {
            $jsonName = Str::uuid() . '.json';
            $zip->renameIndex($i, $jsonName);
            $json_is_extracted = $zip->extractTo(Storage::disk('local')->path($extractDir), $jsonName);
            if ($json_is_extracted) {
                if (!DB::table('jobs')->where('payload', 'LIKE', "%$jsonName%")->exists()) {
                    ProcessCaseJson::dispatch($extractDir, $jsonName);
                }
            } else {
                Log::error("Couldn't extract $jsonName.");
            }
        }
        $zip->close();
    }
}
