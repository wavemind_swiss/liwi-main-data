<?php

namespace App\Jobs;

use App\Device;
use App\Services\Http;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CheckAutoDiagnosticJson implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $json_filename;

    protected $hub;

    /**
     * __construct.
     *
     * @param  string $json_filename
     * @param  Device $hub
     * @return void
     */
    public function __construct(string $json_filename, Device $hub)
    {
        $this->json_filename = $json_filename;
        $this->hub = $hub;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->fileClean($this->hub);
        $autoDiag_dir = config('medal.storage.json_diag_dir') . '/';
        $json_diagnostic = $this->readFile($autoDiag_dir . $this->json_filename);
        $file_path = $autoDiag_dir . config('autoDiagnostic.commit_file');
        $branch_info = [];
        if ($json_diagnostic['getGitInformation']['status']) {
            $commit_Branch = $json_diagnostic['getGitInformation']['data']['git_branch'];
            $commit_hash = $json_diagnostic['getGitInformation']['data']['commit_hash'];
            if (Storage::missing($file_path)) {
                Log::info('File does not exist, creating..');
                $branch_info = $this->fetchAndWrite($commit_Branch, $file_path);
            }
            $git_file = $this->readFile($file_path);
            $commit_present = true;
            if (isset($git_file[$commit_Branch])) {
                [$keys, $values] = Arr::divide($git_file[$commit_Branch]);
                $commit_present = collect($keys)->contains(function ($value, $iteration) use ($commit_hash) {
                    return Str::startsWith($value, $commit_hash);
                });
            }
            if (!Arr::exists($git_file, $commit_Branch) || !$commit_present) {
                Log::info('Git branch or commit does not present in commits file, fetching...');
                $branch_info = $this->fetchAndWrite($commit_Branch, $file_path, $git_file);
                $git_file = $this->readFile($file_path);
                [$keys, $values] = Arr::divide($git_file[$commit_Branch]);
            }
            if (!empty($branch_info)) {
                [$keys, $values] = Arr::divide($branch_info);
            }
            $commit_full_pos = null;
            $is_uptodate = false;

            $offset = array_search($commit_hash, $keys, true);

            if ($offset === 0) {
                $is_uptodate = true;
                $commit_full_pos = $offset;
            }
            if ($offset > 0) {
                $commit_full_pos = $offset;
            }
            if ($offset === false) {
                $offset = -1;
            }

            $auto_diag = $this->hub->auto_diagnostics()->where('device_id', $this->hub->id)->where('path', $autoDiag_dir . $this->json_filename)->first();
            $auto_diag->update([
                'commit_pos' => $commit_full_pos,
                'up_to_date' => $is_uptodate,
                'bitbucket_commit' => Arr::first($keys),
                'bitbucket_commit_time' => $git_file[$commit_Branch][Arr::first($keys)],
            ]);

            if (!Storage::put($autoDiag_dir . $this->json_filename, json_encode($json_diagnostic))) {
                Log::info('Error while processing ' . $this->json_filename);
            }
            Log::info('Successfully processed ' . $this->json_filename);
        }
    }

    /**
     * fetchAndWrite.
     *
     * @param  string $commit_Branch
     * @param  string $file_path
     * @param  string $commit_content
     * @return array
     */
    private function fetchAndWrite(string $commit_Branch, string $file_path, array $commit_content = []): array
    {
        if (!$content = $this->fetchRemote($commit_Branch)) {
            Log::error('Failure to fetch the update');

            return [];
        }
        if (!$branch_data = $this->writeToCommit($content, $commit_Branch, $file_path, $commit_content)) {
            Log::error('Unable to save the file for commit');

            return [];
        }

        return $branch_data;
    }

    /**
     * readFile.
     *
     * @param  string $file_path
     * @return array
     */
    private function readFile(string $file_path): array
    {
        $str_diagnostic = (string) Storage::get($file_path);
        $json_diagnostic = json_decode($str_diagnostic, true);

        return $json_diagnostic;
    }

    /**
     * fetchRemote.
     *
     * @param  string $commit_Branch
     * @return array
     */
    public function fetchRemote(string $commit_Branch): array
    {
        $remoteUrl = config('autoDiagnostic.liwi_local_data_url') . $commit_Branch .
        config('autoDiagnostic.fetch_limit');
        $remoteRequest = Http::get($remoteUrl);
        if ($remoteRequest['code'] != 200) {
            Log::error('Error fetching remote: ' . $remoteUrl . 'status: ' . $remoteRequest['code']);

            return [];
        }

        return (array) json_decode($remoteRequest['content']);
    }

    /**
     * writeToCommit.
     *
     * @param  array $content
     * @param  string $branch_name
     * @param  string $file_path
     * @param  array $commit_content
     * @return array
     */
    public function writeToCommit(array $content, string $branch_name, string $file_path, array $commit_content = []): array
    {
        $commits = [];
        $branch = [];
        foreach ($content['values'] as $item) {
            $commits[$item->hash] = str_replace('T', ' ', explode('+', $item->date)[0]);
        }
        $commit_content ? $branch = $commit_content : null;
        $branch[$branch_name] = $commits;
        $file_saved = Storage::put($file_path, json_encode($branch));
        if (!$file_saved) {
            return [];
        }

        return $branch[$branch_name];
    }

    /**
     * fileClean.
     *
     * @param  Device $hub
     * @return void
     */
    private function fileClean(Device $hub): void
    {
        if ($hub->auto_diagnostics()->count() >= config('autoDiagnostic.file_number_to_keep')) {
            $hub->auto_diagnostics()->latest()->skip(
                config('autoDiagnostic.file_number_to_keep')
            )->get()->each(function ($diagnostic) {
                Storage::delete($diagnostic->path);
                $diagnostic->delete();
                Log::info("Deleted {$diagnostic->path}");
            });
        }
    }
}
