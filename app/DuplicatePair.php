<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DuplicatePair.
 *
 * @property int $id
 * @property array $pairs
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|DuplicatePair newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DuplicatePair newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DuplicatePair query()
 * @method static \Illuminate\Database\Eloquent\Builder|DuplicatePair whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DuplicatePair whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DuplicatePair wherePairs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DuplicatePair whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DuplicatePair extends Model
{
    protected $casts = [
        'pairs' => 'array',
    ];

    protected $guarded = [];
}
