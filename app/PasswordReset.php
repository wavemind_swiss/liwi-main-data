<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PasswordReset.
 *
 * @property int $id
 * @property string $token
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset query()
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordReset whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PasswordReset extends Model
{
    protected $table = 'custom_password_resets';

    protected $guarded = [];

    public static function saveReset($user, $random_password)
    {
        $user = self::updateOrCreate(
            ['email' => $user->email],
            ['token' => $random_password]
        );

        return true;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'email', 'email');
    }
}
