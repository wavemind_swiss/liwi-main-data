<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\AnswerType.
 *
 * @property int $id
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerType query()
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerType whereValue($value)
 * @mixin \Eloquent
 */
class AnswerType extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guarded = [];
}
