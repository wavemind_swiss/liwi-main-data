<?php

namespace App\Providers;

use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        $appUrl = config('app.url');
        $appUrls = explode('//', $appUrl ?? '');
        $allowedHosts = [$appUrl, $appUrls[1]];
        $currentHost = request()->getHttpHost();
        // $currentHostUrls = explode(':', $currentHost ?? '');
        $json_allowed_host = json_encode($allowedHosts);

        if (!config('app.debug')) {
            if (!in_array($currentHost, $allowedHosts)) {
                header('HTTP/1.1 400 Bad Request');
                Log::error("Host mismatch : $currentHost != $json_allowed_host");
                exit;
            }
        }

        if (config('app.env') !== 'local') {
            $url->forceScheme('https');
        }

        Schema::defaultStringLength(191);
    }
}
