<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Version.
 *
 * @property int $id
 * @property int $medal_c_id
 * @property string $name
 * @property int $algorithm_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property bool $consent_management
 * @property string|null $study
 * @property bool|null $is_arm_control
 * @property-read \App\Algorithm|null $algorithm
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \App\PatientConfig|null $configurations
 * @method static \Illuminate\Database\Eloquent\Builder|Version newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Version newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Version query()
 * @method static \Illuminate\Database\Eloquent\Builder|Version whereAlgorithmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Version whereConsentManagement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Version whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Version whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Version whereIsArmControl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Version whereMedalCId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Version whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Version whereStudy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Version whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Version extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guarded = [];

    /**
     * making a relationship to medicalCase.
     * @return Many medical cases
     */
    public function configurations()
    {
        return $this->hasOne('App\PatientConfig');
    }

    /**
     * making a relationship to algorithm.
     * @return one to one medical cases
     */
    public function algorithm()
    {
        return $this->hasOne('App\Algorithm', 'id', 'algorithm_id');
    }
}
