<?php

namespace App\Services;

use App\Algorithm;

class AlgorithmLoader extends ModelLoader
{
    protected $data;

    /**
     * Constructor.
     *
     * @param array $data
     */
    public function __construct($data)
    {
        parent::__construct($data);
        $this->data = $data;
    }

    protected function model()
    {
        return Algorithm::class;
    }

    protected function configName()
    {
        return 'algorithm';
    }
}
