<?php

namespace App\Services;

use App\DiagnosisReference;
use App\Drug;
use App\DrugReference;
use App\Enums\ClinicianChoice;
use App\Formulation;

class DrugReferenceLoader extends ModelLoader
{
    protected $drugRefData;

    protected $diagnosisRef;

    protected $drug;

    protected $formulation;

    protected $isDiagnosisCustom;

    /**
     * @var ClinicianChoice
     */
    protected $clinicianChoice;

    /**
     * Constructor.
     *
     * @param object $data
     * @param DiagnosisReference $diagnosisRef
     * @param Drug $drug
     * @param Formulation $formulation
     * @param ClinicianChoice $clinicianChoice
     * @param bool $isDiagnosisCustom
     */
    public function __construct($drugRefData, $diagnosisRef, $drug, $formulation, ClinicianChoice $clinicianChoice, bool $isDiagnosisCustom)
    {
        parent::__construct($drugRefData);
        $this->drugRefData = $drugRefData;
        $this->diagnosisRef = $diagnosisRef;
        $this->drug = $drug;
        $this->formulation = $formulation;
        $this->clinicianChoice = $clinicianChoice;
        $this->isDiagnosisCustom = $isDiagnosisCustom;
    }

    protected function getKeys()
    {
        if ($this->isDiagnosisCustom) {
            $diag_field = 'custom_diagnosis_id';
        } else {
            $diag_field = 'diagnosis_id';
        }

        return [
            $diag_field => $this->diagnosisRef->id,
            'drug_id' => $this->drug->id,
            'formulation_id' => $this->formulation->id ?? null,
        ];
    }

    protected function getValues()
    {
        $values = parent::getValues();

        return array_merge($values, [
            'status' => $this->clinicianChoice,
            'duration' => $values['duration'] ?? $this->drug->duration,
        ]);
    }

    protected function model()
    {
        return DrugReference::class;
    }

    protected function configName()
    {
        return 'drug_reference';
    }
}
