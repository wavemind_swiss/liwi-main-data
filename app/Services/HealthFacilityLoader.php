<?php

namespace App\Services;

use App\HealthFacility;

class HealthFacilityLoader extends ModelLoader
{
    protected function model()
    {
        return HealthFacility::class;
    }

    protected function configName()
    {
        return 'health_facility';
    }
}
