<?php

namespace App\Services;

use App\DiagnosisReference;
use App\Enums\ClinicianChoice;

class DiagnosisReferenceLoader extends ModelLoader
{
    protected $diagnosisRefData;

    protected $medicalCase;

    protected $diagnosis;

    /**
     * @var ClinicianChoice
     */
    protected $clinicianChoice;

    /**
     * Constructor.
     *
     * @param $diagnosisRefData
     * @param MedicalCase $medicalCase
     * @param Diagnosis $diagnosis
     * @param ClinicianChoice $clinicianChoice
     */
    public function __construct($diagnosisRefData, $medicalCase, $diagnosis, ClinicianChoice $clinicianChoice)
    {
        parent::__construct($diagnosisRefData);
        $this->diagnosisRefData = $diagnosisRefData;
        $this->medicalCase = $medicalCase;
        $this->diagnosis = $diagnosis;
        $this->clinicianChoice = $clinicianChoice;
    }

    protected function getKeys()
    {
        return [
            'medical_case_id' => $this->medicalCase->id,
            'diagnosis_id' => $this->diagnosis->id,
        ];
    }

    protected function getValues()
    {
        return [
            'status' => $this->clinicianChoice,
        ];
    }

    protected function model()
    {
        return DiagnosisReference::class;
    }

    protected function configName()
    {
        return 'diagnosis_reference';
    }
}
