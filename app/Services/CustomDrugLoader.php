<?php

namespace App\Services;

use App\CustomDrug;

class CustomDrugLoader extends ModelLoader
{
    protected $customDrugData;

    protected $customDiagnosis;

    protected $isDiagnosisCustom;

    /**
     * Constructor.
     *
     * @param array $data
     * @param CustomDiagnosis $customDiagnosis
     */
    public function __construct($customDrugData, $customDiagnosis, $isDiagnosisCustom)
    {
        parent::__construct($customDrugData);
        $this->customDrugData = $customDrugData;
        $this->customDiagnosis = $customDiagnosis;
        $this->isDiagnosisCustom = $isDiagnosisCustom;
    }

    protected function getKeys()
    {
        if ($this->isDiagnosisCustom) {
            $diag_field = 'custom_diagnosis_id';
        } else {
            $diag_field = 'diagnosis_id';
        }

        return array_merge(parent::getKeys(), [
            $diag_field => $this->customDiagnosis->id,
        ]);
    }

    protected function model()
    {
        return CustomDrug::class;
    }

    protected function configName()
    {
        return 'custom_drug';
    }
}
