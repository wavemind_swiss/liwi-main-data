<?php

namespace App\Services;

use App\Activity;
use Carbon\Carbon;

class ActivityLoader extends ModelLoader
{
    protected $activitiesData;

    protected $medicalCase;

    /**
     * Constructor.
     *
     * @param array $activitiesData
     */
    public function __construct($activitiesData, $medicalCase)
    {
        parent::__construct($activitiesData);

        $this->activitiesData = $activitiesData;
        $this->medicalCase = $medicalCase;
    }

    protected function getValues()
    {
        $values = array_merge(parent::getValues(), [
            'medical_case_id' => $this->medicalCase->id,
        ]);

        if (!empty($this->activitiesData['created_at'])) {
            $values = array_merge($values, [
                'created_at' => Carbon::createFromTimestampMs($this->activitiesData['created_at'])->format('Y-m-d H:i:s'),
            ]);
        }

        return $values;

    }

    protected function model()
    {
        return Activity::class;
    }

    protected function configName()
    {
        return 'activities';
    }
}
