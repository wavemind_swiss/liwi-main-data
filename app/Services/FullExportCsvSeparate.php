<?php

namespace App\Services;

use App\Activity;
use App\Algorithm;
use App\Answer;
use App\AnswerType;
use App\CustomDiagnosis;
use App\CustomDrug;
use App\Device;
use App\Diagnosis;
use App\DiagnosisReference;
use App\Drug;
use App\DrugReference;
use App\Formulation;
use App\HealthFacility;
use App\Management;
use App\ManagementReference;
use App\MedicalCase;
use App\MedicalCaseAnswer;
use App\Node;
use App\Patient;
use App\Version;
use Illuminate\Support\Collection;

class FullExportCsvSeparate extends ExportCsv
{
    /**
     * @param Patient $patient
     * @return array patient data
     */
    protected static function getPatientData(Patient $patient): array
    {
        return [
            $patient->id,
            $patient->first_name,
            $patient->middle_name,
            $patient->last_name,
            $patient->created_at,
            $patient->updated_at,
            $patient->birthdate,
            $patient->corrected_birthdate,
            $patient->gender,
            $patient->local_patient_id,
            $patient->consent,
            $patient->redcap,
            $patient->duplicate,
            $patient->other_uid,
            $patient->other_study_id,
            $patient->other_group_id,
            $patient->merged_with,
            $patient->merged,
            $patient->is_drop,
            $patient->status,
            $patient->related_ids,
            $patient->other_id,
        ];
    }

    /**
     * @param MedicalCase $medical_case
     * @return array medical case data
     */
    protected static function getMedicalCaseData(MedicalCase $medical_case): array
    {
        return [
            $medical_case->id,
            $medical_case->version_id,
            $medical_case->patient_id,
            $medical_case->local_medical_case_id,
            $medical_case->consent,
            $medical_case->isEligible,
            $medical_case->group_id,
            $medical_case->redcap,
            $medical_case->consultation_date,
            $medical_case->birthdate,
            $medical_case->closedAt,
            $medical_case->force_close,
            $medical_case->mc_redcap_flag,
            $medical_case->is_drop,
            $medical_case->duplicate,
            $medical_case->app_version,
            $medical_case->git_version,
            $medical_case->corrected_bd5ageindays,
            $medical_case->blankcase,
        ];
    }

    /**
     * @param MedicalCaseAnswer $medical_case_answer
     * @return array medical case answer data
     */
    protected static function getMedicalCaseAnswerData(MedicalCaseAnswer $medical_case_answer): array
    {
        return [
            $medical_case_answer->id,
            $medical_case_answer->medical_case_id,
            $medical_case_answer->answer_id,
            $medical_case_answer->node_id,
            $medical_case_answer->value,
            $medical_case_answer->created_at,
            $medical_case_answer->updated_at,
        ];
    }

    /**
     * @param Node $node
     * @return array node data
     */
    protected static function getNodeData(Node $node): array
    {
        return [
            $node->id,
            $node->medal_c_id,
            $node->reference,
            $node->label,
            $node->type,
            $node->category,
            $node->priority,
            $node->stage,
            $node->description,
            $node->formula,
            $node->answer_type_id,
            $node->algorithm_id,
            $node->created_at,
            $node->updated_at,
            $node->is_identifiable,
            $node->display_format,
        ];
    }

    /**
     * @param Version $version
     * @return array version data
     */
    protected static function getVersionData(Version $version): array
    {
        return [
            $version->id,
            $version->medal_c_id,
            $version->name,
            $version->algorithm_id,
            $version->created_at,
            $version->updated_at,
            $version->consent_management,
            $version->study,
            $version->is_arm_control,
        ];
    }

    /**
     * @param Algorithm $algorithm
     * @return array algorithm data
     */
    protected static function getAlgorithmData(Algorithm $algorithm): array
    {
        return [
            $algorithm->id,
            $algorithm->medal_c_id,
            $algorithm->name,
            $algorithm->created_at,
            $algorithm->updated_at,
        ];
    }

    /**
     * @param Activity $activity
     * @return array activity data
     */
    protected static function getActivityData(Activity $activity): array
    {
        return [
            $activity->id,
            $activity->medical_case_id,
            $activity->medal_c_id,
            $activity->step,
            $activity->clinician,
            $activity->mac_address,
            $activity->created_at,
            $activity->updated_at,
        ];
    }

    /**
     * @param Diagnosis $diagnosis
     * @return array diagnosis data
     */
    protected static function getDiagnosisData(Diagnosis $diagnosis): array
    {
        return [
            $diagnosis->id,
            $diagnosis->medal_c_id,
            $diagnosis->label,
            $diagnosis->diagnostic_id,
            $diagnosis->created_at,
            $diagnosis->updated_at,
            $diagnosis->type,
            $diagnosis->version_id,
        ];
    }

    /**
     * @param  CustomDiagnosis $custom_diagnosis
     * @return array custom diagnosis data
     */
    protected static function getCustomDiagnosisData(CustomDiagnosis $custom_diagnosis): array
    {
        return [
            $custom_diagnosis->id,
            $custom_diagnosis->label,
            $custom_diagnosis->drugs,
            $custom_diagnosis->created_at,
            $custom_diagnosis->updated_at,
            $custom_diagnosis->medical_case_id,
        ];
    }

    /**
     * @param  DiagnosisReference $diagnosis_reference
     * @return array diagnosis reference data
     */
    protected static function getDiagnosisReferenceData(DiagnosisReference $diagnosis_reference): array
    {
        return [
            $diagnosis_reference->id,
            $diagnosis_reference->status->value,
            $diagnosis_reference->diagnosis_id,
            $diagnosis_reference->medical_case_id,
        ];
    }

    /**
     * @param  Drug $drug
     * @return array drug data
     */
    protected static function getDrugData(Drug $drug): array
    {
        return [
            $drug->id,
            $drug->medal_c_id,
            $drug->type,
            $drug->label,
            $drug->description,
            $drug->diagnosis_id,
            $drug->created_at,
            $drug->updated_at,
            $drug->is_anti_malarial,
            $drug->is_antibiotic,
            $drug->duration,
        ];
    }

    /**
     * @param CustomDrug $custom
     * @return array additional drug data
     */
    protected static function getCustomDrugData(CustomDrug $custom): array
    {
        return [
            $custom->id,
            $custom->diagnosis_id ?? '',
            $custom->custom_diagnosis_id ?? '',
            $custom->name,
            $custom->duration,
            $custom->created_at,
            $custom->updated_at,
        ];
    }

    /**
     * @param  DrugReference $drug_reference
     * @return array drug reference data
     */
    protected static function getDrugReferenceData(DrugReference $drug_reference): array
    {
        return [
            $drug_reference->id,
            $drug_reference->drug_id,
            $drug_reference->diagnosis_id ?? '',
            $drug_reference->custom_diagnosis_id ?? '',
            $drug_reference->formulation_id,
            $drug_reference->status->value,
            $drug_reference->duration,
        ];
    }

    /**
     * @param  Management $management
     * @return array management data
     */
    protected static function getManagementData(Management $management): array
    {
        return [
            $management->id,
            $management->diagnosis_id,
            $management->medal_c_id,
            $management->type,
            $management->label,
            $management->description,
            $management->created_at,
            $management->updated_at,
        ];
    }

    /**
     * @param  ManagementReference $management_reference
     * @return array management reference data
     */
    protected static function getManagementReferenceData(ManagementReference $management_reference): array
    {
        return [
            $management_reference->id,
            $management_reference->diagnosis_id,
            $management_reference->management_id,
        ];
    }

    /**
     * @param  AnswerType $answer_type
     * @return array answer type data
     */
    protected static function getAnswerTypeData(AnswerType $answer_type): array
    {
        return [
            $answer_type->id,
            $answer_type->value,
            $answer_type->created_at,
            $answer_type->updated_at,
        ];
    }

    /**
     * @param  Formulation $formulation
     * @return array formulation data
     */
    protected static function getFormulationData(Formulation $formulation): array
    {
        return [
            $formulation->id,
            $formulation->medication_form,
            $formulation->administration_route_name,
            $formulation->liquid_concentration,
            $formulation->dose_form,
            $formulation->unique_dose,
            $formulation->by_age,
            $formulation->minimal_dose_per_kg,
            $formulation->maximal_dose_per_kg,
            $formulation->maximal_dose,
            $formulation->description,
            $formulation->doses_per_day,
            $formulation->created_at,
            $formulation->updated_at,
            $formulation->drug_id,
            $formulation->administration_route_category,
            $formulation->medal_c_id,
        ];
    }

    /**
     * @param  Answer $answer
     * @return array answer data
     */
    protected static function getAnswerData(Answer $answer): array
    {
        return [
            $answer->id,
            $answer->label,
            $answer->medal_c_id,
            $answer->node_id,
            $answer->created_at,
            $answer->updated_at,
        ];
    }

    /**
     * @param  HealthFacility $health_facility
     * @return array hf data
     */
    protected static function getHealthFacilityData(HealthFacility $health_facility): array
    {
        return [
            $health_facility->id,
            $health_facility->group_id,
            $health_facility->name,
            $health_facility->long,
            $health_facility->lat,
            $health_facility->hf_mode,
            $health_facility->country,
            $health_facility->area,
            $health_facility->council,
            $health_facility->is_drop,
            $health_facility->version_json_id,
            $health_facility->created_at,
            $health_facility->updated_at,
        ];
    }

    /**
     * @param  Device $device
     * @return array hf data
     */
    protected static function getDeviceData(Device $device): array
    {
        return [
            $device->id,
            $device->name,
            $device->type,
            $device->mac_address,
            $device->model,
            $device->brand,
            $device->os,
            $device->os_version,
            $device->redirect,
            $device->status,
            $device->user_id,
            $device->health_facility_id,
            $device->last_seen,
            $device->created_at,
            $device->updated_at,
        ];
    }

    /**
     *  Adds a patient to the data array.
     */
    protected function addData(string $fileNameConfigKey, string $labelConfigKey, callable $dataProvider): void
    {
        // Get file name.
        $fileName = storage_path('app/export/' . config('csv.folder_separated')) . config($fileNameConfigKey);

        // Open file.
        $file = fopen($fileName, 'ab+');

        // Add headers.
        $headers = $this->getLabels($labelConfigKey);
        fputcsv($file, $headers->toArray());

        // Function to write data.
        $writeDataFunction = function (Collection $data) use ($file) {
            $data->each(function ($line) use ($file) {
                $attributes = $this->attributesToStr((array) $line);
                fputcsv($file, $attributes);
            });
        };

        // Write given data.
        $dataProvider($writeDataFunction);

        // Close file.
        fclose($file);
    }

    /**
     *  Adds a patient to the data array.
     */
    protected function addPatientData(callable $writeDataFunction): void
    {
        $data = Patient::all()->map(function (Patient $patient) {
            return self::getPatientData($patient);
        });

        $writeDataFunction($data);
    }

    /**
     * @param string $configName
     * @return Collection
     */
    protected function getLabels(string $configName): Collection
    {
        return collect(
            $this->getAttributeList(config($configName))
        );
    }

    /**
     * Adds a medical case to the data array.
     */
    protected function addMedicalCaseData(callable $writeDataFunction): void
    {
        MedicalCase::query()->with(['patient'])->chunkById(1000, function (Collection $medicalCases) use ($writeDataFunction) {
            $formattedData = $medicalCases->map(function (MedicalCase $medicalCase) {
                return self::getMedicalCaseData($medicalCase);
            });

            $writeDataFunction($formattedData);
        });
    }

    /**
     * Adds a medical case answer to the data array.
     */
    protected function addMedicalCaseAnswerData(callable $writeDataFunction): void
    {
        MedicalCaseAnswer::exportable()->chunkById(1000, function (Collection $medical_case_answers) use ($writeDataFunction) {
            $formattedData = $medical_case_answers->map(function (MedicalCaseAnswer $medical_case_answer) {
                return self::getMedicalCaseAnswerData($medical_case_answer);
            });

            $writeDataFunction($formattedData);
        });
    }

    /**
     * Adds a node to the data array.
     */
    protected function addNodeData(callable $writeDataFunction): void
    {
        Node::query()->chunkById(1000, function (Collection $nodeChunks) use ($writeDataFunction) {
            $formattedData = $nodeChunks->map(function (Node $node) {
                return self::getNodeData($node);
            });

            $writeDataFunction($formattedData);
        });
    }

    /**
     * Adds a version to the data array.
     */
    protected function addVersionData(callable $writeDataFunction): void
    {
        $formattedData = Version::all()->map(function (Version $version) {
            return self::getVersionData($version);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * Adds an algorithm to the data array.
     */
    protected function addAlgorithmData(callable $writeDataFunction): void
    {
        $formattedData = Algorithm::all()->map(function (Algorithm $algorithm) {
            return self::getAlgorithmData($algorithm);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * Adds an activity to the data array.
     */
    protected function addActivityData(callable $writeDataFunction): void
    {
        Activity::query()->chunkById(1000, function (Collection $activityChunks) use ($writeDataFunction) {
            $formattedData = $activityChunks->map(function (Activity $activity) {
                return self::getActivityData($activity);
            });

            $writeDataFunction($formattedData);
        });
    }

    /**
     * Adds a diagnosis to the data array.
     */
    protected function addDiagnosisData(callable $writeDataFunction): void
    {
        $formattedData = Diagnosis::all()->map(function (Diagnosis $diagnosis) {
            return self::getDiagnosisData($diagnosis);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * Adds a custom diagnosis to the data array.
     */
    protected function addCustomDiagnosisData(callable $writeDataFunction): void
    {
        $formattedData = CustomDiagnosis::all()->map(function (CustomDiagnosis $custom_diagnosis) {
            return self::getCustomDiagnosisData($custom_diagnosis);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * Adds a diagnosis reference to the data array.
     */
    protected function addDiagnosisReferenceData(callable $writeDataFunction): void
    {
        $formattedData = DiagnosisReference::all()->map(function (DiagnosisReference $diagnosis_reference) {
            return self::getDiagnosisReferenceData($diagnosis_reference);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * Adds a drug to the data array.
     */
    protected function addDrugData(callable $writeDataFunction): void
    {
        $formattedData = Drug::all()->map(function (Drug $drug) {
            return self::getDrugData($drug);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * Adds an additional drug to the data array.
     */
    protected function addCustomDrugData(callable $writeDataFunction): void
    {
        $formattedData = CustomDrug::all()->map(function (CustomDrug $custom_drug) {
            return self::getCustomDrugData($custom_drug);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * Adds a drug reference to the data array.
     */
    protected function addDrugReferenceData(callable $writeDataFunction): void
    {
        $formattedData = DrugReference::all()->map(function (DrugReference $drug_reference) {
            return self::getDrugReferenceData($drug_reference);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * Adds a management to the data array.
     */
    protected function addManagementData(callable $writeDataFunction): void
    {
        $formattedData = Management::all()->map(function (Management $management) {
            return self::getManagementData($management);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * Adds a management reference to the data array.
     */
    protected function addManagementReferenceData(callable $writeDataFunction): void
    {
        $formattedData = ManagementReference::all()->map(function (ManagementReference $management_reference) {
            return self::getManagementReferenceData($management_reference);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * Adds an answer type to the data array.
     */
    protected function addAnswerTypeData(callable $writeDataFunction): void
    {
        $formattedData = AnswerType::all()->map(function (AnswerType $answer_type) {
            return self::getAnswerTypeData($answer_type);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * Adds a formulation to the data array.
     */
    protected function addFormulationData(callable $writeDataFunction): void
    {
        $formattedData = Formulation::all()->map(function (Formulation $formulation) {
            return self::getFormulationData($formulation);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * @param callable $writeDataFunction
     * @return void
     */
    protected function addAnswerData(callable $writeDataFunction): void
    {
        $formattedData = Answer::all()->map(function (Answer $answer) {
            return self::getAnswerData($answer);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * @param callable $writeDataFunction
     * @return void
     */
    protected function addHealthFacilityData(callable $writeDataFunction): void
    {
        $formattedData = HealthFacility::all()->map(function (HealthFacility $healthFacility) {
            return self::getHealthFacilityData($healthFacility);
        });

        $writeDataFunction($formattedData);
    }

    /**
     * @param callable $writeDataFunction
     * @return void
     */
    protected function addDeviceData(callable $writeDataFunction): void
    {
        $formattedData = Device::all()->map(function (Device $device) {
            return self::getDeviceData($device);
        });

        $writeDataFunction($formattedData);
    }

    public function export()
    {
        $exports = [
            [
                'file_name_config_key' => 'csv.file_names.patients',
                'label_config_key' => 'csv.identifiers.patient',
                'chunk_provider' => [$this, 'addPatientData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.medical_case_answers',
                'label_config_key' => 'csv.identifiers.medical_case_answer',
                'chunk_provider' => [$this, 'addMedicalCaseAnswerData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.medical_cases',
                'label_config_key' => 'csv.identifiers.medical_case',
                'chunk_provider' => [$this, 'addMedicalCaseData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.nodes',
                'label_config_key' => 'csv.identifiers.node',
                'chunk_provider' => [$this, 'addNodeData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.versions',
                'label_config_key' => 'csv.identifiers.version',
                'chunk_provider' => [$this, 'addVersionData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.algorithms',
                'label_config_key' => 'csv.identifiers.algorithm',
                'chunk_provider' => [$this, 'addAlgorithmData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.activities',
                'label_config_key' => 'csv.identifiers.activity',
                'chunk_provider' => [$this, 'addActivityData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.diagnoses',
                'label_config_key' => 'csv.identifiers.diagnosis',
                'chunk_provider' => [$this, 'addDiagnosisData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.custom_diagnoses',
                'label_config_key' => 'csv.identifiers.custom_diagnosis',
                'chunk_provider' => [$this, 'addCustomDiagnosisData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.diagnosis_references',
                'label_config_key' => 'csv.identifiers.diagnosis_reference',
                'chunk_provider' => [$this, 'addDiagnosisReferenceData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.drugs',
                'label_config_key' => 'csv.identifiers.drug',
                'chunk_provider' => [$this, 'addDrugData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.custom_drugs',
                'label_config_key' => 'csv.identifiers.custom_drug',
                'chunk_provider' => [$this, 'addCustomDrugData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.drug_references',
                'label_config_key' => 'csv.identifiers.drug_reference',
                'chunk_provider' => [$this, 'addDrugReferenceData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.managements',
                'label_config_key' => 'csv.identifiers.management',
                'chunk_provider' => [$this, 'addManagementData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.management_references',
                'label_config_key' => 'csv.identifiers.management_reference',
                'chunk_provider' => [$this, 'addManagementReferenceData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.answer_types',
                'label_config_key' => 'csv.identifiers.answer_type',
                'chunk_provider' => [$this, 'addAnswerTypeData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.formulations',
                'label_config_key' => 'csv.identifiers.formulation',
                'chunk_provider' => [$this, 'addFormulationData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.answers',
                'label_config_key' => 'csv.identifiers.answer',
                'chunk_provider' => [$this, 'addAnswerData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.health_facilities',
                'label_config_key' => 'csv.identifiers.health_facility',
                'chunk_provider' => [$this, 'addHealthFacilityData'],
            ],
            [
                'file_name_config_key' => 'csv.file_names.devices',
                'label_config_key' => 'csv.identifiers.device',
                'chunk_provider' => [$this, 'addDeviceData'],
            ],
        ];

        foreach ($exports as $data) {
            $fileNameConfigKey = $data['file_name_config_key'];
            $labelConfigKey = $data['label_config_key'];
            $chunkProvider = $data['chunk_provider'];

            $this->addData($fileNameConfigKey, $labelConfigKey, $chunkProvider);
        }
    }
}
