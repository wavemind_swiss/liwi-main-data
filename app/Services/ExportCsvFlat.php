<?php

namespace App\Services;

use App\Activity;
use App\Algorithm;
use App\CustomDiagnosis;
use App\Device;
use App\Diagnosis;
use App\DiagnosisReference;
use App\Drug;
use App\DrugReference;
use App\Enums\ClinicianChoice;
use App\Formulation;
use App\HealthFacility;
use App\Management;
use App\ManagementReference;
use App\MedicalCase;
use App\MedicalCaseAnswer;
use App\Patient;
use App\Version;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ExportCsvFlat extends ExportCsv
{
    protected static $DIAGNOSIS_NOT_PROPOSED = null;

    protected static $DIAGNOSIS_PROPOSED_AND_ACCEPTED = 'Accepted';

    protected static $DIAGNOSIS_PROPOSED_AND_REJECTED = 'Rejected';

    protected static $DIAGNOSIS_MANUALLY_ADDED = 'Manually added';

    protected static $VARIABLE_DEFAULT_VALUE = null;

    protected static $DRUG_NOT_PROPOSED = null;

    protected static $ANTIBIOTIC_PRESCRIBED = 0;

    protected static $DRUG_PROPOSED_AND_ACCEPTED = 'Accepted';

    protected static $DRUG_PROPOSED_AND_REJECTED = 'Rejected';

    protected static $DRUG_MANUALLY_ADDED = 'Manually added';

    protected static $MANAGEMENT_PROPOSED = 'Proposed';

    /**
     * @param Patient $patient
     * @param $consultation_date
     * @return array patient data
     */
    protected static function getPatientData(Patient $patient, $consultation_date): array
    {
        return [
            Config::get('csv.flat.identifiers.patient.dyn_pat_study_id_patient') => $patient->id,
            Config::get('csv.flat.identifiers.patient.dyn_pat_birth_date') => Carbon::parse($patient->birthdate)->diffInDays(Carbon::parse($consultation_date)),
            Config::get('csv.flat.identifiers.patient.dyn_cor_pat_birth_date') => Carbon::parse($patient->corrected_birthdate)->diffInDays(Carbon::parse($consultation_date)),
            Config::get('csv.flat.identifiers.patient.dyn_pat_gender') => $patient->gender,
            Config::get('csv.flat.identifiers.patient.dyn_pat_local_patient_id') => $patient->local_patient_id,
            Config::get('csv.flat.identifiers.patient.dyn_pat_consent') => $patient->consent,
            Config::get('csv.flat.identifiers.patient.dyn_pat_redcap') => $patient->redcap,
            Config::get('csv.flat.identifiers.patient.dyn_pat_duplicate') => $patient->duplicate,
            Config::get('csv.flat.identifiers.patient.dyn_pat_other_uid') => $patient->other_uid,
            Config::get('csv.flat.identifiers.patient.dyn_pat_other_study_id') => $patient->other_study_id,
            Config::get('csv.flat.identifiers.patient.dyn_pat_other_group_id') => $patient->other_group_id,
            Config::get('csv.flat.identifiers.patient.dyn_pat_merged_with') => $patient->merged_with,
            Config::get('csv.flat.identifiers.patient.dyn_pat_merged') => $patient->merged,
            Config::get('csv.flat.identifiers.patient.dyn_pat_is_drop') => $patient->is_drop,
            Config::get('csv.flat.identifiers.patient.dyn_pat_status') => $patient->status,
            Config::get('csv.flat.identifiers.patient.dyn_pat_related_ids') => $patient->related_ids,
            Config::get('csv.flat.identifiers.patient.dyn_pat_other_id') => $patient->other_id,
        ];
    }

    /**
     * @param MedicalCase $medical_case
     * @return array medical case data
     */
    protected static function getMedicalCaseData(MedicalCase $medical_case): array
    {
        if ($medical_case->birthdate) {
            $medical_case->birthdate = Carbon::parse($medical_case->birthdate)->diffInDays(Carbon::parse($medical_case->consultation_date));
        }

        if (empty($medical_case->corrected_bd5ageindays)) {
            $medical_case->corrected_bd5ageindays = Carbon::parse($medical_case->patient->corrected_birthdate)->diffInDays(Carbon::parse($medical_case->consultation_date));
        }

        return [
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_id') => $medical_case->id,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_local_medical_case_id') => $medical_case->local_medical_case_id,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_consent') => $medical_case->consent,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_isEligible') => $medical_case->isEligible,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_redcap') => $medical_case->redcap,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_consultation_date') => $medical_case->consultation_date,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_birthdate') => $medical_case->birthdate,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_age_in_days') => $medical_case->corrected_bd5ageindays,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_closedAt') => $medical_case->closedAt,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_force_close') => $medical_case->force_close,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_redcap_flag') => $medical_case->mc_redcap_flag,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_duplicate') => $medical_case->duplicate,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_is_drop') => $medical_case->is_drop,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_app_version') => $medical_case->app_version,
            Config::get('csv.flat.identifiers.medical_case.dyn_mc_blank_case') => $medical_case->blankcase,
        ];
    }

    protected static function getHealthFacilityData(HealthFacility $health_facility): array
    {
        return [
            Config::get('csv.flat.identifiers.health_facility.dyn_hfa_id') => $health_facility->id ?? '',
            Config::get('csv.flat.identifiers.health_facility.dyn_hfa_group_id') => $health_facility->group_id ?? '',
            Config::get('csv.flat.identifiers.health_facility.dyn_hfa_long') => $health_facility->long ?? '',
            Config::get('csv.flat.identifiers.health_facility.dyn_hfa_lat') => $health_facility->lat ?? '',
            Config::get('csv.flat.identifiers.health_facility.dyn_hfa_hf_mode') => $health_facility->hf_mode ?? '',
            Config::get('csv.flat.identifiers.health_facility.dyn_hfa_name') => $health_facility->name ?? '',
            Config::get('csv.flat.identifiers.health_facility.dyn_hfa_country') => $health_facility->country ?? '',
            Config::get('csv.flat.identifiers.health_facility.dyn_hfa_area') => $health_facility->area ?? '',
            Config::get('csv.flat.identifiers.health_facility.dyn_hfa_is_drop') => $health_facility->is_drop ?? '',
            Config::get('csv.flat.identifiers.health_facility.dyn_hfa_council') => $health_facility->council ?? '',
        ];
    }

    protected static function getDeviceData(?Device $device, $activity): array
    {
        return [
            Config::get('csv.flat.identifiers.device.dyn_device_id') => $device->id ?? '',
            Config::get('csv.flat.identifiers.device.dyn_device_name') => $device->name ?? '',
            Config::get('csv.flat.identifiers.device.dyn_device_health_facility_id') => $device->health_facility_id ?? '',
            Config::get('csv.flat.identifiers.device.dyn_device_health_worker') => $activity->clinician ?? '',
            Config::get('csv.flat.identifiers.device.dyn_device_mac_address') => $device->mac_address ?? '',
            Config::get('csv.flat.identifiers.device.dyn_device_last_seen') => $device->last_seen ?? '',
        ];
    }

    /**
     * @param  Version $version
     * @return array version data
     */
    protected static function getVersionData(Version $version): array
    {
        return [
            Config::get('csv.flat.identifiers.version.dyn_ver_id') => $version->id,
            Config::get('csv.flat.identifiers.version.dyn_ver_medal_c_id') => $version->medal_c_id,
            Config::get('csv.flat.identifiers.version.dyn_ver_name') => $version->name,
            Config::get('csv.flat.identifiers.version.dyn_ver_consent_management') => $version->consent_management,
            Config::get('csv.flat.identifiers.version.dyn_ver_study') => $version->study,
            Config::get('csv.flat.identifiers.version.dyn_ver_is_arm_control') => $version->is_arm_control,
        ];
    }

    /**
     * @param  Algorithm $algorithm
     * @return array algorithm data
     */
    protected static function getAlgorithmData(Algorithm $algorithm): array
    {
        return [
            Config::get('csv.flat.identifiers.algorithm.dyn_alg_id') => $algorithm->id,
            Config::get('csv.flat.identifiers.algorithm.dyn_alg_name') => $algorithm->name,
        ];
    }

    /**
     * @param  Activity $activity
     * @return array activity data
     */
    protected static function getActivityData(Activity $activity): array
    {
        return [
            Config::get('csv.flat.identifiers.activity.dyn_act_id') => $activity->id,
            Config::get('csv.flat.identifiers.activity.dyn_act_medical_case_id') => $activity->medical_case_id,
            Config::get('csv.flat.identifiers.activity.dyn_act_medal_c_id') => $activity->medal_c_id,
            Config::get('csv.flat.identifiers.activity.dyn_act_step') => $activity->step,
            Config::get('csv.flat.identifiers.activity.dyn_act_clinician') => $activity->clinician,
            Config::get('csv.flat.identifiers.activity.dyn_act_mac_address') => $activity->mac_address,
            Config::get('csv.flat.identifiers.activity.dyn_act_created_at') => $activity->created_at,
            Config::get('csv.flat.identifiers.activity.dyn_act_updated_at') => $activity->updated_at,
        ];
    }

    /**
     * @param  Diagnosis $diagnosis
     * @return array diagnosis data
     */
    protected static function getDiagnosisData(Diagnosis $diagnosis): array
    {
        return [
            Config::get('csv.flat.identifiers.diagnosis.dyn_dia_id') => $diagnosis->id,
            Config::get('csv.flat.identifiers.diagnosis.dyn_dia_medal_c_id') => $diagnosis->medal_c_id,
            Config::get('csv.flat.identifiers.diagnosis.dyn_dia_label') => $diagnosis->label,
            Config::get('csv.flat.identifiers.diagnosis.dyn_dia_diagnostic_id') => $diagnosis->diagnostic_id,
            Config::get('csv.flat.identifiers.diagnosis.dyn_dia_created_at') => $diagnosis->created_at,
            Config::get('csv.flat.identifiers.diagnosis.dyn_dia_updated_at') => $diagnosis->updated_at,
            Config::get('csv.flat.identifiers.diagnosis.dyn_dia_type') => $diagnosis->type,
            Config::get('csv.flat.identifiers.diagnosis.dyn_dia_version_id') => $diagnosis->version_id,
        ];
    }

    /**
     * @param  CustomDiagnosis $custom_diagnosis
     * @return array custom diagnosis data
     */
    protected static function getCustomDiagnosisData(CustomDiagnosis $custom_diagnosis): array
    {
        return [
            Config::get('csv.flat.identifiers.custom_diagnosis.dyn_cdi_id') => $custom_diagnosis->id,
            Config::get('csv.flat.identifiers.custom_diagnosis.dyn_cdi_label') => $custom_diagnosis->label,
            Config::get('csv.flat.identifiers.custom_diagnosis.dyn_cdi_drugs') => $custom_diagnosis->drugs,
            Config::get('csv.flat.identifiers.custom_diagnosis.dyn_cdi_created_at') => $custom_diagnosis->created_at,
            Config::get('csv.flat.identifiers.custom_diagnosis.dyn_cdi_updated_at') => $custom_diagnosis->updated_at,
            Config::get('csv.flat.identifiers.custom_diagnosis.dyn_cdi_medical_case_id') => $custom_diagnosis->medical_case_id,
        ];
    }

    /**
     * @param  DiagnosisReference $diagnosis_reference
     * @return array  diagnosis reference data
     */
    protected static function getDiagnosisReferenceData(DiagnosisReference $diagnosis_reference): array
    {
        return [
            Config::get('csv.flat.identifiers.diagnosis_reference.dyn_dre_id') => $diagnosis_reference->id,
            Config::get('csv.flat.identifiers.diagnosis_reference.dyn_dre_agreed') => $diagnosis_reference->agreed,
            Config::get('csv.flat.identifiers.diagnosis_reference.dyn_dre_additional') => $diagnosis_reference->additional,
            Config::get('csv.flat.identifiers.diagnosis_reference.dyn_dre_diagnosis_id') => $diagnosis_reference->diagnosis_id,
            Config::get('csv.flat.identifiers.diagnosis_reference.dyn_dre_medical_case_id') => $diagnosis_reference->medical_case_id,
            Config::get('csv.flat.identifiers.diagnosis_reference.dyn_dre_created_at') => $diagnosis_reference->created_at,
            Config::get('csv.flat.identifiers.diagnosis_reference.dyn_dre_updated_at') => $diagnosis_reference->updated_at,
        ];
    }

    /**
     * @param  Drug $drug
     * @return array drug data
     */
    protected static function getDrugData(Drug $drug): array
    {
        return [
            Config::get('csv.flat.identifiers.drug.dyn_dru_id') => $drug->id,
            Config::get('csv.flat.identifiers.drug.dyn_dru_medal_c_id') => $drug->medal_c_id,
            Config::get('csv.flat.identifiers.drug.dyn_dru_type') => $drug->type,
            Config::get('csv.flat.identifiers.drug.dyn_dru_label') => $drug->label,
            Config::get('csv.flat.identifiers.drug.dyn_dru_description') => $drug->description,
            Config::get('csv.flat.identifiers.drug.dyn_dru_diagnosis_id') => $drug->diagnosis_id,
            Config::get('csv.flat.identifiers.drug.dyn_dru_created_at') => $drug->created_at,
            Config::get('csv.flat.identifiers.drug.dyn_dru_updated_at') => $drug->updated_at,
            Config::get('csv.flat.identifiers.drug.dyn_dru_is_anti_malarial') => $drug->is_anti_malarial,
            Config::get('csv.flat.identifiers.drug.dyn_dru_is_antibiotic') => $drug->is_antibiotic,
            Config::get('csv.flat.identifiers.drug.dyn_dru_duration') => $drug->duration,
        ];
    }

    /**
     * @param  DrugReference $drug_reference
     * @return array drug reference data
     */
    protected static function getDrugReferenceData(DrugReference $drug_reference): array
    {
        return [
            Config::get('csv.flat.identifiers.drug_reference.dyn_dre_id') => $drug_reference->id,
            Config::get('csv.flat.identifiers.drug_reference.dyn_dre_drug_id') => $drug_reference->drug_id,
            Config::get('csv.flat.identifiers.drug_reference.dyn_dre_diagnosis_id') => $drug_reference->diagnosis_id ?? '',
            Config::get('csv.flat.identifiers.drug_reference.dyn_dre_custom_diagnosis_id') => $drug_reference->custom_diagnosis_id ?? '',
            Config::get('csv.flat.identifiers.drug_reference.dyn_dre_agreed') => $drug_reference->agreed,
            Config::get('csv.flat.identifiers.drug_reference.dyn_dre_created_at') => $drug_reference->created_at,
            Config::get('csv.flat.identifiers.drug_reference.dyn_dre_updated_at') => $drug_reference->updated_at,
            Config::get('csv.flat.identifiers.drug_reference.dyn_dre_formulationSelected') => $drug_reference->formulationSelected,
            Config::get('csv.flat.identifiers.drug_reference.dyn_dre_formulation_id') => $drug_reference->formulation_id,
            Config::get('csv.flat.identifiers.drug_reference.dyn_dre_additional') => $drug_reference->additional,
            Config::get('csv.flat.identifiers.drug_reference.dyn_dre_duration') => $drug_reference->duration,
        ];
    }

    /**
     * @param  Management $management
     * @return array management data
     */
    protected static function getManagementData(Management $management): array
    {
        return [
            Config::get('csv.flat.identifiers.management.dyn_man_id') => $management->id,
            Config::get('csv.flat.identifiers.management.dyn_man_drug_id') => $management->drug_id,
            Config::get('csv.flat.identifiers.management.dyn_man_diagnosis_id') => $management->diagnosis_id,
            Config::get('csv.flat.identifiers.management.dyn_man_agreed') => $management->agreed,
            Config::get('csv.flat.identifiers.management.dyn_man_created_at') => $management->created_at,
            Config::get('csv.flat.identifiers.management.dyn_man_updated_at') => $management->updated_at,
            Config::get('csv.flat.identifiers.management.dyn_man_formulationSelected') => $management->formulationSelected,
            Config::get('csv.flat.identifiers.management.dyn_man_formulation_id') => $management->formulation_id,
            Config::get('csv.flat.identifiers.management.dyn_man_additional') => $management->additional,
            Config::get('csv.flat.identifiers.management.dyn_man_duration') => $management->duration,
        ];
    }

    /**
     * @param  ManagementReference $management_reference
     * @return array management reference data
     */
    protected static function getManagementReferenceData(ManagementReference $management_reference): array
    {
        return [
            Config::get('csv.flat.identifiers.management_reference.dyn_mre_id') => $management_reference->id,
            Config::get('csv.flat.identifiers.management_reference.dyn_mre_agreed') => $management_reference->agreed,
            Config::get('csv.flat.identifiers.management_reference.dyn_mre_diagnosis_id') => $management_reference->diagnosis_id,
            Config::get('csv.flat.identifiers.management_reference.dyn_mre_created_at') => $management_reference->created_at,
            Config::get('csv.flat.identifiers.management_reference.dyn_mre_updated_at') => $management_reference->updated_at,
            Config::get('csv.flat.identifiers.management_reference.dyn_mre_management_id') => $management_reference->management_id,
        ];
    }

    /**
     * @param  Formulation $formulation
     * @return array formulation data
     */
    protected static function getFormulationData(Formulation $formulation): array
    {
        return [
            Config::get('csv.flat.identifiers.formulation.dyn_for_id') => $formulation->id,
            Config::get('csv.flat.identifiers.formulation.dyn_for_medical_form') => $formulation->medical_form,
            Config::get('csv.flat.identifiers.formulation.dyn_for_administration_route_name') => $formulation->administration_route_name,
            Config::get('csv.flat.identifiers.formulation.dyn_for_liquid_concentration') => $formulation->liquid_concentration,
            Config::get('csv.flat.identifiers.formulation.dyn_for_dose_form') => $formulation->dose_form,
            Config::get('csv.flat.identifiers.formulation.dyn_for_unique_dose') => $formulation->unique_dose,
            Config::get('csv.flat.identifiers.formulation.dyn_for_by_age') => $formulation->by_age,
            Config::get('csv.flat.identifiers.formulation.dyn_for_minimal_dose_per_kg') => $formulation->minimal_dose_per_kg,
            Config::get('csv.flat.identifiers.formulation.dyn_for_maximal_dose_per_kg') => $formulation->maximal_dose_per_kg,
            Config::get('csv.flat.identifiers.formulation.dyn_for_maximal_dose') => $formulation->maximal_dose,
            Config::get('csv.flat.identifiers.formulation.dyn_for_description') => $formulation->description,
            Config::get('csv.flat.identifiers.formulation.dyn_for_doses_per_day') => $formulation->doses_per_day,
            Config::get('csv.flat.identifiers.formulation.dyn_for_created_at') => $formulation->created_at,
            Config::get('csv.flat.identifiers.formulation.dyn_for_updated_at') => $formulation->updated_at,
            Config::get('csv.flat.identifiers.formulation.dyn_for_drug_id') => $formulation->drug_id,
            Config::get('csv.flat.identifiers.formulation.dyn_for_administration_route_category') => $formulation->administration_route_category,
            Config::get('csv.flat.identifiers.formulation.dyn_for_medal_c_id') => $formulation->medal_c_id,
        ];
    }

    /**
     * Adds a medical case to the data array.
     */
    protected function addPatientData(&$data, $index, $patient, $consultation_date): void
    {
        $data[$index] = array_merge($data[$index], $this->getPatientData($patient, $consultation_date));
        if ($this->is_first_med_case) {
            $data[0] = array_merge($data[0], $this->getAttributeList(Config::get('csv.flat.identifiers.patient')));
        }
    }

    /**
     * Adds a health facility to the data array.
     */
    protected function addHealthFacilityData(&$data, $index, $health_facility): void
    {
        $data[$index] = array_merge($data[$index], $this->getHealthFacilityData($health_facility));
        if ($this->is_first_med_case) {
            $data[0] = array_merge($data[0], $this->getAttributeList(Config::get('csv.flat.identifiers.health_facility')));
        }
    }

    /**
     * Adds a device to the data array.
     */
    protected function addDeviceData(&$data, $index, $device, $activity): void
    {
        $data[$index] = array_merge($data[$index], $this->getDeviceData($device, $activity));

        if ($this->is_first_med_case) {
            $data[0] = array_merge($data[0], $this->getAttributeList(Config::get('csv.flat.identifiers.device')));
        }
    }

    /**
     * Adds a medical case to the data array.
     */
    protected function addMedicalCaseData(&$data, $index, $medical_case): void
    {
        $data[$index] = array_merge($data[$index], $this->getMedicalCaseData($medical_case));
        if ($this->is_first_med_case) {
            $data[0] = array_merge($data[0], $this->getAttributeList(Config::get('csv.flat.identifiers.medical_case')));
        }
    }

    /**
     * @return array the list of default variables' values
     */
    protected static function getVariableDefaultValues($node_objs): array
    {
        $nodes_values = [];
        foreach ($node_objs as $node_obj) {
            $nodes_values[$node_obj->id] = self::$VARIABLE_DEFAULT_VALUE;
        }

        return $nodes_values;
    }

    /**
     * @return array the list of variables' labels
     */
    protected static function getVariableLabels($node_objs, $version_node_names): array
    {
        $labels = [];
        $found = false;

        $mapping_category = [
            'unique_triage_question' => 'UT -',
            'chronic_condition' => 'CH -',
            'treatment_question' => 'TQ -',
            'basic_demographic' => 'BD -',
            'demographic' => 'D -',
            'complaint_category' => 'CC -',
            'basic_measurement' => 'BM -',
            'answerable_basic_measurement' => 'BM -',
            'assessment_test' => 'A -',
            'background_calculation' => 'BC -',
            'observed_physical_sign' => 'OS -',
            'exposure' => 'E -',
            'symptom' => 'S -',
            'physical_exam' => 'PE -',
            'referral' => 'R -',
            'vaccine' => 'V -',
            'predefined_syndrome' => 'PS -',
        ];

        foreach ($node_objs as $node_obj) {
            $label = $node_obj->label;
            $category = $node_obj->category;
            $medal_c_id = $node_obj->medal_c_id;

            if (stripos($node_obj->id, '.') !== false) {
                $medal_c_id .= ' categorical';
            }
            if (stripos($node_obj->id, 'estimated') !== false) {
                $medal_c_id .= ' estimated';
            }
            foreach ($version_node_names as $nodes) {
                if (array_key_exists($node_obj->medal_c_id . trim($label), $nodes)) {
                    $label = $nodes[$node_obj->medal_c_id . trim($label)];
                    $found = true;
                }
            }
            if (!$found) {
                $label = "$mapping_category[$category] $label";
            }

            $labels[] = $label . ' - ' . $medal_c_id;
            $found = false;
        }

        return $labels;
    }

    /**
     * Adds a medical case answer to the data array.
     */
    protected function addMedicalCaseAnswerData(&$data, $index, $medical_case_answers, $version_node_names)
    {
        $mc_answers_with_answer_and_value = Cache::store('array')->remember('continous_medical_case_answers', 10800, function () {
            return MedicalCaseAnswer::select('node_id')->whereNotNull('answer_id')->where('value', '!=', '')->groupBy('node_id')->pluck('node_id');
        });

        $mc_answers_with_estimated_value = Cache::store('array')->remember('estimated_value_medical_case_answers', 10800, function () {
            return MedicalCaseAnswer::select('node_id')->whereNotNull('value')->where('value', 'ILIKE', '%_estimated%')->groupBy('node_id')->pluck('node_id');
        });

        $node_objs = Cache::store('array')->remember('node_objs', 10800, function () use ($mc_answers_with_answer_and_value, $mc_answers_with_estimated_value) {
            $nodes = DB::table('nodes')->select('id', 'label', 'category', 'medal_c_id')->get();

            foreach ($mc_answers_with_answer_and_value as $mc_answer_with_answer_and_value) {
                $node_to_duplicate = $nodes->where('id', $mc_answer_with_answer_and_value)->first();
                $new_node = clone $node_to_duplicate;
                $new_node->id = $new_node->id . '.5';
                $nodes->push($new_node);
            }

            foreach ($mc_answers_with_estimated_value as $mc_answer_with_estimated_value) {
                $node_to_duplicate = $nodes->where('id', $mc_answer_with_estimated_value)->first();
                $new_node = clone $node_to_duplicate;
                $new_node->id = $new_node->id . 'estimated';
                $nodes->push($new_node);
            }

            return $nodes->sort();
        });

        $nodes_values = Cache::store('array')->remember('nodes_values', 10800, function () use ($node_objs) {
            return self::getVariableDefaultValues($node_objs);
        });

        foreach ($medical_case_answers as $medical_case_answer) {
            if (self::isSkippedMedicalCaseAnswer($medical_case_answer)) {
                continue;
            }

            $node_id = $medical_case_answer->node_id;

            if ($medical_case_answer->answer_id !== null && $medical_case_answer->value !== null && $medical_case_answer->value !== '') {
                $nodes_values[$node_id . '.5'] = $medical_case_answer->answer->label ?? null;
                $nodes_values[$node_id] = $medical_case_answer->value ?? null;
            } elseif ($medical_case_answer->answer) {
                if (array_key_exists($node_id . '.5', $nodes_values)) {
                    $nodes_values[$node_id . '.5'] = $medical_case_answer->answer->label ?? null;
                } else {
                    $nodes_values[$node_id] = $medical_case_answer->answer->label ?? null;
                }
            } else {
                if (array_key_exists($node_id . 'estimated', $nodes_values)) {
                    if (strpos($medical_case_answer->value, "_measured") !== false) {
                        $nodes_values[$node_id] = str_replace("_measured", "", $medical_case_answer->value ?? null);
                    } else {
                        $nodes_values[$node_id . 'estimated'] = str_replace("_estimated", "", $medical_case_answer->value ?? null);
                    }
                } else {
                    $nodes_values[$node_id] = $medical_case_answer->value ?? null;
                }
            }
        }

        $data[$index] = array_merge($data[$index], $nodes_values);

        // add labels
        if ($this->is_first_med_case) {
            $labels = self::getVariableLabels($node_objs, $version_node_names);
            $data[0] = array_merge($data[0], $labels);
        }
    }

    /**
     * Adds a version answer to the data array.
     */
    protected function addVersionData(&$data, $index, $version): void
    {
        $data[$index] = array_merge($data[$index], $this->getVersionData($version));
        if ($this->is_first_med_case) {
            $data[0] = array_merge($data[0], $this->getAttributeList(Config::get('csv.flat.identifiers.version')));
        }
    }

    /**
     * Adds an algorithm to the data array.
     */
    protected function addAlgorithmData(&$data, $index, $algorithm): void
    {
        $data[$index] = array_merge($data[$index], $this->getAlgorithmData($algorithm));
        if ($this->is_first_med_case) {
            $data[0] = array_merge($data[0], $this->getAttributeList(Config::get('csv.flat.identifiers.algorithm')));
        }
    }

    /**
     * @return array the list of default diagnosis' values
     */
    protected static function getDiagnosisDefaultValues($diagnosis_objs, $version_node_names): array
    {
        $diagnosis_values = [];

        foreach ($diagnosis_objs as $diagnosis_obj) {

            $reference = '';
            $diagnosis_label = rtrim(strtolower($diagnosis_obj->label));

            foreach ($version_node_names as $nodes) {
                if (
                    array_key_exists('final_diagnoses', $nodes)
                    && array_key_exists($diagnosis_obj->medal_c_id, $nodes['final_diagnoses'])
                ) {
                    $reference = $nodes['final_diagnoses'][$diagnosis_obj->medal_c_id];
                }
            }

            $diagnosis_values["DF$reference - $diagnosis_label - $diagnosis_obj->medal_c_id"] = self::$DIAGNOSIS_NOT_PROPOSED;
        }
        return $diagnosis_values;
    }

    /**
     * @return array the list of diagnosis' labels
     */
    protected static function getDiagnosisLabels($diagnosis_objs): array
    {
        $labels = [];
        foreach ($diagnosis_objs as $diagnosis_obj) {
            $labels[] = 'DF - ' . $diagnosis_obj->label . ' - ' . $diagnosis_obj->medal_c_id;
        }

        return $labels;
    }

    /**
     * Adds diagnoses to the data array.
     */
    protected function addDiagnosesData(&$data, $index, $diagnosis_references, $version_node_names): void
    {
        $reference = '';

        $diagnosis_objs = Cache::store('array')->remember('diagnosis_objs', 10800, function () {
            return DB::table('diagnoses')->select('id', 'label', 'medal_c_id')->get();
        });

        $diagnosis_values = Cache::store('array')->remember('diagnosis_values', 10800, function () use ($diagnosis_objs, $version_node_names) {
            return self::getDiagnosisDefaultValues($diagnosis_objs, $version_node_names);
        });

        foreach ($diagnosis_references as $diagnosis_reference) {
            $medal_c_id = $diagnosis_reference->diagnoses->medal_c_id;
            $diagnosis_label = rtrim(strtolower($diagnosis_reference->diagnoses->label));

            foreach ($version_node_names as $nodes) {
                if (
                    array_key_exists('final_diagnoses', $nodes)
                    && array_key_exists($medal_c_id, $nodes['final_diagnoses'])
                ) {
                    $reference = $nodes['final_diagnoses'][$medal_c_id];
                }
            }

            if ($diagnosis_reference->status->is(ClinicianChoice::ADDITIONAL())) {
                $diagnosis_values["DF$reference - $diagnosis_label - $medal_c_id"] = self::$DIAGNOSIS_MANUALLY_ADDED;
            } else {
                $diagnosis_values["DF$reference - $diagnosis_label - $medal_c_id"] = $diagnosis_reference->status->is(ClinicianChoice::AGREED())
                    ? self::$DIAGNOSIS_PROPOSED_AND_ACCEPTED
                    : self::$DIAGNOSIS_PROPOSED_AND_REJECTED;
            }
        }

        $data[$index] = array_merge($data[$index], $diagnosis_values);

        // add labels
        if ($this->is_first_med_case) {
            $data[0] = array_merge($data[0], array_keys($diagnosis_values));
        }
    }

    /**
     * Adds managements to the data array.
     * $data, $index, $management_references
     */
    protected function addManagementsData(&$data, $index, $diagnosis_references): void
    {
        $managements = Cache::store('array')->remember('management_objs', 10800, function () {
            return DB::table('managements')->select('id', 'label', 'medal_c_id')->get();
        });

        $management_values = Cache::store('array')->remember('management_values', 10800, function () use ($managements) {
            return self::getManagementDefaultValues($managements);
        });

        foreach ($diagnosis_references as $diagnosis_reference) {
            $management_references = $diagnosis_reference->management_references;
            foreach ($management_references as $management_reference) {
                $medal_c_id = $management_reference->managements->medal_c_id;
                $managements_label = rtrim($management_reference->managements->label);
                $management_values["$managements_label - $medal_c_id"] = self::$MANAGEMENT_PROPOSED;
            }
        }

        $data[$index] = array_merge($data[$index], $management_values);

        // add labels
        if ($this->is_first_med_case) {
            $data[0] = array_merge($data[0], array_keys($management_values));
        }
    }


    /**
     * @return array the list of default variables' values
     */
    protected static function getManagementDefaultValues($managements): array
    {
        $values = [];

        foreach ($managements as $management) {
            $medal_c_id = $management->medal_c_id;
            $managements_label = rtrim($management->label);
            $values["$managements_label - $medal_c_id"] = self::$VARIABLE_DEFAULT_VALUE;
        }

        return $values;
    }

    /**
     * Adds custom diagnoses to the data array.
     */
    protected function addCustomDiagnosesData(&$data, $index, $custom_diagnoses): void
    {
        $table_custom_diagnoses[] = $custom_diagnoses->pluck('label')->implode(', ');

        $list_custom_diagnoses = implode(', ', $table_custom_diagnoses);

        $data[$index][] = $list_custom_diagnoses;
        // add labels
        if ($this->is_first_med_case) {
            $data[0][] = config('csv.header_custom_diagnoses');
        }
    }

    /**
     * @return array the list of default drugs' values
     */
    protected static function getDrugDefaultValues($drug_objs): array
    {
        $drug_values = [];
        foreach ($drug_objs as $drug_obj) {
            $drug_label = rtrim(strtolower($drug_obj->label));
            $drug_values["DRUG - $drug_label - $drug_obj->medal_c_id"] = self::$DRUG_NOT_PROPOSED;
        }

        // Rainer need : One bool column to say if antiobiotic/antimalarial were prescribed or not
        $drug_values['DRUG - Antibiotic prescribed ?'] = self::$ANTIBIOTIC_PRESCRIBED;
        $drug_values['DRUG - Antimalarial prescribed ?'] = self::$ANTIBIOTIC_PRESCRIBED;

        return $drug_values;
    }

    /**
     * Adds drugs to the data array.
     */
    protected function addDrugData(&$data, $index, $diagnosis_references, $custom_diagnoses): void
    {
        $drug_objs = Cache::store('array')->remember('drug_objs', 10800, function () {
            return DB::table('drugs')->select('medal_c_id', 'id', 'label')->get();
        });

        $drug_values = Cache::store('array')->remember('drug_values', 10800, function () use ($drug_objs) {
            return self::getDrugDefaultValues($drug_objs);
        });

        foreach ($diagnosis_references as $diagnosis_reference) {
            $drug_references = $diagnosis_reference->drug_references;
            foreach ($drug_references as $drug_reference) {
                $medal_c_id = $drug_reference->drugs->medal_c_id;
                $drug_label = rtrim(strtolower($drug_reference->drugs->label));

                if ($drug_reference->status->is(ClinicianChoice::ADDITIONAL())) {
                    $drug_values["DRUG - $drug_label - $medal_c_id"] = self::$DRUG_MANUALLY_ADDED;
                } else {
                    $drug_values["DRUG - $drug_label - $medal_c_id"] = $drug_reference->status->is(ClinicianChoice::AGREED())
                        ? self::$DRUG_PROPOSED_AND_ACCEPTED
                        : self::$DRUG_PROPOSED_AND_REJECTED;
                }

                if ($drug_values['DRUG - Antibiotic prescribed ?'] === 0 && $drug_reference->drugs->is_antibiotic) {
                    $drug_values['DRUG - Antibiotic prescribed ?'] = 1;
                }
                if ($drug_values['DRUG - Antimalarial prescribed ?'] === 0 && $drug_reference->drugs->is_antimalarial) {
                    $drug_values['DRUG - Antimalarial prescribed ?'] = 1;
                }
            }
        }

        foreach ($custom_diagnoses as $custom_diagnose) {
            $drugs_references = $custom_diagnose->drugs_references;
            foreach ($drugs_references as $drug_reference) {
                $drug_label = rtrim(strtolower($drug_reference->drugs->label));
                $drug_mcid = $drug_reference->drugs->medal_c_id;
                $drug_values["DRUG - $drug_label - $drug_mcid"] = self::$DRUG_PROPOSED_AND_ACCEPTED;
                if ($drug_values['DRUG - Antibiotic prescribed ?'] === 0 && $drug_reference->drugs->is_antibiotic) {
                    $drug_values['DRUG - Antibiotic prescribed ?'] = 1;
                }
                if ($drug_values['DRUG - Antimalarial prescribed ?'] === 0 && $drug_reference->drugs->is_antimalarial) {
                    $drug_values['DRUG - Antimalarial prescribed ?'] = 1;
                }
            }
        }

        $data[$index] = array_merge($data[$index], $drug_values);

        // add labels when first chunk
        if ($this->is_first_med_case) {
            $data[0] = array_merge($data[0], array_keys($drug_values));
        }
    }

    /**
     * Adds custom drugs to the data array.
     */
    protected function addCustomDrugData(&$data, $index, $diagnosis_references, $custom_diagnoses): void
    {
        $table_custom_drugs = [];

        foreach ($diagnosis_references as $diagnosis_reference) {
            $custom_drug = $diagnosis_reference->custom_drugs()->get();
            if (!$custom_drug->isEmpty()) {
                $table_custom_drugs[] = $custom_drug->pluck('name')->implode(', ');
            }
        }

        foreach ($custom_diagnoses as $custom_diagnosis) {
            $table_custom_drugs[] = $custom_diagnosis->custom_drugs->pluck('name')->implode(', ');
        }

        $list_custom_drugs = implode(', ', $table_custom_drugs);

        $data[$index][] = $list_custom_drugs;
        // add labels
        if ($this->is_first_med_case) {
            $data[0][] = config('csv.header_custom_drug');
        }
    }

    /**
     * Adds all children to the variables data. Recursive.
     */
    protected function addChildrenToArray(&$data, $json_children, $id): void
    {
        foreach ($json_children as $json_child) {
            if ($json_child) {
                if (array_key_exists('children', $json_child)) {
                    $json_child_children = $json_child['children'];
                    $this->addChildrenToArray($data, $json_child_children, $id); // recursive search
                } else {
                    $variable_name = trim(strstr($json_child['title'], ' '));
                    $variable_name = trim(strstr($variable_name, ' '));
                    $data[$id][$json_child['id'] . $variable_name] = $json_child['title'];
                }
            }
        }
    }

    /**
     * Get the informations of nodes references for every version.
     * Messy function to retrieve the variables' references. Should be a specific field in the future.
     */
    protected function getJsonNodesInfo()
    {
        return Cache::store('array')->remember('versions_data', 10800, function () {
            $versions = Version::all();

            $versions_data = [];
            foreach ($versions as $version) {
                $id = $version->medal_c_id;
                $versions_data[$id] = [];

                ini_set('allow_url_fopen', 1);
                $json = file_get_contents(Config::get('medal.urls.creator_algorithm_url') . $version->medal_c_id);
                $obj = json_decode($json);
                $json_steps = json_decode($obj->full_order_json, true);
                foreach ($json_steps as $json_step) {
                    $json_children = $json_step['children'];
                    $this->addChildrenToArray($versions_data, $json_children, $id);
                }
                $dfs = $obj->medal_r_json->final_diagnoses;
                foreach ($dfs as $df) {
                    if (property_exists($df, 'reference')) {
                        $versions_data[$id]['final_diagnoses'][$df->id] = $df->reference;
                    }
                }
            }

            return $versions_data;
        });
    }

    /**
     * Retrieve all the data.
     */
    protected function getDataFromMedicalCases(): array
    {
        $data = [];
        if ($this->is_first_med_case) {
            $data[] = []; // list of attributes.
        }

        $version_node_names = $this->getJsonNodesInfo();

        foreach ($this->medical_cases as $medical_case) {
            $patient = $medical_case->patient;
            $index = $patient->id . '-' . $medical_case->id;
            $data[$index] = [];
            // get medical case data
            $this->addMedicalCaseData($data, $index, $medical_case);

            // get patient data
            $this->addPatientData($data, $index, $patient, $medical_case->consultation_date);

            $health_facility = $medical_case->facility;

            // get health facility
            $this->addHealthFacilityData($data, $index, $health_facility);

            $activity = $medical_case
                ->activities()
                ->whereNotNull('device_id')
                ->where('device_id', '<>', '')
                ->get()
                ->last();
            $device = optional($activity)->device;

            // get device
            $this->addDeviceData($data, $index, $device, $activity);

            $version = $medical_case->version;
            // get version data
            $this->addVersionData($data, $index, $version);

            $algorithm = $version->algorithm;
            // get algorithm data
            $this->addAlgorithmData($data, $index, $algorithm);

            $medical_case_answers = $medical_case->medical_case_answers;

            // get medical case answer data
            $this->addMedicalCaseAnswerData($data, $index, $medical_case_answers, $version_node_names);

            $diagnosis_references = $medical_case->diagnoses_references;
            // get diagnosis data
            $this->addDiagnosesData($data, $index, $diagnosis_references, $version_node_names);

            // get management data
            $this->addManagementsData($data, $index, $diagnosis_references);

            $custom_diagnoses = $medical_case->custom_diagnoses;
            // get custom diagnosis data
            $this->addCustomDiagnosesData($data, $index, $custom_diagnoses);

            // get drug data
            $this->addDrugData($data, $index, $diagnosis_references, $custom_diagnoses);

            // get custom drug data
            $this->addCustomDrugData($data, $index, $diagnosis_references, $custom_diagnoses);

            $this->is_first_med_case = false;
        }

        return $data;
    }

    public function export()
    {
        $data = $this->getDataFromMedicalCases();

        $folder = storage_path('app/export/' . Config::get('csv.flat.folder'));
        if (!File::exists($folder)) {
            File::makeDirectory($folder);
        }
        $file = fopen($folder . 'answers.csv', 'a+');
        if ($this->is_first_med_case) {
            fwrite($file, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF))); // UTF-8 encoding
        }
        foreach ($data as $line) {
            $attributes = $this->attributesToStr((array) $line);
            fputcsv($file, $attributes);
        }
        fclose($file);
    }
}
