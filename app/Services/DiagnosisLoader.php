<?php

namespace App\Services;

use App\Diagnosis;

class DiagnosisLoader extends ModelLoader
{
    protected $diagnosisData;

    protected $version;

    /**
     * Constructor.
     *
     * @param array $drugData
     * @param Version $version
     */
    public function __construct($diagnosisData, $version)
    {
        parent::__construct($diagnosisData);
        $this->diagnosisData = $diagnosisData;
        $this->version = $version;
    }

    protected function getValues()
    {
        return array_merge(parent::getValues(), [
            'version_id' => $this->version->id,
        ]);
    }

    protected function model()
    {
        return Diagnosis::class;
    }

    protected function configName()
    {
        return 'diagnosis';
    }

    /**
     * Create a model instance based on the data that was provided.
     *
     * @return Diagnosis
     */
    public function load()
    {
        $record = $this->model()::updateOrCreate(
            $this->getKeys(),
            $this->getValues()
        );

        $record->save();

        return $record;
    }
}
