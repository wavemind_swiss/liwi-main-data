<?php

namespace App\Services;

use App\Diagnosis;
use App\ExcludedDiagnosesList;
use App\MedicalCase;
use DomainException;
use Exception;
use Illuminate\Support\Collection;

class ExcludedDiagnosesListLoader extends ModelLoader
{
    /**
     * @var array
     */
    protected $excludedDiagnosesListData;

    /**
     * @var MedicalCase
     */
    protected $medicalCase;

    /**
     * Constructor.
     *
     * @param array $excludedDiagnosesListData
     * @param MedicalCase $medicalCase
     */
    public function __construct(array $excludedDiagnosesListData, MedicalCase $medicalCase)
    {
        parent::__construct($excludedDiagnosesListData);
        $this->excludedDiagnosesListData = $excludedDiagnosesListData;
        $this->medicalCase = $medicalCase;
    }

    protected function getKeys()
    {
        return [
            'medical_case_id' => $this->medicalCase->id,
        ];
    }

    /**
     * @throws Exception
     */
    protected function getValues()
    {
        /** @var Collection $diagnoses */
        $diagnoses = Diagnosis::whereIn('medal_c_id', $this->excludedDiagnosesListData)
            ->get(['id', 'medal_c_id'])
            ->keyBy('medal_c_id');

        // Retrieve diagnosis id from medal data according to medal_c_id.
        $diagnosesIds = collect($this->excludedDiagnosesListData)->map(function ($medalCreatorId) use ($diagnoses) {
            $diagnosis = $diagnoses->get($medalCreatorId);

            if (!$diagnosis) {
                throw new DomainException("medal_c_id '$medalCreatorId' has no corresponding diagnosis in the database.");
            }

            return $diagnosis->id;
        });

        return [
            'excluded_diagnoses' => $diagnosesIds,
        ];
    }

    protected function model()
    {
        return ExcludedDiagnosesList::class;
    }

    protected function configName()
    {
        return 'excluded_diagnoses_list';
    }
}
