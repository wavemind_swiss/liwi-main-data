<?php

namespace App\Services;

use App\Answer;
use App\MedicalCase;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class MedicalCaseLoader extends ModelLoader
{
    protected $caseData;

    protected $patient;

    /**
     * Constructor.
     *
     * @param object $caseData
     * @param Patient $patient
     * @param Version $version
     */
    public function __construct($caseData, $patient, $version, $config)
    {
        parent::__construct($caseData);
        $this->caseData = $caseData;
        $this->patient = $patient;
        $this->version = $version;
        $this->config = $config;
    }

    protected function getValues()
    {
        return array_merge(parent::getValues(), [
            'patient_id' => $this->patient->id,
            'version_id' => $this->version->id,
            'consultation_date' => Carbon::createFromTimestampMs($this->caseData['createdAt']),
            'closedAt' => Carbon::createFromTimestampMs($this->caseData['closedAt']),
            'group_id' => $this->caseData['patient']['group_id'],
            'force_close' => $this->caseData['forceClosed'] ?? false,
            'app_version' => $this->caseData['appVersion'] ?? null,
            'git_version' => $this->caseData['gitVersion'] ?? null,
            // By default the consent is true
            // We check for project with the new consent management
            // If not found then it's true
            'consent' => $this->consentAnswerOrDefault('consent_question_id', true),
        ]);
    }

    protected function model()
    {
        return MedicalCase::class;
    }

    protected function configName()
    {
        return 'medical_case';
    }

    protected function consentAnswerOrDefault($configKey, $default)
    {
        if (isset($this->config->config[$configKey])) {
            $nodeKey = $this->config->config[$configKey];
        } else {
            $nodeKey = null;
        }

        if (isset($this->caseData['nodes'][$nodeKey])) {
            $answer = Answer::where('medal_c_id', $this->caseData['nodes'][$nodeKey]['answer'])->first();
            if ($answer) {
                return Str::contains($answer->label, 'Yes');
            }
        }

        return $default;
    }
}
