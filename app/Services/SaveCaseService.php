<?php

namespace App\Services;

use App\Answer;
use App\CustomDiagnosis;
use App\Diagnosis;
use App\DiagnosisReference;
use App\Drug;
use App\Enums\ClinicianChoice;
use App\Formulation;
use App\HealthFacility;
use App\Management;
use App\MedicalCase;
use App\Node;
use App\Patient;
use App\PatientConfig;
use App\Version;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use InvalidArgumentException;
use UnexpectedValueException;

class SaveCaseService
{
    protected $caseData;

    protected $algorithm;

    protected $version;

    protected $patientConfig;

    /**
     * Save a medical case on the database.
     *
     * @param object $caseData
     * @return MedicalCase
     */
    public function save($caseData)
    {
        self::checkHasProperties($caseData, ['patient', 'nodes', 'diagnosis', 'version_id']);
        self::checkHasProperties($caseData['patient'], ['group_id']);

        $hf = $this->updateHf($caseData['patient']['group_id']);

        $versionId = $caseData['version_id'];

        $version = Version::where('medal_c_id', $versionId)->first();
        $config = null;
        if ($version) {
            $config = PatientConfig::where('version_id', $version->id)->first();
        } else {
            $data = $this->getVersionData($hf, $versionId);
            $versionData = $data['medal_r_json'];
            $configData = $this->getPatientConfigData($versionId);
            $version = $this->updateVersion($versionData);
            $config = $this->updateConfig($configData, $version);
        }
        $patient = $this->savePatient($caseData, $config, $version);
        $case = $this->saveCase($caseData, $version, $patient, $config);

        return $case;
    }

    public function getPatientConfigData($versionId)
    {
        $data = Http::get(Config::get('medal.creator.url') . Config::get('medal.creator.medal_data_config_endpoint') . $versionId);

        return json_decode($data['content'], true);
    }

    public function getVersionData($hf, $versionId)
    {
        if (Config::get('medal.global.local_health_facility_management')) {
            $versionJson = $hf->versionJson;
            if ($versionJson === null) {
                throw new UnexpectedValueException("Health facility $hf->group_id has no associated version");
            }

            return json_decode($versionJson->json, true);
        } else {
            $data = Http::get(Config::get('medal.urls.creator_algorithm_url') . $versionId);

            return json_decode($data['content'], true);
        }
    }

    /**
     * Update the patient config from medalc.
     *
     * @param Version $version
     * @return PatientConfig
     */
    public function updateConfig($configData, $version)
    {
        return (new PatientConfigLoader($configData, $version))->load();
    }

    /**
     * Retrieve the health facility from the database.
     *
     * @param int $groupId
     * @return HealthFacility
     */
    public function updateHf($groupId)
    {
        $hf = HealthFacility::where('group_id', $groupId)->first();

        if ($hf === null) {
            if (Config::get('medal.global.local_health_facility_management')) {
                throw new UnexpectedValueException("Health facility with group_id $groupId not found in database");
            } else {
                $data = Http::get(Config::get('medal.urls.creator_health_facility_url') . $groupId);
                $hfData = json_decode($data['content'], true);
                $hf = (new HealthFacilityLoader($hfData))->load();
            }
        }

        return $hf;
    }

    /**
     * Update the algorithm and the version from medalc if necessary.
     *
     * @param int $versionId
     * @return Version
     */
    public function updateVersion($algorithmData)
    {
        self::checkHasProperties($algorithmData, ['nodes', 'final_diagnoses', 'health_cares']);

        $algorithm = (new AlgorithmLoader($algorithmData))->load();
        $version = (new VersionLoader($algorithmData, $algorithm))->load();

        foreach ($algorithmData['nodes'] as $nodeData) {
            self::checkHasProperties($nodeData, ['type', 'answers']);
            // TODO env variable?
            if ($nodeData['type'] == 'Question' || ($nodeData['type'] == 'QuestionsSequence' && $nodeData['category'] == 'predefined_syndrome')) {
                $answerType = (new AnswerTypeLoader($nodeData))->load();
                if (($nodeData['type'] == 'QuestionsSequence' && $nodeData['category'] == 'predefined_syndrome')) {
                    $node = (new QuestionSequenceLoader($nodeData, $algorithm, $answerType))->load();
                    foreach ($nodeData['answers'] as $answerData) {
                        $answer = (new AnswerQuestionSequenceLoader($answerData, $node))->load();
                    }
                } else {
                    $node = (new NodeLoader($nodeData, $algorithm, $answerType))->load();

                    foreach ($nodeData['answers'] as $answerData) {
                        $answer = (new AnswerLoader($answerData, $node))->load();
                    }
                }
            }
        }

        foreach ($algorithmData['final_diagnoses'] as $finalDiagnosisData) {
            self::checkHasProperties($finalDiagnosisData, ['type', 'drugs', 'managements']);

            $diagnosis = (new DiagnosisLoader($finalDiagnosisData, $version))->load();

            foreach ($finalDiagnosisData['drugs'] as $drugId => $drugRefData) {
                self::checkHasProperties($drugRefData, ['duration']);
                $drugData = $algorithmData['health_cares'][$drugId];
                $drug = (new DrugLoader($drugData, $diagnosis, $drugRefData['duration']))->load();

                foreach ($drugData['formulations'] as $formulationData) {
                    $formulation = (new FormulationLoader($formulationData, $drug))->load();
                }
            }

            foreach ($finalDiagnosisData['managements'] as $managementId => $managementRefData) {
                $managementData = $algorithmData['health_cares'][$managementId];
                $management = (new ManagementLoader($managementData, $diagnosis))->load();
            }
        }

        foreach ($algorithmData['health_cares'] as $drugData) {
            self::checkHasProperties($drugData, ['category']);
            if ($drugData['category'] == 'drug') {
                self::checkHasProperties($drugData, ['formulations']);
                $drug = (new DrugLoader($drugData))->load();

                foreach ($drugData['formulations'] as $formulationData) {
                    $formulation = (new FormulationLoader($formulationData, $drug))->load();
                }
            }
        }

        return $version;
    }

    /**
     * Save the patient and their config file from a medical case.
     *
     * @param object $caseData
     * @param PatientConfig $patientConfig
     * @return Patient
     */
    public function savePatient($caseData, $patientConfig, $version)
    {
        $patientData = $caseData['patient'];
        self::checkHasProperties($patientData, ['uid', 'consent_file']);

        // Consent file
        $consentFileName = null;
        if ($version->consent_management) {
            try {
                $consentPath = Config::get('medal.storage.consent_img_dir');
                $consentFileName = $patientData['uid'] . '_image.jpg';
                Storage::makeDirectory($consentPath);
                $consentImg = Image::make($patientData['consent_file']);
                $consentImg->save(Storage::disk('local')->path($consentPath . '/' . $consentFileName));
            } catch (Exception $ex) {
                $consentFileName = '';
            }
        }

        // Patient
        $patientLoader = new PatientLoader($patientData, $caseData['nodes'], $patientConfig, $consentFileName);
        $duplicateDataExists = Patient::where($patientLoader->getDuplicateConditions())->exists();

        $existingPatientIsTrue = false;
        if (strpos($version->study, 'Dynamic') !== false) {
            $existingPatientIsTrue = Answer::where($patientLoader->getExistingPatientAnswer())->first()->label == 'Yes';
        }

        $patientLoader->flagAsDuplicate($duplicateDataExists, $existingPatientIsTrue);

        return $patientLoader->load();
    }

    /**
     * Save the case.
     *
     * @param object $caseData
     * @param Version $version
     * @param Patient $patient
     * @return MedicalCase
     */
    public function saveCase($caseData, $version, $patient, $config)
    {
        if ($patient->merged) {
            $patient = Patient::whereMerged(false)->where('local_patient_id', $patient->local_patient_id)->first();
        }

        // Medical case
        /** @var MedicalCase $medicalCase */
        $medicalCase = (new MedicalCaseLoader($caseData, $patient, $version, $config))->load();
        // Case answers
        foreach ($caseData['nodes'] as $nodeData) {
            self::checkHasProperties($nodeData, ['id', 'answer']);
            $algoNode = Node::where('medal_c_id', $nodeData['id'])->first();

            if ($algoNode) { // Only responses to questions are stored (QS for example aren't)
                $algoNodeAnswer = Answer::where('medal_c_id', $nodeData['answer'])->first();
                $medicalCaseAnswer = (new MedicalCaseAnswerLoader($nodeData, $medicalCase, $algoNode, $algoNodeAnswer))->load();
            }
        }

        // Activities
        if ($caseData['activities']) {
            foreach ($caseData['activities'] as $activityData) {
                self::checkHasProperties($activityData, ['step', 'clinician']);
                (new ActivityLoader($activityData, $medicalCase))->load();
            }
        }

        // Diagnoses
        $diagnosesData = $caseData['diagnosis'];
        self::checkHasProperties($diagnosesData, ['agreed', 'additional', 'excluded', 'refused', 'custom']);
        $this->saveAgreedDiagnoses($diagnosesData['agreed'], $medicalCase);
        $this->saveAdditionalDiagnoses($diagnosesData['additional'], $medicalCase);
        $this->saveExcludedDiagnoses($diagnosesData['excluded'], $medicalCase);
        $this->saveRefusedDiagnoses($diagnosesData['refused'], $medicalCase);
        $this->saveCustomDiagnoses($diagnosesData['custom'], $medicalCase);

        return $medicalCase;
    }

    /**
     * Save diagnostics from a case.
     *
     * @param array $diagnosesData
     * @param MedicalCase $medicalCase
     * @return void
     */
    protected function saveAgreedDiagnoses($diagnosesData, $medicalCase)
    {
        foreach ($diagnosesData as $diagnosisRefData) {
            $diagnosisId = $diagnosisRefData['id'] ?? $diagnosisRefData;

            $diagnosis = Diagnosis::where('medal_c_id', $diagnosisId)->first();
            /** @var DiagnosisReference $diagnosisRef */
            $diagnosisRef = (new DiagnosisReferenceLoader($diagnosisRefData, $medicalCase, $diagnosis, ClinicianChoice::AGREED()))->load();

            $this->saveDrugs($diagnosisRefData, $diagnosisRef);
        }
    }

    /**
     * saveAdditionalDiagnoses.
     *
     * @param  array $diagnosesData
     * @param  MedicalCase $medicalCase
     * @return void
     */
    protected function saveAdditionalDiagnoses(array $diagnosesData, MedicalCase $medicalCase)
    {
        foreach ($diagnosesData as $diagnosisRefData) {
            $diagnosisId = $diagnosisRefData['id'] ?? $diagnosisRefData;

            $diagnosis = Diagnosis::where('medal_c_id', $diagnosisId)->first();

            /** @var DiagnosisReference $diagnosisRef */
            $diagnosisRef = (new DiagnosisReferenceLoader($diagnosisRefData, $medicalCase, $diagnosis, ClinicianChoice::ADDITIONAL()))->load();

            $this->saveDrugs($diagnosisRefData, $diagnosisRef);
        }
    }

    /**
     * saveCustomDiagnoses.
     *
     * @param  array $customDiagnosesData
     * @param  MedicalCase $medicalCase
     * @return void
     */
    protected function saveCustomDiagnoses(array $customDiagnosesData, MedicalCase $medicalCase)
    {
        foreach ($customDiagnosesData as $customDiagnosisRefData) {
            /** @var CustomDiagnosis $customDiagnosisRef */
            $customDiagnosisRef = (new CustomDiagnosisLoader($customDiagnosisRefData, $medicalCase))->load();

            $this->saveCustomDiagDrugs($customDiagnosisRefData, $customDiagnosisRef);
        }
    }

    /**
     * saveRefusedDiagnoses.
     *
     * @param  array $diagnosesData
     * @param  MedicalCase $medicalCase
     * @return void
     */
    protected function saveRefusedDiagnoses(array $diagnosesData, MedicalCase $medicalCase)
    {
        foreach ($diagnosesData as $diagnosisRefData) {
            $diagnosisId = $diagnosisRefData['id'] ?? $diagnosisRefData;

            $diagnosis = Diagnosis::where('medal_c_id', $diagnosisId)->first();
            $diagnosisRef = (new DiagnosisReferenceLoader($diagnosisRefData, $medicalCase, $diagnosis, ClinicianChoice::REFUSED()))->load();
        }
    }

    /**
     * Save excluded diagnostics from a case.
     *
     * @param array $diagnosesData
     * @param MedicalCase $medicalCase
     * @return void
     */
    protected function saveExcludedDiagnoses(array $diagnosesData, MedicalCase $medicalCase)
    {
        (new ExcludedDiagnosesListLoader($diagnosesData, $medicalCase))->load();
    }

    /**
     * saveDrugs.
     *
     * @param  array $diagnosisRefData
     * @param  DiagnosisReference $diagnosisRef
     * @return void
     */
    protected function saveDrugs(array $diagnosisRefData, DiagnosisReference $diagnosisRef)
    {
        self::checkHasProperties($diagnosisRefData, ['drugs']);

        $drugRefsData = $diagnosisRefData['drugs'];

        foreach ($drugRefsData['agreed'] as $drugId => $drugRefData) {
            $drug = Drug::where('medal_c_id', $drugId)->first();
            $formulation = array_key_exists('formulation_id', $drugRefData) ?
                Formulation::where('medal_c_id', $drugRefData['formulation_id'])->first()
                : null;

            $drugRef = (new DrugReferenceLoader($drugRefData, $diagnosisRef, $drug, $formulation, ClinicianChoice::AGREED(), false))->load();
        }

        foreach ($drugRefsData['additional'] as $drugId => $drugRefData) {
            $drug = Drug::where('medal_c_id', $drugId)->first();
            $formulation = array_key_exists('formulation_id', $drugRefData) ?
                Formulation::where('medal_c_id', $drugRefData['formulation_id'])->first()
                : null;

            $drugRef = (new DrugReferenceLoader($drugRefData, $diagnosisRef, $drug, $formulation, ClinicianChoice::ADDITIONAL(), false))->load();
        }

        foreach ($drugRefsData['refused'] as $drugId) {
            $drugRefData = null;
            $drug = Drug::where('medal_c_id', $drugId)->first();
            $drugRef = (new DrugReferenceLoader($drugRefData, $diagnosisRef, $drug, null, ClinicianChoice::REFUSED(), false))->load();
        }

        if (array_key_exists('custom', $drugRefsData)) {
            foreach ($drugRefsData['custom'] as $customDrugData) {
                $customDrug = (new CustomDrugLoader($customDrugData, $diagnosisRef, false))->load();
            }
        }

        foreach ($diagnosisRefData['managements'] as $managementId) {
            $management = Management::where('medal_c_id', $managementId)->first();
            $managementRef = (new ManagementReferenceLoader($managementId, $diagnosisRef, $management))->load();
        }
    }

    /**
     * Save additional and custom drugs for custom diagnosis.
     *
     * @param  array $diagnosisRefData
     * @param  CustomDiagnosis $diagnosisRef
     * @return void
     */
    protected function saveCustomDiagDrugs($diagnosisRefData, CustomDiagnosis $diagnosisRef)
    {
        $drugRefsData = $diagnosisRefData['drugs'];

        if (array_key_exists('additional', $drugRefsData)) {
            foreach ($drugRefsData['additional'] as $drugId => $drugRefData) {
                $drug = Drug::where('medal_c_id', $drugId)->first();
                $formulation = array_key_exists('formulation_id', $drugRefData) ?
                    Formulation::where('medal_c_id', $drugRefData['formulation_id'])->first()
                    : null;

                $drugRef = (new DrugReferenceLoader($drugRefData, $diagnosisRef, $drug, $formulation, ClinicianChoice::ADDITIONAL(), true))->load();
            }
        }

        if (array_key_exists('custom', $drugRefsData)) {
            foreach ($drugRefsData['custom'] as $customDrugData) {
                $customDrug = (new CustomDrugLoader($customDrugData, $diagnosisRef, true))->load();
            }
        }

        //Still save old json with only the drug inside the drugs array and a UUID as the key
        foreach ($drugRefsData as $drugUuid => $customDrugData) {
            if (is_string($drugUuid) && (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $drugUuid))) {
                $customDrug = (new CustomDrugLoader($customDrugData, $diagnosisRef, true))->load();
            }
        }
    }

    private static function checkHasProperties($data, $properties)
    {
        foreach ($properties as $property) {
            if (!array_key_exists($property, $data)) {
                throw new InvalidArgumentException("Missing property '$property'");
            }
        }
    }
}
