<?php

namespace App\Services;

use App\AutoDiagnostic;
use App\Device;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;

class AutoDiagnosticService
{
    protected $latest_diagnostic;

    protected $diag_file;

    /**
     * Gets Hubs pertinant information.
     *
     * @param  Collection $devices
     * @return Collection
     */
    public function getFormattedDevices(Collection $devices): Collection
    {
        $commits = [];
        $commit_file = config('medal.storage.json_diag_dir') . '/' . config('autoDiagnostic.commit_file');

        if (Storage::exists($commit_file)) {
            $str_diagnostic = Storage::get($commit_file);
            $json_diagnostic = json_decode($str_diagnostic, true);
            $commits = $json_diagnostic;
        }

        $devices->each(function (Device $device) use ($commits) {
            $device->is_uptodate = false;
            $device->offset = null;
            $diag = $device->auto_diagnostics->last();
            if (empty($diag) || Storage::missing($diag->path)) {
                return;
            }
            $diag_file = Storage::get($diag->path);
            $json_diagnostic = json_decode($diag_file, true);
            $current_hub_commit = $json_diagnostic['getGitInformation']['data']['commit_hash'];

            foreach ($commits as $commit) {
                $offset = array_search($current_hub_commit, array_keys($commit), true);
                if ($offset === 0) {
                    $device->is_uptodate = true;
                    $device->offset = $offset;

                    return;
                }
                if ($offset > 0) {
                    $device->offset = $offset;

                    return;
                }
                if ($offset === false) {
                    $device->offset = -1;
                }
            }
        });

        $devices_not_uptodate = $devices->reject(function (Device $device) {
            if ($device->is_uptodate) {
                return $device;
            }
        });

        return $devices_not_uptodate;
    }

    /**
     * generateAutoDiagnostic.
     *
     * @param  Device $device
     * @return array
     */
    public function generateAutoDiagnostic(Device $device): array
    {
        if (!$device->auto_diagnostics()->exists()) {
            return [];
        }

        $last_diag = $device->auto_diagnostics()->latest()->first();

        if (Storage::missing($last_diag->path)) {
            return [];
        }

        $this->latest_diagnostic = $last_diag;
        $this->diag_file = (string) Storage::get($last_diag->path);

        $json_diagnostic = json_decode($this->diag_file, true);
        $json_diagnostic['getGitInformation']['data']['hub_up_to_date'] = $last_diag->up_to_date;
        $json_diagnostic['getGitInformation']['data']['hub_commit_position'] = $last_diag->commit_pos;
        $json_diagnostic['getGitInformation']['data']['branch_current_commit'] = $last_diag->bitbucket_commit;
        $json_diagnostic['getGitInformation']['data']['branch_commit_time'] = $last_diag->bitbucket_commit_time;

        return [
            'sync_info' => $this->getSyncInformation($device),
            'recent_errors' => $json_diagnostic['getErrorInLog'],
            'git_info' => $json_diagnostic['getGitInformation'],
            'last_patient' => $json_diagnostic['getDateOfLastPatientCreated'],
        ];
    }

    /**
     * generateRecentErrors.
     *
     * @param  AutoDiagnostic  $device
     * @param  string $file
     * @return array
     */
    public function generateRecentErrors(AutoDiagnostic $auto_diag, string $file): array
    {
        $json_diagnostic = json_decode($file, true);
        $array['recent_errors'] = $json_diagnostic['getErrorInLog'];
        $array['recent_errors']['date_received'] = $auto_diag->created_at;
        $array['recent_errors']['path'] = basename($auto_diag->path);

        return $array;
    }

    /**
     * getSyncInformation.
     *
     * @param  Device $device
     * @return array
     */
    private function getSyncInformation(Device $device): array
    {
        $info_limit = 5;
        $sync_info = [];

        foreach ($device->auto_diagnostics()->latest()->get() as $key => $diagnostic) {
            if ($key > $info_limit - 1) {
                break;
            }
            $sync_info[$key] = $this->prepareInfo($diagnostic, 'getSyncInformation');
        }

        return $sync_info;
    }

    /**
     * prepareInfo.
     *
     * @param  AutoDiagnostic $diagnostic
     * @param  string $array_name
     * @return array
     */
    private function prepareInfo(AutoDiagnostic $diagnostic, $array_name): array
    {
        $no_error = false;
        $date_received = $diagnostic->created_at;
        $json_diagnostic = json_decode($this->diag_file, true);

        if (
            !$json_diagnostic['getErrorInLog']['status']
            && $json_diagnostic['getErrorInLog']['data']['errorMessage'] === "No error found in logs"
        ) {
            $no_error = true;
        }

        $json_diagnostic['getSyncInformation']['date_received'] = $date_received;
        $json_diagnostic['getSyncInformation']['path'] = basename($diagnostic->path);
        $json_diagnostic['getSyncInformation']['no_error'] = $no_error;

        return $json_diagnostic[$array_name];
    }
}
