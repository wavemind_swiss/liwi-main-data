<?php

namespace App\Services;

use App\Drug;
use Illuminate\Support\Facades\Config;

class DrugLoader extends ModelLoader
{
    protected $drugData;

    protected $diagnosis;

    protected $duration;

    /**
     * Constructor.
     *
     * @param object $drugData
     * @param Diagnosis $diagnosis
     * @param string $duration
     */
    public function __construct($drugData, $diagnosis = null, $duration = null)
    {
        parent::__construct($drugData);
        $this->drugData = $drugData;
        $this->diagnosis = $diagnosis;
        $this->duration = $duration;
    }

    protected function getKeys()
    {
        return array_merge(parent::getKeys(), [
            'diagnosis_id' => $this->diagnosis->id ?? null,
        ]);
    }

    protected function getValues()
    {
        if (is_array($this->duration)) {
            $duration = $this->duration[Config::get('medal.global.language')];
        } else {
            $duration = $this->duration;
        }

        if (is_array($this->drugData['label'])) {
            $label = $this->drugData['label'][config('medal.global.language')];
        } else {
            $label = $this->drugData['label'];
        }

        return array_merge(parent::getValues(), [
            'duration' => $duration,
            'label' => $label,
        ]);
    }

    protected function model()
    {
        return Drug::class;
    }

    protected function configName()
    {
        return 'drug';
    }

    /**
     * Create a Drug instance based on the data that was provided.
     *
     * @return Drug
     */
    public function load()
    {
        $record = $this->model()::updateOrCreate(
            $this->getKeys(),
            $this->getValues()
        );
        $record->save();

        return $record;
    }
}
