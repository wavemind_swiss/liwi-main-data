<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Formulation.
 *
 * @property int $id
 * @property string $medication_form
 * @property string $administration_route_name
 * @property float|null $liquid_concentration
 * @property float|null $dose_form
 * @property string|null $unique_dose
 * @property bool|null $by_age
 * @property float|null $minimal_dose_per_kg
 * @property float|null $maximal_dose_per_kg
 * @property float|null $maximal_dose
 * @property string|null $description
 * @property float|null $doses_per_day
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $drug_id
 * @property string|null $administration_route_category
 * @property int $medal_c_id
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation query()
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereAdministrationRouteCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereAdministrationRouteName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereByAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereDoseForm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereDosesPerDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereDrugId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereLiquidConcentration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereMaximalDose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereMaximalDosePerKg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereMedalCId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereMedicationForm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereMinimalDosePerKg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereUniqueDose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Formulation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Formulation extends Model
{
    protected $guarded = [];
}
