<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ConsultationConfig.
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ConsultationConfig newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConsultationConfig newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConsultationConfig query()
 * @mixin \Eloquent
 */
class ConsultationConfig extends Model
{
    protected $guarded = [];
}
