<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use OwenIt\Auditing\Contracts\Auditable;
use Schema;

/**
 * App\Patient.
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $birthdate
 * @property string|null $weight
 * @property string|null $gender
 * @property string|null $local_patient_id
 * @property int|null $group_id
 * @property string|null $consent
 * @property bool $redcap
 * @property bool $duplicate
 * @property string|null $other_uid
 * @property string|null $other_study_id
 * @property string|null $other_group_id
 * @property string|null $merged_with
 * @property bool $merged
 * @property bool $status
 * @property array|null $related_ids
 * @property string|null $middle_name
 * @property string|null $other_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read mixed $facility_name
 * @property-read \Illuminate\Database\Eloquent\Collection|MedicalCase[] $medical_cases
 * @property-read int|null $medical_cases_count
 * @method static \Illuminate\Database\Eloquent\Builder|Patient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Patient newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Patient notMerged()
 * @method static \Illuminate\Database\Eloquent\Builder|Patient query()
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereBirthdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereConsent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereDuplicate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereLocalPatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereMerged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereMergedWith($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereOtherGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereOtherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereOtherStudyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereOtherUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereRedcap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereRelatedIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Patient whereWeight($value)
 * @mixin \Eloquent
 */
class Patient extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public function isRedcapFlagged(): bool
    {
        // TODO : return value of redcap flag in the database
        return false;
    }

    protected $casts = [
        'related_ids' => 'array',
    ];

    protected $guarded = [];

    public function scopeNotMerged($query)
    {
        $query->where('merged', 0);
    }

    /**
     * Scope a query to only include non dropped mcs.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeNotDropped($query)
    {
        return $query->where('is_drop', false);
    }

    public function findByUids()
    {
        $nonMergedPatients = self::NotDropped()->NotMerged()->latest()->take(150)->get();

        $duplicateArray = [];
        $nonMergedPatients->each(function ($patient) use (&$duplicateArray) {
            $keyword = $patient->other_uid;
            if (!$keyword) {
                $keyword = 'nothingToSearch';
            }
            $patientDuplicate = self::where([
                ['other_uid', $patient->local_patient_id],
                ['merged', 0],
                ['is_drop', 0],
            ])
                ->orWhere([
                    ['merged', 0],
                    ['is_drop', 0],
                ])
                ->whereJsonContains('related_ids', [$keyword])
                ->get()->toArray();
            if ($patientDuplicate) {
                array_push($patientDuplicate, $patient->toArray());
                array_push($duplicateArray, $patientDuplicate);
            }
        });

        return $duplicateArray;
    }

    public function findByDuplicateKey($duplicateArray)
    {
        $markedPatients = self::where([
            ['duplicate', 1],
            ['merged', 0],
            ['is_drop', 0],
        ])->latest()->take(150)->get();
        foreach ($markedPatients as $patient) {
            $patientDuplicate = self::where([
                ['last_name', $patient->last_name],
                ['merged', 0],
                ['status', 0],
                ['is_drop', 0],
            ])
                ->where('first_name', $patient->first_name)
                ->where('birthdate', $patient->birthdate)
                ->get()->toArray();
            if (count($patientDuplicate) > 1) {
                $pairExist = false;
                collect($duplicateArray)->each(function ($duplicateGroup) use (&$patientDuplicate, &$pairExist) {
                    $existingIds = [];
                    $incomingIds = [];
                    collect($duplicateGroup)->each(function ($arrayPatient) use (&$existingIds) {
                        array_push($existingIds, $arrayPatient['id']);
                    });
                    collect($patientDuplicate)->each(function ($arrayPatient) use (&$incomingIds) {
                        array_push($incomingIds, $arrayPatient['id']);
                    });
                    sort($existingIds);
                    sort($incomingIds);
                    if ($existingIds === $incomingIds) {
                        $pairExist = true;
                    }
                });
                if (!$pairExist) {
                    array_push($duplicateArray, $patientDuplicate);
                }
            }
        }

        return $duplicateArray;
    }

    public function keepPairs($pair)
    {
        if (DuplicatePair::whereJsonContains('pairs', $pair)->doesntExist()) {
            DuplicatePair::create([
                'pairs' => $pair,
            ]);
        }
    }

    public function combinePairIds($first_patient_ids, $second_patient_ids)
    {
        $first_patient_ids = $first_patient_ids;
        if ($first_patient_ids == null) {
            $first_patient_ids = [];
        }
        $second_patient_ids = $second_patient_ids;
        if ($second_patient_ids == null) {
            $second_patient_ids = [];
        }
        $allrelatedIds = array_filter(
            array_merge(
                array_diff($first_patient_ids, $second_patient_ids),
                array_diff($second_patient_ids, $first_patient_ids)
            )
        );

        return $allrelatedIds;
    }

    public function addLocalPatientIds($first_patient_id, $second_patient_ids, $all_related_ids)
    {
        if (!in_array($first_patient_id, $all_related_ids)) {
            array_push($all_related_ids, $first_patient_id);
        }
        if (!in_array($second_patient_ids, $all_related_ids)) {
            array_push($all_related_ids, $second_patient_ids);
        }

        return $all_related_ids;
    }

    public function addConsentList($first_consent, $second_consent)
    {
        $consent_array = [];
        if ($first_consent) {
            array_push($consent_array, $first_consent);
        }
        if ($second_consent) {
            array_push($consent_array, $second_consent);
        }

        return serialize($consent_array);
    }

    public function patientData()
    {
        $filename = 'patients.csv';
        $patient_data = self::all();
        $table_columns = Schema::getColumnListing('patients');
        // for patient table
        $patient_data->each(function ($patient) {
            return $patient->related_ids = implode(',', $patient->related_ids);
        });
        $patient_data = Arr::prepend($patient_data->toArray(), $table_columns);
        // file creation
        $file = fopen($filename, 'w');
        foreach ($patient_data as $line) {
            fputcsv($file, $line);
        }
        fclose($file);

        return $filename;
    }

    /**
     * making a relationship to medicalCase.
     * @return Many medical cases
     */
    public function medical_cases()
    {
        return $this->hasMany(MedicalCase::class);
    }
}
