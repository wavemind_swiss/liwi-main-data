<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CustomDiagnosis.
 *
 * @property int $id
 * @property string|null $label
 * @property string|null $drugs
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $medical_case_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CustomDrug[] $custom_drugs
 * @property-read int|null $custom_drugs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DrugReference[] $drugs_references
 * @property-read int|null $drugs_references_count
 * @property-read \App\MedicalCase $medical_case
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDiagnosis newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDiagnosis newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDiagnosis query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDiagnosis whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDiagnosis whereDrugs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDiagnosis whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDiagnosis whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDiagnosis whereMedicalCaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDiagnosis whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CustomDiagnosis extends Model
{
    protected $guarded = [];

    public function drugs_references()
    {
        return $this->hasMany(DrugReference::class, 'custom_diagnosis_id', 'id');
    }

    public function custom_drugs()
    {
        return $this->hasMany(CustomDrug::class, 'custom_diagnosis_id', 'id');
    }

    public function medical_case()
    {
        return $this->belongsTo(MedicalCase::class, 'medical_case_id');
    }
}
