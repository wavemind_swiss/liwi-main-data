<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Algorithm.
 *
 * @property int $id
 * @property int $medal_c_id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|Algorithm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Algorithm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Algorithm query()
 * @method static \Illuminate\Database\Eloquent\Builder|Algorithm whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Algorithm whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Algorithm whereMedalCId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Algorithm whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Algorithm whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Algorithm extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guarded = [];
}
