<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Diagnostic.
 *
 * @property int $id
 * @property int $medal_c_id
 * @property string $reference
 * @property string $label
 * @property int $version_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnostic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnostic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnostic query()
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnostic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnostic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnostic whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnostic whereMedalCId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnostic whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnostic whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnostic whereVersionId($value)
 * @mixin \Eloquent
 */
class Diagnostic extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guarded = [];
}
