<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\MedicalStaffRole.
 *
 * @property int $id
 * @property string $type
 * @property string $label
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaffRole newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaffRole newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaffRole query()
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaffRole whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaffRole whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaffRole whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaffRole whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaffRole whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MedicalStaffRole extends Model
{
    protected $fillable = [
        'label',
        'type',
    ];

    protected $guarded = [];
}
