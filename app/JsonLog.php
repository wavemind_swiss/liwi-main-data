<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\JsonLog.
 *
 * @property int $id
 * @property string $case_json
 * @property string $json_id
 * @property int|null $group_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $zip_name
 * @method static \Illuminate\Database\Eloquent\Builder|JsonLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JsonLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JsonLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|JsonLog whereCaseJson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JsonLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JsonLog whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JsonLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JsonLog whereJsonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JsonLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JsonLog whereZipName($value)
 * @mixin \Eloquent
 */
class JsonLog extends Model
{
    protected $guarded = [];
}
