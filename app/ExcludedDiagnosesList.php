<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\ExcludedDiagnosesList.
 *
 * @property int $id
 * @property int $medical_case_id
 * @property MedicalCase $medical_case
 * @property array $excluded_diagnoses
 * @method static \Illuminate\Database\Eloquent\Builder|ExcludedDiagnosesList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExcludedDiagnosesList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ExcludedDiagnosesList query()
 * @method static \Illuminate\Database\Eloquent\Builder|ExcludedDiagnosesList whereExcludedDiagnoses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExcludedDiagnosesList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ExcludedDiagnosesList whereMedicalCaseId($value)
 * @mixin \Eloquent
 */
class ExcludedDiagnosesList extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'medical_case_id',
        'excluded_diagnoses',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'excluded_diagnoses' => 'array',
    ];

    public function medical_case(): BelongsTo
    {
        return $this->belongsTo(MedicalCase::class);
    }
}
