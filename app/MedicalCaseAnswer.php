<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\MedicalCaseAnswer.
 *
 * @property int $id
 * @property int $medical_case_id
 * @property int|null $answer_id
 * @property int $node_id
 * @property string|null $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Answer|null $answer
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \App\MedicalCase $medical_case
 * @property-read \App\Node $node
 * @method static Builder|MedicalCaseAnswer exportable()
 * @method static Builder|MedicalCaseAnswer newModelQuery()
 * @method static Builder|MedicalCaseAnswer newQuery()
 * @method static Builder|MedicalCaseAnswer query()
 * @method static Builder|MedicalCaseAnswer whereAnswerId($value)
 * @method static Builder|MedicalCaseAnswer whereCreatedAt($value)
 * @method static Builder|MedicalCaseAnswer whereId($value)
 * @method static Builder|MedicalCaseAnswer whereMedicalCaseId($value)
 * @method static Builder|MedicalCaseAnswer whereNodeId($value)
 * @method static Builder|MedicalCaseAnswer whereUpdatedAt($value)
 * @method static Builder|MedicalCaseAnswer whereValue($value)
 * @mixin \Eloquent
 */
class MedicalCaseAnswer extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guarded = [];

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeExportable(Builder $query): Builder
    {
        return $query->where(function (Builder $query) {
            // Has an answer.
            $query
                ->where('value', '<>', '')
                ->orWhereNotNull('answer_id');
        })->where(function (Builder $query) {
            // We want to export answers that compute WHO scores, and they have a 'Reference'.
            $query->whereHas('node', function (Builder $query) {
                $query
                    ->where('category', '<>', 'background_calculation')
                    ->orWhere('display_format', 'Reference');
            });
        });
    }

    /**
     * Get all audits of one medical case.
     * @params $id
     * @return $all
     */
    public static function getAudit($id)
    {
        $medicalCaseAnswer = self::find($id);

        return $medicalCaseAnswer->audits;
    }

    public function medical_case()
    {
        return $this->belongsTo('App\MedicalCase');
    }

    public function answer()
    {
        return $this->belongsTo('App\Answer');
    }

    public function node()
    {
        return $this->belongsTo('App\Node');
    }
}
