<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\MedicalStaff.
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $first_name
 * @property string $last_name
 * @property int $medical_staff_role_id
 * @property int|null $health_facility_id
 * @property-read \App\HealthFacility|null $healthFacility
 * @property-read \App\MedicalStaffRole $medicalStaffRole
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaff newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaff newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaff query()
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaff whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaff whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaff whereHealthFacilityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaff whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaff whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaff whereMedicalStaffRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalStaff whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MedicalStaff extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'medical_staff_role_id',
        'health_facility_id',
    ];

    protected $guarded = [];

    public function healthFacility()
    {
        return $this->belongsTo('App\HealthFacility');
    }

    public function lastActivity()
    {
        return $this->hasOne(Activity::class, 'first_name' . ' ' . 'last_name', 'clinician')->latest();
    }

    public function medicalStaffRole()
    {
        return $this->belongsTo('App\MedicalStaffRole');
    }
}
