<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;

class AutoDiagnosticsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'auth_id'=>['required'],
            'getErrorInLog.status'=>['required', 'bool'],
            'getErrorInLog.data'=>['required', 'array'],
            'getGitInformation.status'=>['required', 'bool'],
            'getGitInformation.data'=>['required', 'array'],
            'getSyncInformation.status'=>['required', 'bool'],
            'getSyncInformation.data'=>['required', 'array'],
            'getDateOfLastPatientCreated.status'=>['required', 'bool'],
            'getDateOfLastPatientCreated.data'=>['required', 'array'],

        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails()) {
            $this->auth_id ? Log::info('Attempt:Failed initial AutoDiagnostic Validation, Auth_id:' . $this->auth_id) :
            Log::info('Failed initial Validation,No Auth_Id Provided ');
        }
    }
}
