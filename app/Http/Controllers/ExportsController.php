<?php

namespace App\Http\Controllers;

use App\Exports\AdditionalDrugExport;
use App\Exports\AlgorithmExport;
use App\Exports\AnswerExport;
use App\Exports\AnswerTypeExport;
use App\Exports\CustomDiagnosisExport;
use App\Exports\DiagnosisExport;
use App\Exports\DiagnosisReferenceExport;
use App\Exports\DrugExport;
use App\Exports\DrugReferenceExport;
use App\Exports\FormulationExport;
use App\Exports\ManagementExport;
use App\Exports\ManagementReferenceExport;
use App\Exports\Medical_CaseExport;
use App\Exports\MedicalCaseAnswerExport;
use App\Exports\NodeExport;
use App\Exports\PatientExport;
use App\Exports\VersionExport;
use App\MedicalCaseAnswer;
use Auth;
use Carbon\Carbon;
use Excel;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Gate;

class ExportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private static function checkGateAllows()
    {
        if (!Gate::allows('Export', Auth::user())) {
            abort(403, 'You are not authorized to export data.');
        }
    }

    public function index()
    {
        self::checkGateAllows();
        $user = Auth::user();
        $export_path = storage_path('app/export/');
        $full_data_set = false;

        if (File::exists($export_path)) {
            $files = File::files($export_path);
        }

        if ($user->can('Access_Full_Data_Export')) {
            $full_data_set = File::exists($export_path . 'full_export_separated.zip');
        }

        $data = [
            'files' => $files ?? [],
            'full_data_set' => $full_data_set,
        ];

        return view('exports.index')->with($data);
    }

    public function DownloadExport($file)
    {
        self::checkGateAllows();
        $user = Auth::user();

        if (!$user->can('Download_Full_Data_Export') && $file === 'full_export_separated.zip') {
            return redirect()->back()->withErrors('You are not a allowed to download this file');
        }

        $file_path = storage_path('app/export/' . $file);
        if (!File::exists($file_path)) {
            return redirect()->back()->withErrors('File not found');
        }

        return response()->download($file_path, Carbon::today()->format('d-m-Y') . '-' . $file, ['Cache-Control' => 'no-cache, must-revalidate']);
    }

    public function Patients()
    {
        self::checkGateAllows();

        return Excel::download(new PatientExport, 'patients.csv');
    }

    public function cases()
    {
        self::checkGateAllows();

        return Excel::download(new Medical_CaseExport, 'medical_cases.csv');
    }

    public function casesAnswers()
    {
        self::checkGateAllows();

        ini_set('memory_limit', '4096M');
        ini_set('max_execution_time', '300');

        return Excel::download(new MedicalCaseAnswerExport, 'medical_case_answers.csv');
    }

    public function casesAnswers2()
    {
        self::checkGateAllows();

        ini_set('memory_limit', '4096M');
        $callback = function () {
            // Open output stream
            $handle = fopen('php://output', 'w');
            // Add CSV headers
            fputcsv($handle, ['id', 'medical_case_id', 'answer_id', 'node_id', 'value', 'created_at', 'updated_at']);
            $case_answers = MedicalCaseAnswer::all();
            $case_answers->each(function ($case_answer) use (&$handle) {
                $c_answer = [
                    $case_answer->id,
                    $case_answer->medical_case_id,
                    $case_answer->answer_id,
                    $case_answer->node_id,
                    $case_answer->value,
                    $case_answer->created_at,
                    $case_answer->updated_at,
                ];
                fputcsv($handle, $c_answer);
            });
            // Close the output stream
            fclose($handle);
        };
        // build response headers so file downloads.
        $headers = ['Content-Type' => 'text/csv'];
        // return the response as a streamed response.
        for ($i = 0; $i < 7; $i++) {
            return response()->streamDownload($callback, 'medical_case_answers.csv', $headers);
        }
        // return response()->streamDownload($callback, 'medical_case_answers.csv', $headers);
    }

    public function answers()
    {
        self::checkGateAllows();

        return Excel::download(new AnswerExport, 'answers.csv');
    }

    public function diagnosisReferences()
    {
        self::checkGateAllows();

        return Excel::download(new DiagnosisReferenceExport, 'diagnosis_references.csv');
    }

    public function customDiagnoses()
    {
        self::checkGateAllows();

        return Excel::download(new CustomDiagnosisExport, 'custom_diagnoses.csv');
    }

    public function drugReferences()
    {
        self::checkGateAllows();

        return Excel::download(new DrugReferenceExport, 'drug references.csv');
    }

    public function additionalDrugs()
    {
        self::checkGateAllows();

        return Excel::download(new AdditionalDrugExport, 'additional_drugs.csv');
    }

    public function managementReferences()
    {
        self::checkGateAllows();

        return Excel::download(new ManagementReferenceExport, 'management_references.csv');
    }

    public function diagnoses()
    {
        self::checkGateAllows();

        return Excel::download(new DiagnosisExport, 'diagnoses.csv');
    }

    public function drugs()
    {
        self::checkGateAllows();

        return Excel::download(new DrugExport, 'drugs.csv');
    }

    public function formulations()
    {
        self::checkGateAllows();

        return Excel::download(new FormulationExport, 'formulations.csv');
    }

    public function managements()
    {
        self::checkGateAllows();

        return Excel::download(new ManagementExport, 'managements.csv');
    }

    public function nodes()
    {
        self::checkGateAllows();

        return Excel::download(new NodeExport, 'nodes.csv');
    }

    public function answer_types()
    {
        self::checkGateAllows();

        return Excel::download(new AnswerTypeExport, 'answer_types.csv');
    }

    public function algorithms()
    {
        self::checkGateAllows();

        return Excel::download(new AlgorithmExport, 'algorithms.csv');
    }

    public function algorithmVersions()
    {
        self::checkGateAllows();

        return Excel::download(new VersionExport, 'algorithm_versions.csv');
    }
}
