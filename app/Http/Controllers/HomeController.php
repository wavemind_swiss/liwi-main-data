<?php

namespace App\Http\Controllers;

use App\MedicalCase;
use App\Patient;
use App\User;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['forgotPassword']]);
        $this->middleware('2fa');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $metadata = json_decode(file_get_contents(base_path('package.json')));

        $data = [
            'currentUser' => Auth::user(),
            'userCount' => User::count(),
            'mdCases' => MedicalCase::count(),
            'patientCount' => Patient::count(),
            'version' => $metadata->version,
        ];

        return view('home')->with($data);
    }

    public function reauthenticate(Request $request)
    {
        $user = Auth::user();

        return view('google2fa.confirm', [
            'user_id' => $user->id,
        ]);
    }

    public function reauthenticateConfirmed()
    {
        $user = Auth::user();

        $google2fa = app('pragmarx.google2fa');
        $user->google2fa_secret = $google2fa->generateSecretKey();
        $user->save();

        // generate the QR image
        $QR_Image = $google2fa->getQRCodeInline(
            config('app.name'),
            $user->email,
            $user->google2fa_secret
        );

        // Pass the QR barcode image to our view
        return view('google2fa.register', [
            'QR_Image' => $QR_Image,
            'secret' => $user->google2fa_secret,
            'reauthenticating' => true,
        ]);
    }
}
