<?php

namespace App\Http\Controllers\Api;

use App\Device;
use App\HealthFacility;
use App\Http\Controllers\Controller;
use App\Http\Requests\DeviceInfoRequest;
use App\Http\Resources\Device as DeviceResource;
use App\Jobs\ProcessUploadZip;
use App\Services\AlgorithmService;
use App\Services\DeviceService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use ZipArchive;

class AuthDeviceController extends Controller
{
    protected $deviceService;

    protected $algorithmService;

    public function __construct(DeviceService $deviceService, AlgorithmService $algorithmService)
    {
        $this->deviceService = $deviceService;
        $this->algorithmService = $algorithmService;
    }

    public function healthFacilityInfo(Request $request, Device $device)
    {
        $healthFacility = HealthFacility::where('id', $device->health_facility_id)->first();
        $device_id = $device->oauth_client_id ?? 'unknown';
        if ($healthFacility == null) {
            $info = [
                'en' => "Error. Your device has been unassigned from the health facility in medAL-data. Please inform your IT support. (Device ID: $device_id)",
                'fr' => "Erreur. Votre appareil a été désassocié du centre de santé dans medAL-data. Veuillez en informer votre répondant informatique. (Device ID : $device_id)",
            ];
            Log::error($info['en']);

            return response()->json($info, 422);
        }

        $info = $this->deviceService->getHealthFacilityInfo($device, $healthFacility);

        return response()->json($info);
    }

    public function storeDeviceInfo(DeviceInfoRequest $request, Device $device)
    {
        $validated = $request->validated();
        $this->deviceService->storeDeviceInfo($device, $validated);

        return response()->json(new DeviceResource($device));
    }

    public function tokeninfo()
    {
        return response()->noContent();
    }

    public function algorithm(Request $request, Device $device)
    {
        $json_version = (int) $request->get('json_version');

        $healthFacility = HealthFacility::where('id', $device->health_facility_id)->first();
        $device_id = $device->oauth_client_id ?? 'unknown';
        $hf_group_id = $healthFacility->group_id ?? 'unknown';

        if (!$healthFacility) {
            $info = [
                'en' => "Error. Your device has been unassigned from the health facility in medAL-data. Please inform your IT support. (Device ID: $device_id)",
                'fr' => "Erreur. Votre appareil a été désassocié du centre de santé dans medAL-data. Veuillez en informer votre répondant informatique. (Device ID : $device_id)",
            ];
            Log::error($info['en']);

            return response()->json($info, 422);
        }

        if (!$healthFacility->versionJson) {
            $info = [
                'en' => "Error. There is no algorithm associated with your health facility in medAL-data. Please inform your IT support. (Device ID: $device_id). Health facility ID: $hf_group_id)",
                'fr' => "Erreur. Aucun algorithme associé à votre centre de santé dans medAL-data. Veuillez en informer votre répondant informatique. (Device ID: $device_id). Centre de santé n° $hf_group_id)",
            ];
            Log::error($info['en']);

            return response()->json($info, 422);
        }

        $alg = $this->algorithmService->getAlgorithmJsonForDevice($healthFacility);

        if ($json_version < $alg['json_version'] || $json_version == null) {
            $zip = new ZipArchive();
            $res = $zip->open('algo.zip', ZipArchive::CREATE);
            if ($res === true) {
                $zip->addFromString('content.json', json_encode($alg['algo']->medal_r_json));
            }
            $zip->close();

            return response()->download('algo.zip')->deleteFileAfterSend();
        } else {
            return response()->noContent();
        }
    }

    public function emergencyContent(Request $request, Device $device)
    {
        $json_version = (int) $request->get('json_version');
        $healthFacility = HealthFacility::where('id', $device->health_facility_id)->first();
        $device_id = $device->oauth_client_id ?? 'unknown';
        $hf_group_id = $healthFacility->group_id ?? 'unknown';

        if (!$healthFacility) {
            $info = [
                'en' => "Error. Your device has been unassigned from the health facility in medAL-data. Please inform your IT support. (Device ID: $device_id)",
                'fr' => "Erreur. Votre appareil a été désassocié du centre de santé dans medAL-data. Veuillez en informer votre répondant informatique. (Device ID : $device_id)",
            ];
            Log::error($info['en']);

            return response()->json($info, 422);
        }

        if (!$healthFacility->versionJson) {
            $info = [
                'en' => "Error. There is no algorithm associated with your health facility in medAL-data. Please inform your IT support. (Device ID: $device_id). Health facility ID: $hf_group_id)",
                'fr' => "Erreur. Aucun algorithme associé à votre centre de santé dans medAL-data. Veuillez en informer votre répondant informatique. (Device ID: $device_id). Centre de santé n° $hf_group_id)",
            ];
            Log::error($info['en']);

            return response()->json($info, 422);
        }

        $emergencyContent = $this->algorithmService->getAlgorithmEmergencyContentJsonForDevice($healthFacility);

        if ($json_version < $emergencyContent['json_version'] || $json_version == null) {
            $zip = new ZipArchive();
            $res = $zip->open('emergency.zip', ZipArchive::CREATE);
            if ($res === true) {
                $zip->addFromString('content.json', json_encode($emergencyContent['emergency_content']));
            }
            $zip->close();

            return response()->download('emergency.zip')->deleteFileAfterSend();
        } else {
            return response()->noContent();
        }
    }

    public function syncMedicalCases(Request $request, Device $device)
    {
        if (!$request->hasFile('file')) {
            return response('Missing attached file', 400);
        }

        /** @var \Illuminate\Http\UploadedFile $file */
        $file = $request->file('file');
        if ($file->getSize() === 0) {
            Log::error('Recieved empty zip from ' . $device->oauth_client_id ?? 'unknown');

            return response('Empty file', 400);
        }

        if ($file->getMimeType() !== 'application/zip') {
            Log::error('Recieved ' . $file->getMimeType() . ' file from' . $device->oauth_client_id ?? 'unknown');

            return response('Wrong format', 400);
        }

        $path = $file->store(Config::get('medal.storage.cases_zip_dir'));
        if ($path === false) {
            return response('Unable to save file', 500);
        }

        try {
            Log::info('Recieved ' . $file->getClientOriginalExtension() . ' file, size ' . $file->getSize() . '. From ' . optional($device->health_facility)->name);
        } catch (\Throwable $th) {
        }

        if (!DB::table('jobs')->where('payload', 'LIKE', "%$path%")->exists()) {
            ProcessUploadZip::dispatch($path);
        }

        return response('Zip file received', 200);
    }
}
