<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\RegisterRequest;
use App\Jobs\ResetAccountPasswordJob;
use App\PasswordReset;
use App\User;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    protected function redirectTo()
    {
        Log::info('User registered.', ['user_name' => Auth::user()->name, 'user_id' => Auth::user()->id, 'user_email' => Auth::user()->email]);

        return '/home';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    /**
     * Register method to use 2FA.
     */
    public function register()
    {
        $user = session('user');
        if (auth()->user()) {
            $user = auth()->user();
        }

        if (!$user) {
            return redirect()->back();
        }

        // initialise the 2FA class
        $google2fa = app('pragmarx.google2fa');
        $user->google2fa_secret = $google2fa->generateSecretKey();
        session(['google2fa_secret' => $user->google2fa_secret]);
        $registration_data = $user;
        // generate the QR image.
        $QR_Image = $google2fa->getQRCodeInline(
            config('app.name'),
            $user->email,
            $user->google2fa_secret
        );
        // pass the QR barcode image to our view
        return view('google2fa.register', [
            'QR_Image' => $QR_Image,
            'secret' => $registration_data['google2fa_secret'],
            'reauthenticating' => false,
        ]);
    }

    public function completeRegistration(Request $request)
    {
        if (auth()->user()) {
            $user = auth()->user();
        } else {
            $token = PasswordReset::where('token', session('reset_token'))->first();
            $user = optional($token)->user;
        }

        if (!$user) {
            return view('errors.404');
        }
        $user->update(['google2fa_secret' => session('google2fa_secret')]);

        // foget all the sessions
        session()->forget('google2fa_secret');
        session()->forget('reset_token');
        if ($user->passwordReset) {
            $user->passwordReset->delete();
        }

        // redirect back to login
        return redirect(route('login'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Adds roles to specific accounts when creating a new user.
     */
    protected function addRoleToSpecificAccounts(&$user)
    {
        if ($user === null) {
            throw new \InvalidArgumentException('User should not be null.');
        }

        switch ($user->email) {
            case 'admin@dynamic.com':
                $role = 'Administrator';
                break;
            case 'datamanager@dynamic.com':
                $role = 'Data Manager';
                break;
            case 'projectviewer@dynamic.com':
                $role = 'Project Viewer';
                break;
            case 'statistician@dynamic.com':
                $role = 'Statistician';
                break;
            case 'logistician@dynamic.com':
                $role = 'Logistician';
                break;
            default:
                $role = null;
        }

        // no role to add
        if ($role === null) {
            return;
        }

        $role_entry = DB::table('roles')->where('name', $role)->first();
        if ($role_entry !== null) {
            $user->roles()->sync([$role_entry->id]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $name = $data['name'];
        $email = $data['email'];
        $password = Hash::make($data['password']);
        $google2fa_secret = $data['google2fa_secret'];
        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'google2fa_secret' => $google2fa_secret,
        ]);

        $this->addRoleToSpecificAccounts($user);

        return $user;
    }

    public function checkToken($id, Request $request)
    {
        $code_exist = PasswordReset::where('token', $id)->exists();
        $request->session()->put('reset_token', $id);
        if ($code_exist) {
            return view('emails.reset')->with(['token' => $id]);
        }

        return view('errors.404');
    }

    public function makePassword(RegisterRequest $request)
    {
        try {
            $reset_token = $request->session()->get('reset_token');
            $pasword_reset = PasswordReset::where('token', $reset_token)->first();
            $validated = $request->validated();
            $user = $pasword_reset->user;
            $user->update(['password' => Hash::make($validated['password'])]);
            if (is_null($user->getAttributes()['google2fa_secret'])) {
                Log::info('The password of user ' . $user->email . ' has been changed.Proceeding to 2fa');

                return redirect()->route('register.register_2fa')
                    ->with('user', $pasword_reset->user);
            } else {
                Log::info('The password of user ' . $user->email . ' has been changed.');

                return redirect(route('login'));
            }
        } catch (Exception $e) {
            $message = 'Something went wrong, please retry or contact the administrator for help.';
            Log::error('Tried to change the password of user ' . optional($user)->email . ', but something went wrong.' . $e);

            return Redirect::back()->with(['error' => $message]);
        }
    }

    /**
     * forgotPassword.
     *
     * @param  Request $request
     * @return RedirectResponse
     */
    public function forgotPassword(ForgotPasswordRequest $request)
    {
        $email = Str::lower($request->email);
        $userNotIn = User::where('email', $email)->doesntExist();
        if ($userNotIn) {
            $message = 'Email has been sent to you for password reset.';
            Log::error('Tried to send a request to change the password of user ' . $request->email . ', but the given email does not exist.');

            return Redirect::back()->with(['success' => $message]);
        }

        $random_password = Str::random(30);
        while (PasswordReset::where('token', $random_password)->exists()) {
            $random_password = Str::random(30);
        }
        $user = User::where('email', $email)->first();
        $saveCode = PasswordReset::saveReset($user, $random_password);

        if ($saveCode) {
            $body = 'Click this link to reset your password';
            dispatch((new ResetAccountPasswordJob($body, $email, $user->name, $random_password))->onQueue('high'));
            $message = 'Email has been sent to you for password reset.';
            Log::info('A request to change the password of user ' . $request->email . ' has been sent by email.');

            return Redirect::back()->with(['success' => $message]);
        }

        $message = 'Something went wrong.';
        Log::error('Tried to send a request to change the password of user ' . $request->email . ', but something went wrong.');

        return Redirect::back()->with(['error' => $message]);
    }
}
