<?php

namespace App\Http\Controllers;

use App\DuplicatePair;
use App\MedicalCase;
use App\MedicalCaseAnswer;
use App\Node;
use App\Patient;
use App\PatientConfig;
use Auth;
use DataTables;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class PatientsController extends Controller
{
    /**
     * To block any non-authorized user.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->authorizeResource(Patient::class, 'patient');
    }

    /**
     * View all patients.
     * @param Request $request
     * @return $patients
     */
    public function index(Request $request)
    {
        $ids = $request->get('ids') ?? [];
        $chunked_ids = array_chunk($ids, 200);

        $patients = Patient::when($chunked_ids, function ($query, $chunked_ids) {
            foreach ($chunked_ids as $ids) {
                $query = $query->whereIn('id', $ids);
            }
            return $query;
        })->orderBy('created_at');

        return view('patients.index')->with([
            'patients' => $patients,
            'ids' => $ids,
        ]);
    }

    /**
     * Show individual Patient.
     * @param $patient
     * @return $patient
     */
    public function show(Patient $patient)
    {
        $patient->related_ids = implode(', ', $patient->related_ids);
        Log::info('User with id ' . Auth::user()->id . ' checked out patient ' . $patient->id . '.');

        return view('patients.show', [
            'patient' => $patient,
        ]);
    }

    /**
     * Drop for analysis individual Patient.
     * @param int $patient_id
     * @return JsonResponse
     */
    public function dropForAnalysis(int $patient_id): JsonResponse
    {
        $patient = Patient::find($patient_id);

        if (empty($patient)) {
            $data = ['message' => 'Patient not found !'];

            return response()->json($data, 403);
        }

        if ($patient->is_drop) {
            $data = ['message' => 'This patient is already dropped for analysis!'];

            return response()->json($data, 403);
        }

        try {
            DB::beginTransaction();
            $patient->update(['is_drop' => true]);
            $patient->medical_cases()->update(['is_drop' => true]);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $data = ['message' => 'Error while updating the patient!'];
            Log::error("Error while updating the patient {$patient->id}");

            return response()->json($data, 520);
        }

        Log::info('User ' . Auth::user()->id . ' dropped for analysis patient ' . $patient->id);
        $data = ['message' => 'Patient successfully dropped for analysis'];

        return response()->json($data);
    }

    /**
     * Drop for analysis individual Patient.
     * @return JsonResponse
     */
    public function bulkDropForAnalysis(Request $request): JsonResponse
    {
        $patients = Patient::whereIn('local_patient_id', $request->patients);

        if ($patients->get()->isEmpty()) {
            $data = ['message' => 'No patients found!'];
            return response()->json($data, 403);
        }

        try {
            DB::beginTransaction();
            $patients->update(['is_drop' => true]);
            $patients->each(function ($patient) {
                $patient->medical_cases()->update(['is_drop' => true]);
            });
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $data = ['message' => 'Error while updating patients!'];
            Log::error("Error while updating patients");
            report($e);

            return response()->json($data, 520);
        }

        Log::info('User ' . Auth::user()->id . ' dropped for analysis multiple patients');
        $data = ['message' => 'Patients successfully dropped for analysis'];

        return response()->json($data);
    }

    /**
     * Process ajax request.
     * @param $request
     */
    public function getPatients(Request $request)
    {
        $searchValue = $request->get('search')['value'] ?? null;
        $ids = $request->get('ids') ?? [];

        $patients = Patient::with(['medical_cases'])
            ->when($searchValue, function ($query, $searchValue) {
                return $query->where(function (Builder $query) use ($searchValue) {
                    return $query
                        ->where('patients.id', 'LIKE', "%$searchValue%")
                        ->orWhere('local_patient_id', 'LIKE', "%$searchValue%")
                        ->orWhere('first_name', 'ILIKE', "%$searchValue%")
                        ->orWhere('middle_name', 'ILIKE', "%$searchValue%")
                        ->orWhere('last_name', 'ILIKE', "%$searchValue%")
                        ->orWhere('birthdate', 'LIKE', "%$searchValue%");
                });
            })->when($ids, function ($query, $ids) {
                return $query->whereIn('id', $ids);
            })->select('patients.*');

        return DataTables::eloquent($patients)
            ->addColumn('show', function (Patient $patient) {
                if ($patient->is_drop) {
                    return '<a href=' . route('patients.show', [$patient->id]) . " class='btn btn-outline-dark'>Show</a>";
                }
                $mc_ids = $patient->medical_cases->pluck('id')->toArray();

                return '<a href=' . route('patients.show', [$patient->id]) . " class='btn btn-outline-dark'>Show</a>"
                    . '<button data-ids="' . json_encode(array_values($mc_ids)) . '" onClick=\'prepareFordrop(this, ' . $patient->toJson() . ')\' id="drop_button" class="ml-2 btn btn-outline-danger">Drop</button>';
            })
            ->editColumn('status', function (Patient $patient) {
                return $patient->merged ? 'Merged' : 'Active';
            })
            ->rawColumns(['show'])
            ->toJson();
    }

    /**
     * Shows comparison between patients.
     * @param $firstId
     * @param $secondId
     * @return $patients
     */
    public function compare($firstId, $secondId)
    {
        $this->authorize('duplicate', Patient::class);
        $first_patient = Patient::find($firstId);
        //$first_patient = $this->FV($first_patient);
        $second_patient = Patient::find($secondId);
        $data = [
            'first_patient' => $first_patient,
            'second_patient' => $second_patient,
        ];

        return view('patients.compare')->with($data);
    }

    /**
     * Find duplicates by a certain value.
     * @return View
     * @return $catchEachDuplicate
     */
    public function findDuplicates()
    {
        $this->authorize('duplicate', Patient::class);

        $criteria = session()->get('searchCriteria');

        if ($criteria && count(array_filter($criteria))) {
            $duplicates = Patient::select($criteria)
                ->where('merged', 0)
                ->where('is_drop', 0)
                ->groupBy($criteria)
                ->havingRaw('COUNT(*) > 1')
                ->get()
                ->toArray();
        } else {
            $duplicates = Patient::select('first_name', 'last_name', 'birthdate')
                ->where('merged', 0)
                ->where('is_drop', 0)
                ->groupBy('first_name', 'last_name', 'birthdate')
                ->havingRaw('COUNT(*) > 1')
                ->get()
                ->toArray();
        }

        $catchEachDuplicate = [];
        foreach ($duplicates as $duplicate) {
            $patients = Patient::where('merged', 0)
                ->where('is_drop', 0)
                ->when(array_key_exists('first_name', $duplicate), function ($query) use ($duplicate) {
                    return $query->where('first_name', $duplicate['first_name']);
                })->when(array_key_exists('last_name', $duplicate), function ($query) use ($duplicate) {
                    return $query->where('last_name', $duplicate['last_name']);
                })->when(array_key_exists('birthdate', $duplicate), function ($query) use ($duplicate) {
                    return $query->where('birthdate', $duplicate['birthdate']);
                })->get();
            array_push($catchEachDuplicate, $patients);
        }
        $catchEachDuplicate = $this->checkForPairs($catchEachDuplicate);

        return view('patients.showDuplicates')->with([
            'catchEachDuplicate' => $catchEachDuplicate,
            'criteria' => $criteria
        ]);
    }

    /**
     * Shows merge between patients.
     * @param $firstId
     * @param $secondId
     * @return $patients
     */
    public function mergeShow($firstId, $secondId)
    {
        $this->authorize('merge', Patient::class);

        $first_patient = Patient::with('medical_cases')->find($firstId);
        $second_patient = Patient::with('medical_cases')->find($secondId);
        $mc_first_patient = $first_patient->medical_cases()->latest('consultation_date')->first();
        $mc_second_patient = $second_patient->medical_cases()->latest('consultation_date')->first();
        $config_first_patient = PatientConfig::where('version_id', $mc_first_patient->version_id)->first();
        $config_second_patient = PatientConfig::where('version_id', $mc_second_patient->version_id)->first();

        $fp_village_node_id = $config_first_patient->config['village_question_id'] ?? null;
        if ($fp_village_node_id) {
            $fp_node_village = Node::whereMedalCId($fp_village_node_id)->first();
            if ($fp_node_village) {
                $fp_village = MedicalCaseAnswer::whereNodeId($fp_node_village->id)->whereMedicalCaseId($mc_first_patient->id)->first();
            }
        }
        $fp_last_name_caregiver_node_id = $config_first_patient->config['last_name_caregiver_id'] ?? null;
        if ($fp_last_name_caregiver_node_id) {
            $fp_last_name_node = Node::whereMedalCId($fp_last_name_caregiver_node_id)->first();
            if ($fp_last_name_node) {
                $fp_last_name_caregiver = MedicalCaseAnswer::whereNodeId($fp_last_name_node->id)->whereMedicalCaseId($mc_first_patient->id)->first();
            }
        }
        $fp_first_name_caregiver_node_id = $config_first_patient->config['first_name_caregiver_id'] ?? null;
        if ($fp_first_name_caregiver_node_id) {
            $fp_first_name_node = Node::whereMedalCId($fp_first_name_caregiver_node_id)->first();
            if ($fp_first_name_node) {
                $fp_first_name_caregiver = MedicalCaseAnswer::whereNodeId($fp_first_name_node->id)->whereMedicalCaseId($mc_first_patient->id)->first();
            }
        }
        $sp_village_node_id = $config_second_patient->config['village_question_id'] ?? null;
        if ($sp_village_node_id) {
            $sp_node_village = Node::whereMedalCId($sp_village_node_id)->first();
            if ($sp_node_village) {
                $sp_village = MedicalCaseAnswer::whereNodeId($sp_node_village->id)->whereMedicalCaseId($mc_second_patient->id)->first();
            }
        }
        $sp_last_name_caregiver_node_id = $config_second_patient->config['last_name_caregiver_id'] ?? null;
        if ($sp_last_name_caregiver_node_id) {
            $sp_last_name_node = Node::whereMedalCId($sp_last_name_caregiver_node_id)->first();
            if ($sp_last_name_node) {
                $sp_last_name_caregiver = MedicalCaseAnswer::whereNodeId($sp_last_name_node->id)->whereMedicalCaseId($mc_second_patient->id)->first();
            }
        }
        $sp_first_name_caregiver_node_id = $config_second_patient->config['first_name_caregiver_id'] ?? null;
        if ($sp_first_name_caregiver_node_id) {
            $sp_first_name_node = Node::whereMedalCId($sp_first_name_caregiver_node_id)->first();
            if ($sp_first_name_node) {
                $sp_first_name_caregiver = MedicalCaseAnswer::whereNodeId($sp_first_name_node->id)->whereMedicalCaseId($mc_second_patient->id)->first();
            }
        }

        $data = [
            'first_patient' => $first_patient,
            'second_patient' => $second_patient,
            'fp_village' => $fp_village->value ?? 'Unknow',
            'fp_last_name_caregiver' => $fp_last_name_caregiver->value ?? 'Unknow',
            'fp_first_name_caregiver' => $fp_first_name_caregiver->value ?? 'Unknow',
            'sp_village' => $sp_village->value ?? 'Unknow',
            'sp_last_name_caregiver' => $sp_last_name_caregiver->value ?? 'Unknow',
            'sp_first_name_caregiver' => $sp_first_name_caregiver->value ?? 'Unknow',
        ];

        return view('patients.merge')->with($data);
    }

    /**
     * Search duplicates.
     * @param $request
     * @return view
     */
    public function searchDuplicates(Request $request)
    {
        $this->authorize('duplicate', Patient::class);

        $criteria = $request->input('searchCriteria');
        if (!$criteria) {
            return redirect()->action('PatientsController@findDuplicates');
        }

        $tablecolumns = Schema::getColumnListing('patients');
        if (count(array_diff($criteria, $tablecolumns)) == 0) {
            if (count($criteria) == 1) {
                $scriteria = strval($criteria[0]);
                $duplicates = Patient::select($scriteria)
                    ->where([
                        ['merged', 0], ['is_drop', 0], ['status', 0]
                    ])
                    ->groupBy($criteria[0])
                    ->havingRaw('COUNT(*) > 1')
                    ->get()
                    ->toArray();
                $catchEachDuplicate = [];
                foreach ($duplicates as $duplicate) {
                    $patients = Patient::where(
                        [
                            [$scriteria, $duplicate[$scriteria]],
                            ['merged', 0],
                            ['status', 0],
                            ['is_drop', 0],
                        ]
                    )->get();
                    array_push($catchEachDuplicate, $patients);
                }
                $catchEachDuplicate = $this->checkForPairs($catchEachDuplicate);
                return view('patients.showDuplicates')->with(['catchEachDuplicate' => $catchEachDuplicate, 'criteria' => $criteria]);
            } elseif (count($criteria) == 2) {
                $duplicates = Patient::select($criteria[0], $criteria[1])
                    ->where('merged', 0)
                    ->where('is_drop', 0)
                    ->groupBy($criteria[0], $criteria[1])
                    ->havingRaw('COUNT(*) > 1')
                    ->get()
                    ->toArray();
                $catchEachDuplicate = [];
                foreach ($duplicates as $duplicate) {
                    $patients = Patient::where(
                        [
                            [$criteria[0], $duplicate[$criteria[0]]],
                            [$criteria[1], $duplicate[$criteria[1]]],
                            ['merged', 0],
                            ['is_drop', 0],
                        ]
                    )->get();
                    array_push($catchEachDuplicate, $patients);
                }
                $catchEachDuplicate = $this->checkForPairs($catchEachDuplicate);
                return view('patients.showDuplicates')->with(['catchEachDuplicate' => $catchEachDuplicate, 'criteria' => $criteria]);
            } elseif (count($criteria) == 3) {
                $duplicates = Patient::select($criteria[0], $criteria[1], $criteria[2])
                    ->where('merged', 0)
                    ->where('is_drop', 0)
                    ->groupBy($criteria[0], $criteria[1], $criteria[2])
                    ->havingRaw('COUNT(*) > 1')
                    ->get()
                    ->toArray();
                $catchEachDuplicate = [];
                foreach ($duplicates as $duplicate) {
                    $patients = Patient::where(
                        [
                            [$criteria[0], $duplicate[$criteria[0]]],
                            [$criteria[1], $duplicate[$criteria[1]]],
                            [$criteria[2], $duplicate[$criteria[2]]],
                            ['merged', 0],
                            ['is_drop', 0],
                        ]
                    )->get();
                    array_push($catchEachDuplicate, $patients);
                }
                $catchEachDuplicate = $this->checkForPairs($catchEachDuplicate);
                return view('patients.showDuplicates')->with(['catchEachDuplicate' => $catchEachDuplicate, 'criteria' => $criteria]);
            } elseif (count($criteria) == 4) {
                $duplicates = Patient::select($criteria[0], $criteria[1], $criteria[2], $criteria[3])
                    ->where('merged', 0)
                    ->where('is_drop', 0)
                    ->groupBy($criteria[0], $criteria[1], $criteria[2], $criteria[3])
                    ->havingRaw('COUNT(*) > 1')
                    ->get()
                    ->toArray();
                $catchEachDuplicate = [];
                foreach ($duplicates as $duplicate) {
                    $patients = Patient::where(
                        [
                            [$criteria[0], $duplicate[$criteria[0]]],
                            [$criteria[1], $duplicate[$criteria[1]]],
                            [$criteria[2], $duplicate[$criteria[2]]],
                            [$criteria[3], $duplicate[$criteria[3]]],
                            ['merged', 0],
                            ['is_drop', 0],
                        ]
                    )->get();
                    array_push($catchEachDuplicate, $patients);
                }
                $catchEachDuplicate = $this->checkForPairs($catchEachDuplicate);
                return view('patients.findDuplicates')->with(['catchEachDuplicate' => $catchEachDuplicate, 'criteria' => $criteria]);
            } else {
                return redirect()->action('PatientsController@findDuplicates');
            }
        }

        return redirect()->action('PatientsController@findDuplicates');
    }

    public function checkForpairs($duplicateArray)
    {
        collect($duplicateArray)->each(function ($duplicatePair, $index) use (&$duplicateArray) {
            $idArray = [];
            collect($duplicatePair)->each(function ($pair) use (&$idArray) {
                array_push($idArray, $pair['id']);
            });
            if (DuplicatePair::whereJsonContains('pairs', $idArray)->exists()) {
                unset($duplicateArray[$index]);
            }
        });

        return $duplicateArray;
    }

    /**
     * Merge between two records.
     * @param $request
     * @return PatientsController@findDuplicates
     */
    public function merge(Request $request)
    {
        $this->authorize('merge', Patient::class);

        $patient = new Patient();
        $first_patient = $patient->find($request->firstp_id);
        $second_patient = $patient->find($request->secondp_id);
        if ($request->has('Keep')) {
            $array_pair = [$first_patient->id, $second_patient->id];
            $patient->keepPairs($array_pair);

            return redirect()->action('PatientsController@findDuplicates')->with('status', ' Rows Kept as Non Duplicates');
        }

        $allrelatedIds = $patient->combinePairIds($first_patient->related_ids, $second_patient->related_ids);
        $allrelatedIds = $patient->addLocalPatientIds($first_patient->local_patient_id, $second_patient->local_patient_id, $allrelatedIds);
        $consent = $patient->addConsentList($first_patient->consent, $second_patient->consent);
        //creating a new patient
        $hybrid_patient = new Patient([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'local_patient_id' => collect([$first_patient, $second_patient])->sortByDesc('updated_at')->first()->local_patient_id,
            'birthdate' => $request->birthdate,
            'weight' => $request->weight,
            'gender' => $request->gender,
            'other_id' => $request->other_id,
            'group_id' => $request->group_id,
            'consent' => $consent,
            'related_ids' => $allrelatedIds,
        ]);
        $hybrid_patient->save();
        $first_patient->medical_cases()->each(function (MedicalCase $case) use (&$hybrid_patient) {
            $case->patient_id = $hybrid_patient->id;
            $case->save();
        });
        $second_patient->medical_cases()->each(function (MedicalCase $case) use (&$hybrid_patient) {
            $case->patient_id = $hybrid_patient->id;
            $case->save();
        });
        //making the first person and second person record termed as merged
        $first_patient->merged = 1;
        $first_patient->merged_with = $second_patient->local_patient_id;
        $first_patient->save();
        $second_patient->merged = 1;
        $second_patient->merged_with = $first_patient->local_patient_id;
        $second_patient->save();
        $criteria = $request->input('searchCriteria');

        return redirect()->route('patients.findDuplicates')->with([
            'status' => 'Success',
            'searchCriteria' => $criteria,
        ]);
    }

    /**
     * Delete a particular patient record.
     * @param $request
     * @return View
     */
    public function destroy(Request $request)
    {
        $patient = Patient::find($request->patient_id);
        if ($patient->medical_cases) {
            foreach ($patient->medical_cases as $case) {
                $case->diagnoses_references->each->delete();
            }
            $patient->medical_cases->each->delete();
        }
        $old_patient = clone $patient;
        if ($patient->delete()) {
            Log::info('User with id ' . Auth::user()->id . ' removed a patient.', ['patient' => $old_patient]);

            return redirect()->action(
                'PatientsController@findDuplicates'
            )->with('status', 'Row Deleted!');
        }
    }
}
