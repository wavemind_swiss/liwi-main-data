<?php

namespace App\Http\Controllers;

use App\HealthFacility;
use App\Patient;
use App\MedicalCase;
use Illuminate\Support\Facades\DB;

class FacilitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->authorizeResource(HealthFacility::class);
    }

    public function index()
    {
        $dateToday = now()->toDateString();

        $facilities = DB::table('health_facilities as hf')
            ->select(
                'hf.name',
                'hf.group_id',
                DB::raw('(SELECT COUNT(medical_cases.id) FROM medical_cases WHERE hf.group_id = medical_cases.group_id) AS medical_cases_count'),
                DB::raw('(SELECT COUNT(medical_cases.id) FROM medical_cases WHERE hf.group_id = medical_cases.group_id AND DATE(medical_cases.consultation_date) = ?) AS medical_cases_today_count'),
                DB::raw('(SELECT COUNT(DISTINCT p.local_patient_id) FROM medical_cases mc INNER JOIN patients p ON mc.patient_id = p.id WHERE hf.group_id = mc.group_id) AS patients_count'),
                DB::raw('(SELECT id FROM medical_cases WHERE hf.group_id = medical_cases.group_id ORDER BY id DESC LIMIT 1) AS latest_medical_case_id'),
                DB::raw('(SELECT patient_id FROM medical_cases WHERE hf.group_id = medical_cases.group_id ORDER BY id DESC LIMIT 1) AS latest_patient_id')
            )
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('medical_cases')
                    ->whereRaw('hf.group_id = medical_cases.group_id');
            })
            ->setBindings([$dateToday])
            ->get();

        foreach ($facilities as $facility) {
            $facility->latest_patient_updated_at = Patient::find($facility->latest_patient_id)->updated_at ?? 'No cases';
            $facility->latest_patient_consultation_date =  MedicalCase::find($facility->latest_medical_case_id)->consultation_date ?? 'No cases';
        }

        return view('facilities.index')->with('facilities', $facilities);
    }
}
