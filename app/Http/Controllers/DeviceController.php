<?php

namespace App\Http\Controllers;

use App\Device;
use App\Http\Requests\AutoDiagnosticsRequest;
use App\Http\Requests\DeviceRequest;
use App\Http\Resources\Device as DeviceResource;
use App\Jobs\CheckAutoDiagnosticJson;
use App\Services\AutoDiagnosticService;
use App\Services\DeviceService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class DeviceController extends Controller
{
    protected $deviceService;

    /**
     * @var AutoDiagnosticService
     */
    protected $autoDiagnosticService;

    /**
     * __construct.
     *
     * @param  DeviceService $deviceService
     * @param  AutoDiagnosticService $autoDiagnosticService
     * @return void
     */
    public function __construct(DeviceService $deviceService, AutoDiagnosticService $autoDiagnosticService)
    {
        $this->deviceService = $deviceService;
        $this->autoDiagnosticService = $autoDiagnosticService;
        $this->authorizeResource(Device::class);
    }

    public function manageTokens($id)
    {
        $device = Device::find($id);
        $user = Auth::user();
        $tokens = $user->tokens;

        $tokens = $tokens->filter(function ($token, $key) use ($device) {
            return $token->client_id == $device->oauth_client_id && $token->revoked == false;
        });

        return response()->json([
            'nbTokens' => $tokens->count(),
            'deviceName' => $device->name,
        ]);
    }

    public function revokeTokens($id)
    {
        $device = Device::find($id);
        $user = Auth::user();
        $tokens = $user->tokens;

        $tokens = $tokens->filter(function ($token, $key) use ($device) {
            return $token->client_id == $device->oauth_client_id;
        });

        foreach ($tokens as $token) {
            $this->revokeAccessAndRefreshTokens($token->id);
        }

        return response()->json([
            'message' => 'revoked tokens',
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index($type = ''): view
    {
        //todo refacto this function with "when"
        $types = Device::select('type')->distinct()->get()->pluck('type')->toArray();

        if (!empty($type) && !in_array($type, $types)) {
            return view('errors.404');
        }

        if (empty($type)) {
            $devices = Device::with('auto_diagnostics')->get()->each(function (Device $device) {
                if (!$device->health_facility_id) {
                    $device->oauth_client_id = 'Not assigned';
                }
            });
        }

        if (in_array($type, $types)) {
            $devices = Device::with('auto_diagnostics')->where('type', $type)->with('auto_diagnostics')->get()->each(function (Device $device) {
                if (!$device->health_facility_id) {
                    $device->oauth_client_id = 'Not assigned';
                }
            });
        }

        $devices_resources = DeviceResource::collection($devices);

        return view('devices.index', ['devices' => $devices_resources->toJson()]);
    }

    /**
     * Display the Hubs Manager.
     */
    public function hubManager(): view
    {
        $devices = Device::select('id', 'name', 'type')->with('health_facility')->withCount('auto_diagnostics')->where('type', 'hub')->get();

        $formatted_devices = $this->autoDiagnosticService->getFormattedDevices($devices);

        return view('devices.hubmanager', ['devices' => $formatted_devices]);
    }

    /**
     * Display the recent error file.
     */
    public function getRecentErrors(Device $device, string $file)
    {
        $full_path_file = config('medal.storage.json_diag_dir') . '/' . $file;

        if (Storage::missing($full_path_file)) {
            return redirect()->back()->withErrors('File not found');
        }

        $diag_file = Storage::get($full_path_file);
        $auto_diag = $device->auto_diagnostics()->where('device_id', $device->id)->where('path', $full_path_file)->first();

        if (empty($auto_diag)) {
            return redirect()->back()->withErrors('Diagnostic not found');
        }

        $json_diagnostic = $this->autoDiagnosticService->generateRecentErrors($auto_diag, $diag_file);

        return view('devices.recent_errors', [
            'device' => $device,
            'file' => json_encode($diag_file),
            'json_diagnostic' => $json_diagnostic,
        ]);
    }

    /**
     * showAutoDiagnostics.
     *
     * @param  int $device_id
     * @return View
     */
    public function showAutoDiagnostics(Device $device)
    {
        return view('devices.showautoDiagnostic')->with([
            'device' => $device,
            'json_diagnostic' => $this->autoDiagnosticService->generateAutoDiagnostic($device),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(DeviceRequest $request)
    {
        $validated = $request->validated();
        $device = $this->deviceService->add($validated);
        Log::info('User with id ' . Auth::user()->id . ' created a new device.', ['device' => $device]);

        return response()->json(new DeviceResource($device));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Device  $device
     * @return JsonResponse
     */
    public function update(DeviceRequest $request, Device $device)
    {
        $old_device = clone $device;
        $validated = $request->validated();
        $device = $this->deviceService->update($validated, $device);
        Log::info('User with id ' . Auth::user()->id . ' updated a device.', ['old_device' => $old_device, 'new_device' => $device]);

        return response()->json(new DeviceResource($device));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Device  $device
     * @return JsonResponse
     */
    public function destroy(Device $device)
    {
        //Remove the device and Revoke the associated OAuth client
        if (!$device->delete()) {

            return response()->json([
                'message' => 'An error occured',
            ], 401);
        }

        // Delete corresponding client
        $clientRepository = app('Laravel\Passport\ClientRepository');
        $client = $clientRepository->findForUser($device->oauth_client_id, Auth::user()->id);
        if ($client !== null) {
            try {
                $clientRepository->delete($client);
            } catch (Exception $e) {
                Log::error("Error while deleting Device {$device->oauth_client_id}");
                report($e);
                return response()->json([
                    'message' => 'An error occured',
                ], 401);
            }
        }
        //todo keep this ?
        // DB::table('oauth_clients')->where('name', '=', $device->name)->delete();

        return response()->json([
            'id' => "{$device->id}",
            'message' => 'Deleted',
        ], 200);
    }

    private function revokeAccessAndRefreshTokens($tokenId)
    {
        $tokenRepository = app('Laravel\Passport\TokenRepository');
        $refreshTokenRepository = app('Laravel\Passport\RefreshTokenRepository');

        $tokenRepository->revokeAccessToken($tokenId);
        $refreshTokenRepository->revokeRefreshTokensByAccessTokenId($tokenId);
    }

    public function storeDeviceDiagnostics(AutoDiagnosticsRequest $request)
    {
        try {
            $validated = $request->validated();
            $hub = Device::where([
                ['oauth_client_id', $validated['auth_id']],
                ['type', 'hub'],
            ])->first();

            if (!$hub) {
                Log::error('Hub (oauth_client_id) ' . $validated['auth_id'] . ' not found');

                return $this->respondDiagnostics();
            }

            if (!$this->makeAutoDiagDir()) {
                Log::error('Error while creating json_diag folder');

                return $this->respondDiagnostics();
            }

            $filename = $hub->id . '_' . Carbon::now()->timestamp . '.json';
            $file_path = config('medal.storage.json_diag_dir') . '/' . $filename;
            $file_saved = Storage::put(
                config('medal.storage.json_diag_dir') . '/' . $filename,
                json_encode($validated)
            );

            if (!$file_saved) {
                Log::error("Error while saving $hub->name hub diagnostic file");

                return $this->respondDiagnostics();
            }

            if (!$hub->auto_diagnostics()->create(['path' => $file_path])) {
                Log::error('Error while saving ' . $file_path . 'entry in database');

                return $this->respondDiagnostics();
            }

            CheckAutoDiagnosticJson::dispatch($filename, $hub)->onQueue('high');

            Log::info("Successfully saved $hub->name diagnostic");

            return $this->respondDiagnostics(true);
        } catch (HttpResponseException $e) {
            report($e);

            return $this->respondDiagnostics();
        } catch (Exception $e) {
            report($e);

            return $this->respondDiagnostics();
        }
    }

    private function makeAutoDiagDir(): bool
    {
        if (!Storage::exists(config('medal.storage.json_diag_dir'))) {
            Storage::makeDirectory(config('medal.storage.json_diag_dir'));
        }

        return true;
    }

    /**
     * respondDiagnostics.
     *
     * @param  bool $status
     * @return JsonResponse
     */
    private function respondDiagnostics($status = false): JsonResponse
    {
        if ($status) {
            return response()->json([
                'status' => true,
                'info' => 'received',
            ], 201);
        }

        return response()->json([
            'status' => false,
            'info' => 'Not Authorized',
        ], 401);
    }
}
