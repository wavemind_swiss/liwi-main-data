<?php

namespace App\Http\Controllers;

use App\Answer;
use App\AnswerType;
use App\DiagnosisReference;
use App\Exports\MedicalCaseExport;
use App\HealthFacility;
use App\Jobs\RemoveFollowUp;
use App\MedicalCase;
use App\MedicalCaseAnswer;
use App\Node;
use App\Patient;
use App\User;
use Carbon\Carbon;
use DataTables;
use Excel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class MedicalCasesController extends Controller
{
    /**
     * To block any non-authorized user.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->authorizeResource(MedicalCase::class);
    }

    /**
     * Display all medical Cases.
     * @param Request $request
     * @return View,
     */
    public function index(Request $request)
    {
        $ids = $request->get('ids') ?? [];
        $chunked_ids = array_chunk($ids, 200);

        $medical_cases = MedicalCase::with([
            'patient',
            'facility',
        ])->when($chunked_ids, function ($query, $chunked_ids) {
            foreach ($chunked_ids as $ids) {
                $query = $query->whereIn('id', $ids);
            }
            return $query;
        })->latest('consultation_date');

        return view('medicalCases.index')->with([
            'medicalCases', $medical_cases,
            'ids' => $ids,
        ]);
    }

    public function getMedicalCases(Request $request)
    {
        $searchValue = $request->get('search')['value'];
        $ids = $request->get('ids') ?? [];

        $medical_cases = MedicalCase::with([
            'patient',
            'facility',
        ])->when($searchValue, function ($query, $searchValue) {
            return $query->where(function (Builder $query) use ($searchValue) {
                return $query
                    ->where('medical_cases.id', 'LIKE', "%$searchValue%")
                    ->orWhere('local_medical_case_id', 'LIKE', "%$searchValue%")
                    ->orWhere('consultation_date', 'LIKE', "%$searchValue%")
                    ->orWhere('app_version', 'LIKE', "%$searchValue%")
                    ->orWhereHas('facility', function ($query) use ($searchValue) {
                        $query->where('name', 'ILIKE', "%$searchValue%");
                    })
                    ->orWhereHas('patient', function ($query) use ($searchValue) {
                        $query->where('local_patient_id', 'LIKE', "%$searchValue%");
                    });
            });
        })->when($ids, function ($query, $ids) {
            return $query->whereIn('id', $ids);
        })->select('medical_cases.*');

        return DataTables::eloquent($medical_cases)
            ->addColumn('facility', function (MedicalCase $mc) {
                return optional($mc->facility)->name;
            })
            ->addColumn('local_patient_id', function (MedicalCase $mc) {
                return $mc->patient->local_patient_id;
            })
            ->addColumn('show', function (MedicalCase $mc) {
                if ($mc->is_drop) {
                    return '<a href=' . route('medical-cases.show', [$mc->id]) . " class='btn btn-outline-dark'>Show</a>";
                }

                return '<a href=' . route('medical-cases.show', [$mc->id]) . " class='btn btn-outline-dark'>Show</a>"
                    . '<button onClick="drop(' . $mc->id . ');" class="ml-2 btn btn-outline-danger">Drop</button>';
            })
            ->editColumn('consultation_date', function (MedicalCase $mc) {
                return $mc->consultation_date ? $mc->consultation_date : $mc->created_at;
            })
            ->rawColumns(['show'])
            ->toJson();
    }

    /**
     * Show specific medical Case.
     * @param $id
     * @return View
     */
    public function show(MedicalCase $medical_case)
    {
        $medical_case_info = [];
        foreach ($medical_case->medical_case_answers as $medical_case_answer) {
            if ($medical_case_answer->answer_id == 0) {
                $data = [
                    'answer' => $medical_case_answer->value,
                    'question' => Node::find($medical_case_answer->node_id),
                ];
                array_push($medical_case_info, json_decode(json_encode($data)));
            } else {
                $data = [
                    'answer' => Answer::find($medical_case_answer->answer_id)->label,
                    'question' => Node::find($medical_case_answer->node_id),
                ];
                array_push($medical_case_info, json_decode(json_encode($data)));
            }
        }
        $diagnoses = DiagnosisReference::getDiagnoses($medical_case->id);
        $data = [
            'medicalCase' => $medical_case,
            'medicalCaseInfo' => $medical_case_info,
            'diagnoses' => $diagnoses,
        ];

        return view('medicalCases.show')->with($data);
    }

    /**
     * Drop for analysis individual Medical Case.
     * @param int $mc_id
     * @return JsonResponse
     */
    public function dropForAnalysis(int $mc_id): JsonResponse
    {
        $mc = MedicalCase::find($mc_id);

        if (empty($mc)) {
            $data = ['message' => 'Medical case not found !'];

            return response()->json($data, 403);
        }

        if ($mc->is_drop) {
            $data = ['message' => 'This medical case is already dropped for analysis!'];

            return response()->json($data, 403);
        }
        $update = $mc->update(['is_drop' => true]);

        if (!$update) {
            $data = ['message' => 'Error while updating the medical case!'];

            return response()->json($data, 520);
        }

        Log::info('User ' . Auth::user()->id . ' dropped for analysis mc ' . $mc->id);
        $data = ['message' => 'Medical case successfully dropped for analysis'];

        return response()->json($data);
    }

    /**
     * Drop for analysis individual Medical Case.
     * @param Request $request
     * @return JsonResponse
     */
    public function bulkDropForAnalysis(Request $request): JsonResponse
    {
        $mcs = MedicalCase::whereIn('local_medical_case_id', $request->ids);

        if ($mcs->get()->isEmpty()) {
            $data = ['message' => 'Medical cases not found !'];
            return response()->json($data, 403);
        }

        try {
            DB::beginTransaction();
            $mcs->update(['is_drop' => true]);
            Log::info($mcs->update(['is_drop' => true]));
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $data = ['message' => 'Error while updating the medical case!'];
            Log::error("Error while updating the medical case!");
            report($e);

            return response()->json($data, 520);
        }

        Log::info('User ' . Auth::user()->id . ' dropped for analysis mcs');
        $data = ['message' => 'Medical cases successfully dropped for analysis'];

        return response()->json($data);
    }

    /**
     * Compare between two Medical cases.
     * @params firstId
     * @params secondId
     * @return view
     * @return $data
     */
    public function compare($first_id, $second_id)
    {
        $first_medical_case = MedicalCase::find($first_id);
        $second_medical_case = MedicalCase::find($second_id);
        $medical_case_info = $this->comparison($first_medical_case, $second_medical_case);
        $data = [
            'first_medical_case' => $first_medical_case,
            'second_medical_case' => $second_medical_case,
            'medical_case_info' => $medical_case_info,
        ];

        return view('medicalCases.compare')->with($data);
    }

    /**
     * Compare questions between two Medical cases.
     * @params first_medical_case
     * @params second_medical_case
     * @return $all_questions
     */
    public static function comparison($first_medical_case, $second_medical_case)
    {
        $first_case_questions = [];
        $first_medical_case->medical_case_answers->each(function ($answer) use (&$first_case_questions) {
            $first_case_questions[] = $answer->node_id;
        });
        $second_case_questions = [];
        $second_medical_case->medical_case_answers->each(function ($answer) use (&$second_case_questions) {
            $second_case_questions[] = $answer->node_id;
        });
        $common_questions_id = array_intersect(
            $first_case_questions,
            $second_case_questions
        );
        $common_questions = [];
        foreach ($common_questions_id as $question_id) {
            $first_case_answer = $first_medical_case->medical_case_answers->where('node_id', $question_id)->first();
            $first_answer = $first_case_answer->value;
            if ($first_case_answer->answer) {
                $first_answer = $first_case_answer->answer->label;
            }
            $second_case_answer = $second_medical_case->medical_case_answers->where('node_id', $question_id)->first();
            $second_answer = $first_case_answer->value;
            if ($second_case_answer->answer) {
                $second_answer = $second_case_answer->answer->label;
            }
            $common_questions[] = [
                'medal_c_id' => $second_case_answer->node->medal_c_id,
                'question_label' => $second_case_answer->node->label,
                'question_stage' => $second_case_answer->node->stage,
                'first_answer' => $first_answer,
                'second_answer' => $second_answer,
            ];
        }
        $uncommon_questions_id = array_merge(
            array_diff($first_case_questions, $second_case_questions),
            array_diff($second_case_questions, $first_case_questions)
        );
        $uncommon_questions = [];
        foreach ($uncommon_questions_id as $question_id) {
            // finding for the first medical case
            $first_case_answer = $first_medical_case->medical_case_answers->where('node_id', $question_id)->first();
            if ($first_case_answer) {
                $first_answer = $first_case_answer->value;
                if ($first_case_answer->answer) {
                    $first_answer = $first_case_answer->answer->label;
                }
                $uncommon_questions[] = [
                    'medal_c_id' => $first_case_answer->node->medal_c_id,
                    'question_label' => $first_case_answer->node->label,
                    'question_stage' => $first_case_answer->node->stage,
                    'first_answer' => $first_answer,
                    'second_answer' => $second_answer = null,
                ];
            }
            $second_case_answer = $second_medical_case->medical_case_answers->where('node_id', $question_id)->first();
            if ($second_case_answer) {
                $second_answer = $second_case_answer->value;
                if ($second_case_answer->answer) {
                    $second_answer = $second_case_answer->answer->label;
                }
                $uncommon_questions[] = [
                    'medal_c_id' => $second_case_answer->node->medal_c_id,
                    'question_label' => $second_case_answer->node->label,
                    'question_stage' => $second_case_answer->node->stage,
                    'first_answer' => $first_answer = null,
                    'second_answer' => $second_answer,
                ];
            }
        }
        $all_questions = array_merge($common_questions, $uncommon_questions);
        $all_questions = array_filter($all_questions, function ($question) {
            return !(empty($question['first_answer']) && empty($question['second_answer']));
        });

        return $all_questions;
    }

    /**
     * Display an answer of a specific medical case.
     * @params $medicalCaseId
     * @params $questionId
     * @return View
     * @return $question
     */
    public function medicalCaseQuestion($medical_case_id, $question_id)
    {
        $this->authorize('question', MedicalCase::class);

        $answers = [];
        $question = Node::find($question_id);
        $answer_type = AnswerType::find($question->answer_type_id);
        foreach ($question->answers as $answer) {
            $answers[$answer->id] = $answer->label;
        }
        $data = [
            'medicalCase' => MedicalCase::find($medical_case_id),
            'question' => $question,
            'answers' => $answers,
            'answer_type' => $answer_type,
        ];

        return view('medicalCases.question')->with($data);
    }

    /**
     * Find the details of the medical case.
     * @params $medical Case
     * @params $label_info
     * @params $medical_case_info
     * @return $medical_case_info
     */
    public static function detailFind($medical_case, $label_info, $medical_case_info = [])
    {
        foreach ($medical_case->medical_case_answers as $medicalCaseAnswer) {
            if ($medicalCaseAnswer->answer_id == 0) {
                $answer = $medicalCaseAnswer->value;
                $question = Node::find($medicalCaseAnswer->node_id);
                // $medicalCaseAnswer = $medicalCaseAnswer;
                $medical_case_info[$question->id]['question'] = $question;
                $medical_case_info[$question->id][$label_info] = [
                    'answer' => $answer,
                    'medicalCaseAnswer' => $medicalCaseAnswer,
                ];
            } else {
                $answer = Answer::find($medicalCaseAnswer->answer_id);
                $question = Node::find($medicalCaseAnswer->node_id);
                // $medicalCaseAnswer = $medicalCaseAnswer;
                $medical_case_info[$question->id]['question'] = $question;
                $medical_case_info[$question->id][$label_info] = [
                    'answer' => $answer,
                    'medicalCaseAnswer' => $medicalCaseAnswer,
                ];
            }
        }

        return $medical_case_info;
    }

    /**
     * Show Audit Trail of a particular medical Case.
     * @params $medical_case_id
     * @return View
     * @return $medicalCaseAudits
     */
    public function showCaseChanges($medical_case_id)
    {
        $this->authorize('changes', MedicalCase::class);

        $medicalCase = MedicalCase::find($medical_case_id);
        $all_audits = [];
        foreach ($medicalCase->medical_case_answers as $medicalCaseAnswer) {
            $medicalCaseAudit = MedicalCaseAnswer::getAudit($medicalCaseAnswer->id);
            $medicalCaseAuditSize = count($medicalCaseAudit->toArray());
            if ($medicalCaseAuditSize > 0) {
                foreach ($medicalCaseAudit as $audit) {
                    $audit_array = [
                        'user' => User::find($audit->user_id)->name,
                        'question' => Node::find($medicalCaseAnswer->node_id)->label,
                        'old_value' => Answer::find($audit->old_values['answer_id'])->label,
                        'new_value' => Answer::find($audit->new_values['answer_id'])->label,
                        'url' => $audit->url,
                        'event' => $audit->event,
                        'ip_address' => $audit->ip_address,
                        'created_at' => $audit->created_at,
                    ];
                    array_push($all_audits, $audit_array);
                }
            }
        }
        $data = [
            'allAudits' => $all_audits,
            'medicalCaseId' => $medical_case_id,
        ];

        return view('medicalCases.showCaseChanges')->with($data);
    }

    /**
     * Find duplicates by a certain value.
     * @return View
     * @return $catchEachDuplicate
     */
    public function findDuplicates()
    {
        return view('medicalCases.showDuplicates2');
    }

    public function findDuplicates2()
    {
        $case_columns = ['patient_id', DB::raw('Date(consultation_date)'), DB::raw('COUNT(*) as count')];

        $medicalCases = MedicalCase::notDropped()->with(['patient', 'facility'])
            ->where('duplicate', false)
            ->whereDate('consultation_date', '<=', Carbon::now())
            ->whereIn('medical_cases.patient_id', function ($query) use ($case_columns) {
                $query->select($case_columns[0])
                    ->fromSub(function ($query) use ($case_columns) {
                        $query->select($case_columns)
                            ->from('medical_cases')
                            ->groupBy(DB::raw('Date(consultation_date)'), 'patient_id')
                            ->havingRaw('COUNT(*) > 1');
                    }, 'medical_cases');
            })
            ->orderByDesc('consultation_date')
            ->get();

        $medicalCases->each(function (MedicalCase $medicalCase) {
            $medicalCase->hf = $medicalCase->facility->name ?? '';
            $medicalCase->consultation_date = Carbon::createFromFormat('Y-m-d H:i:s', $medicalCase->consultation_date)->format('Y-m-d');
        });

        $medicalCases = $medicalCases->groupBy(function ($item) {
            return $item['consultation_date'] . $item['patient_id'];
        })->filter(function ($item) {
            return $item->count() > 1;
        });

        return response()->json(['mcs' => array_values($medicalCases->toArray())]);
    }

    public function deduplicate_redcap(Request $request)
    {
        $validated = $request->validate([
            'medicalc_id' => 'required',
        ]);
        $medicalCase = MedicalCase::find((int) $request->input('medicalc_id'));
        dispatch((new RemoveFollowUp($medicalCase))->onQueue('high'));

        return redirect()->action(
            'MedicalCasesController@findDuplicates'
        )->with('status', "Follow Up for '{$medicalCase->local_medical_case_id}' is Queued for removal in redcap");
    }

    /**
     * Search duplicates.
     * @params $request
     * @return view
     */
    public function searchDuplicates(Request $request)
    {
        $this->authorize('duplicates', MedicalCase::class);

        $columns = Schema::getColumnListing('medical_cases');
        $search_value = $request->search;
        if (in_array($search_value, $columns)) {
            $duplicates = MedicalCase::select($search_value)
                ->groupBy($search_value)
                ->havingRaw('COUNT(*) > 1')
                ->get();
            $catch_each_duplicate = [];
            foreach ($duplicates as $duplicate) {
                $case_duplicates = MedicalCase::where($search_value, $duplicate->$search_value)->get();
                array_push($catch_each_duplicate, $case_duplicates);
            }

            return view('medicalCases.showDuplicates')->with('catchEachDuplicate', $catch_each_duplicate);
        }

        return redirect()->action(
            'medical-cases.findDuplicates'
        );
    }

    /**
     * Delete a particular medical case record.
     * @params $request
     * @return View
     */
    public function destroy(Request $request)
    {
        $medical_case = Patient::find($request->medicalc_id);
        if ($medical_case->medical_case_answers) {
            $medical_case->medical_case_answers->each->delete();
        }
        $old_medical_case = clone $medical_case;
        if ($medical_case->delete()) {
            Log::info('User with id ' . Auth::user()->id . ' removed a medical case.', ['medical_case' => $old_medical_case]);

            return redirect()->action(
                'MedicalCasesController@findDuplicates'
            )->with('status', 'Row Deleted!');
        }
    }

    public function followUpDelayed()
    {
        $this->authorize('followUp', MedicalCase::class);

        $all_unfollowed = new MedicalCase;

        return view('medicalCases.unfollowed')->with('unfollowed', $all_unfollowed->listUnfollowed());
    }

    public function showFacilities()
    {
        $this->authorize('followUp', MedicalCase::class);

        return view('medicalCases.followed')->with('facilities', $facilities = HealthFacility::all());
    }

    public function showFacility($id)
    {
        $this->authorize('followuP', MedicalCase::class);

        $facility_exist = HealthFacility::where('group_id', $id)->first();
        if (!$facility_exist) {
            return response()->json([
                'facility' => false,
            ]);
        }

        $patients = Patient::where('group_id', $id)->get();
        $count_redcap_sent = 0;
        $count_redcap_unsent = 0;
        foreach ($patients as $patient) {
            foreach ($patient->medical_cases as $case) {
                if ($case->redcap) {
                    $count_redcap_sent += 1;
                } else {
                    $count_redcap_unsent += 1;
                }
            }
        }

        return response()->json([
            'facility' => true,
            'data' => [
                'redcap_sent' => $count_redcap_sent,
                'redcap_unsent' => $count_redcap_unsent,
            ],
        ]);
    }

    public function medicalCaseIntoExcel()
    {
        return Excel::download(new MedicalCaseExport, 'medicalCases.xlsx');
    }

    public function medicalCaseIntoCsv()
    {
        return Excel::download(new MedicalCaseExport, 'medicalCases.csv');
    }
}
