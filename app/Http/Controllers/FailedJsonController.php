<?php

namespace App\Http\Controllers;

use App\HealthFacility;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use SplFileInfo;

class FailedJsonController extends Controller
{
    /**
     * To block any non-authorized user.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:Access_Failed_Json_Panel');
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!Auth::check()) {
            return;
        }

        $path = storage_path('app/' . Config::get('medal.storage.json_failure_dir') . '/');
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 50;

        if (!File::exists($path)) {
            return view('failed.index', [
                'jsons' => collect([]),
                'total' => 0,
            ]);
        }

        $jsons = collect(File::files($path))
            ->sortByDesc(function (SplFileInfo $file) {
                return $file->getCTime();
            });

        $total = $jsons->count();

        $currentPageItems = $jsons->slice(($currentPage * $perPage) - $perPage, $perPage)
            ->map(function (SplFileInfo $file) {
                $json_content = json_decode(File::get($file->getRealPath()), true);
                $file->name = $file->getBaseName();

                // In case of an empty JSON
                if (!$json_content || !is_array($json_content)) {
                    $file->group_id = '';
                    $file->date = 'Empty json';
                    $file->version_id = '';
                    $file->json_version = '';
                    $file->hf_name = '';

                    return $file;
                }

                // In case of a corrupted JSON
                if (
                    !json_last_error() === JSON_ERROR_NONE ||
                    !$json_content['patient'] ||
                    !$json_content['patient']['group_id'] ||
                    !is_int($json_content['patient']['group_id'])
                ) {
                    $file->group_id = '';
                    $file->date = 'Corrupted json';
                    $file->version_id = '';
                    $file->json_version = '';
                    $file->hf_name = '';

                    return $file;
                }

                $hf = HealthFacility::where('group_id', $json_content['patient']['group_id'])->first();
                $file->hf_name = optional($hf)->name;
                $file->group_id = $json_content['patient']['group_id'] ?? '';
                $file->date = Carbon::createFromTimestampMs($json_content['patient']['createdAt'])->format('Y-m-d');
                $file->version_id = $json_content['version_id'] ?? '';
                $file->json_version = $json_content['json_version'] ?? '';

                return $file;
            });

        $paginatedJsons = new LengthAwarePaginator($currentPageItems, $jsons->count(), $perPage);
        $paginatedJsons->setPath($request->url());

        return view('failed.index', [
            'jsons' => $paginatedJsons,
            'total' => $total,
        ]);
    }
}
