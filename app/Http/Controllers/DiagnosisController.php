<?php

namespace App\Http\Controllers;

use App\DiagnosisReference;
use DataTables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class DiagnosisController extends Controller
{
    public function index()
    {
        $diagnoses_references = DiagnosisReference::with([
            'medical_case',
            'medical_case.patient',
            'medical_case.facility',
            'diagnosis',
        ])->orderBy('created_at');

        return view('diagnoses.index')->with('diagnoses', $diagnoses_references);
    }

    /**
     * Process ajax request.
     * @param $request
     */
    public function getDiagnoses(Request $request)
    {
        $searchValue = $request->get('search')['value'];

        $diagnosis_ref = DiagnosisReference::with([
            'medical_case',
            'medical_case.patient',
            'medical_case.facility',
            'diagnosis',
        ])->when($searchValue, function ($query, $searchValue) {
            return $query->where(function (Builder $query) use ($searchValue) {
                return $query
                    ->where('diagnosis_references.id', 'LIKE', "%$searchValue%")
                    ->orWhereHas('medical_case', function ($query) use ($searchValue) {
                        $query->where('local_medical_case_id', 'ILIKE', "%$searchValue%");
                    })
                    ->orWhereHas('medical_case.patient', function ($query) use ($searchValue) {
                        $query->where('local_patient_id', 'ILIKE', "%$searchValue%");
                    })
                    ->orWhereHas('diagnosis', function ($query) use ($searchValue) {
                        $query->where('label', 'ILIKE', "%$searchValue%");
                    })
                    ->orWhereHas('medical_case.facility', function ($query) use ($searchValue) {
                        $query->where('name', 'ILIKE', "%$searchValue%");
                    });
            });
        })->select('diagnosis_references.*');

        return DataTables::eloquent($diagnosis_ref)
            ->addColumn('local_medical_case_id', function (DiagnosisReference $diag_ref) {
                return optional($diag_ref->medical_case)->local_medical_case_id;
            })
            ->addColumn('local_patient_id', function (DiagnosisReference $diag_ref) {
                return optional($diag_ref->medical_case)->patient->local_patient_id;
            })
            ->addColumn('diagnosis_label', function (DiagnosisReference $diag_ref) {
                return optional($diag_ref->diagnoses)->label;
            })
            ->addColumn('facility_name', function (DiagnosisReference $diag_ref) {
                return optional($diag_ref->medical_case)->facility->name;
            })
            ->editColumn('status', function (DiagnosisReference $diag_ref) {
                return $diag_ref->status->description;
            })
            ->rawColumns(['show'])
            ->toJson();
    }
}
