<?php

namespace App\Http\Controllers;

use App\DrugReference;
use DataTables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class DrugController extends Controller
{
    public function index()
    {
        $drug_references = DrugReference::with([
            'diagnosisReference',
            'diagnosisReference.diagnoses',
            'diagnosisReference.medical_case',
            'drug',
            'drug.formulations',
            'customDiagnosisReference',
            'customDiagnosisReference.medical_case',
            'customDiagnosisReference.custom_drugs',
        ])->orderBy('created_at');

        return view('drugs.index')->with('drugs', $drug_references);
    }

    /**
     * Process ajax request.
     * @param $request
     */
    public function getDrugs(Request $request)
    {
        $searchValue = $request->get('search')['value'];

        $drug_refs = DrugReference::with([
            'diagnosisReference',
            'diagnosisReference.diagnoses',
            'diagnosisReference.medical_case',
            'drug',
            'drug.formulations',
            'customDiagnosisReference',
            'customDiagnosisReference.medical_case',
            'customDiagnosisReference.custom_drugs',
        ])->when($searchValue, function ($query, $searchValue) {
            return $query->where(function (Builder $query) use ($searchValue) {
                return $query
                    ->where('drug_references.id', 'LIKE', "%$searchValue%")
                    ->orWhere('drug_references.status', 'LIKE', "%$searchValue%")
                    ->orWhereHas('diagnosisReference.medical_case', function ($query) use ($searchValue) {
                        $query->where('local_medical_case_id', 'ILIKE', "%$searchValue%");
                    })
                    ->orWhereHas('customDiagnosisReference.medical_case', function ($query) use ($searchValue) {
                        $query->where('local_medical_case_id', 'ILIKE', "%$searchValue%");
                    })
                    ->orWhereHas('drug', function ($query) use ($searchValue) {
                        $query->where('label', 'ILIKE', "%$searchValue%");
                    })
                    ->orWhereHas('diagnosisReference.diagnoses', function ($query) use ($searchValue) {
                        $query->where('label', 'ILIKE', "%$searchValue%");
                    })
                    ->orWhereHas('customDiagnosisReference', function ($query) use ($searchValue) {
                        $query->where('label', 'ILIKE', "%$searchValue%");
                    })
                    ->orWhereHas('drug.formulations', function ($query) use ($searchValue) {
                        $query->where('description', 'ILIKE', "%$searchValue%");
                    });
            });
        })->select('drug_references.*');

        return DataTables::eloquent($drug_refs)
            ->addColumn('local_medical_case_id', function (DrugReference $drug_ref) {
                $diag_ref = $drug_ref->diagnosisReference;
                if (optional($drug_ref->customDiagnosisReference)->exists()) {
                    $diag_ref = $drug_ref->customDiagnosisReference;
                }

                return optional($diag_ref->medical_case)->local_medical_case_id;
            })
            ->addColumn('diagnosis_label', function (DrugReference $drug_ref) {
                if (!$diag_ref = $drug_ref->diagnosisReference) {
                    return null;
                }

                return optional($diag_ref->diagnoses)->label;
            })
            ->addColumn('custom_diagnosis_label', function (DrugReference $drug_ref) {
                if (!$diag_ref = $drug_ref->customDiagnosisReference) {
                    return null;
                }

                return $diag_ref->label;
            })
            ->addColumn('drug_label', function (DrugReference $drug_ref) {
                return optional($drug_ref)->drug->label;
            })
            ->editColumn('formulation', function (DrugReference $drug_ref) {
                return optional($drug_ref->formulation)->description;
            })
            ->toJson();
    }
}
