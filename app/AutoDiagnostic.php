<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutoDiagnostic extends Model
{
    protected $fillable = [
        'commit_pos',
        'up_to_date',
        'path',
        'bitbucket_commit',
        'bitbucket_commit_time',
    ];

    public function device()
    {
        return $this->belongsTo(Device::class);
    }
}
