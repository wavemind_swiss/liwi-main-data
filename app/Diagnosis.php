<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Diagnosis.
 *
 * @property int $id
 * @property int $medal_c_id
 * @property string $label
 * @property int $diagnostic_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $type
 * @property int $version_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Drug[] $drugs
 * @property-read int|null $drugs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Management[] $managements
 * @property-read int|null $managements_count
 * @property-read \App\MedicalCase $medical_case
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\MedicalCase[] $medical_cases
 * @property-read int|null $medical_cases_count
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnosis newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnosis newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnosis query()
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnosis whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnosis whereDiagnosticId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnosis whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnosis whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnosis whereMedalCId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnosis whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnosis whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Diagnosis whereVersionId($value)
 * @mixin \Eloquent
 */
class Diagnosis extends Model
{
    protected $guarded = [];

    /**
     * Make drugs relation.
     */
    public function drugs()
    {
        return $this->hasMany('App\Drug');
    }

    /**
     * Make managements relation.
     */
    public function managements()
    {
        return $this->hasMany('App\Management');
    }

    /**
     * Make medical case relation.
     */
    public function medical_case()
    {
        return $this->belongsTo('App\MedicalCase');
    }

    /**
     * @return BelongsToMany
     */
    public function medical_cases(): BelongsToMany
    {
        return $this->belongsToMany(MedicalCase::class)
            ->withPivot('status')
            ->withTimestamps();
    }
}
