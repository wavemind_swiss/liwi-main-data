<?php

namespace App\Support;

use Illuminate\Contracts\Encryption\DecryptException;
use PragmaRX\Google2FALaravel\Support\Authenticator;

class Google2FAAuthenticator extends Authenticator
{
    protected function canPassWithoutCheckingOTP()
    {
        try {
            $this->isActivated();
        } catch (DecryptException $e) {
            return redirect()->route('register.register_2fa');
        }

        return
        !$this->isEnabled() ||
        $this->noUserIsAuthenticated() ||
        !$this->isActivated() ||
        $this->twoFactorAuthStillValid();
    }
}
