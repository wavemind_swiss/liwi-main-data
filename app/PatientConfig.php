<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PatientConfig.
 *
 * @property int $id
 * @property array|null $config
 * @property int $version_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PatientConfig newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PatientConfig newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PatientConfig query()
 * @method static \Illuminate\Database\Eloquent\Builder|PatientConfig whereConfig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientConfig whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientConfig whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientConfig whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PatientConfig whereVersionId($value)
 * @mixin \Eloquent
 */
class PatientConfig extends Model
{
    protected $table = 'patient_configs';

    protected $guarded = [];

    protected $casts = [
        'config' => 'array',
    ];
}
