<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static AGREED()
 * @method static static ADDITIONAL()
 * @method static static REFUSED()
 */
final class ClinicianChoice extends Enum implements LocalizedEnum
{
    public const AGREED = 'agreed';

    public const ADDITIONAL = 'additional';

    public const REFUSED = 'refused';
}
