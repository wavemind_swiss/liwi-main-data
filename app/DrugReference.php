<?php

namespace App;

use App\Enums\ClinicianChoice;
use Illuminate\Database\Eloquent\Model;

/**
 * App\DrugReference.
 *
 * @property int $id
 * @property int $drug_id
 * @property int|null $diagnosis_id
 * @property int|null $formulation_id
 * @property string|null $duration
 * @property int|null $custom_diagnosis_id
 * @property \App\Enums\ClinicianChoice|null $status
 * @property-read \App\CustomDiagnosis|null $customDiagnosisReference
 * @property-read \App\DiagnosisReference|null $diagnosisReference
 * @property-read \App\Drug $drug
 * @property-read \App\Drug|null $drugs
 * @property-read \App\Formulation|null $formulation
 * @method static \Illuminate\Database\Eloquent\Builder|DrugReference newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DrugReference newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DrugReference query()
 * @method static \Illuminate\Database\Eloquent\Builder|DrugReference whereCustomDiagnosisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DrugReference whereDiagnosisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DrugReference whereDrugId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DrugReference whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DrugReference whereFormulationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DrugReference whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DrugReference whereStatus($value)
 * @mixin \Eloquent
 */
class DrugReference extends Model
{
    protected $table = 'drug_references';

    public $timestamps = false;

    protected $guarded = [];

    /**
     * @param $value
     * @return ClinicianChoice|null
     */
    public function getStatusAttribute($value): ?ClinicianChoice
    {
        return ClinicianChoice::coerce($value);
    }

    public function drugs()
    {
        return $this->hasOne(Drug::class, 'id', 'drug_id');
    }

    public function drug()
    {
        return $this->belongsTo(Drug::class, 'drug_id');
    }

    public function formulation()
    {
        return $this->belongsTo(Formulation::class);
    }

    public function diagnosisReference()
    {
        return $this->belongsTo(DiagnosisReference::class, 'diagnosis_id');
    }

    public function customDiagnosisReference()
    {
        return $this->belongsTo(CustomDiagnosis::class, 'custom_diagnosis_id');
    }
}
