<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\HealthFacility.
 *
 * @property int $id
 * @property int|null $group_id
 * @property string|null $facility_name
 * @property string $long
 * @property string $lat
 * @property string $hf_mode
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property string|null $country
 * @property string|null $area
 * @property string|null $local_data_ip
 * @property string|null $pin_code
 * @property int|null $user_id
 * @property int|null $version_json_id
 * @property-read \Illuminate\Database\Eloquent\Collection|Device[] $devices
 * @property-read int|null $devices_count
 * @property-read HealthFacilityAccess|null $healthFacilityAccess
 * @property-read \Illuminate\Database\Eloquent\Collection|JsonLog[] $log_cases
 * @property-read int|null $log_cases_count
 * @property-read \Illuminate\Database\Eloquent\Collection|MedicalCase[] $medical_cases
 * @property-read int|null $medical_cases_count
 * @property-read \Illuminate\Database\Eloquent\Collection|MedicalStaff[] $medical_staff
 * @property-read int|null $medical_staff_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Patient[] $patients
 * @property-read int|null $patients_count
 * @property-read \Illuminate\Database\Eloquent\Collection|MedicalCase[] $patients_medical_cases
 * @property-read int|null $patients_medical_cases_count
 * @property-read VersionJson|null $versionJson
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility query()
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereFacilityName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereHfMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereLocalDataIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereLong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility wherePinCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacility whereVersionJsonId($value)
 * @mixin \Eloquent
 */
class HealthFacility extends Model  implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'name',
        'user_id',
        'local_data_ip',
        'pin_code',
        'lat',
        'long',
        'country',
        'area',
        'council',
        'group_id',
        'hf_mode',
        'is_drop',
    ];

    protected $guarded = [];

    public function medical_cases()
    {
        return $this->hasMany(MedicalCase::class, 'group_id', 'group_id');
    }

    public function log_cases()
    {
        return $this->hasMany(JsonLog::class, 'group_id', 'group_id');
    }

    public function patients()
    {
        return $this->hasMany(Patient::class, 'group_id', 'group_id');
    }

    public function versionJson()
    {
        return $this->hasOne(VersionJson::class);
    }

    public function healthFacilityAccess()
    {
        return $this->hasOne(HealthFacilityAccess::class)->where('access', true);
    }

    public function medical_staff()
    {
        return $this->hasMany(MedicalStaff::class);
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function medical_cases_patients()
    {
        return $this->hasManyThrough(Patient::class, MedicalCase::class, 'group_id', 'id', 'group_id', 'patient_id');
    }

    public function latestMedicalCase()
    {
        return $this->hasOne(MedicalCase::class, 'group_id', 'group_id')->latest('id');
    }

    public function latestMedical_case_patient()
    {
        return $this->hasOneThrough(Patient::class, MedicalCase::class, 'group_id', 'id', 'group_id', 'patient_id')->latest();
    }

    /**
     * Scope a query to only include non dropped mcs.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeNotDropped($query)
    {
        return $query->where('is_drop', false);
    }
}
