<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\VersionJson.
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $health_facility_id
 * @property mixed $json
 * @property mixed|null $emergency_content
 * @property int|null $emergency_content_version
 * @method static \Illuminate\Database\Eloquent\Builder|VersionJson newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VersionJson newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VersionJson query()
 * @method static \Illuminate\Database\Eloquent\Builder|VersionJson whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VersionJson whereEmergencyContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VersionJson whereEmergencyContentVersion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VersionJson whereHealthFacilityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VersionJson whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VersionJson whereJson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VersionJson whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class VersionJson extends Model
{
    //
}
