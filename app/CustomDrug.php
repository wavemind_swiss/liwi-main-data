<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CustomDrug.
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property string $duration
 * @property int|null $custom_diagnosis_id
 * @property int|null $diagnosis_id
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDrug newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDrug newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDrug query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDrug whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDrug whereCustomDiagnosisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDrug whereDiagnosisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDrug whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDrug whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDrug whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomDrug whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CustomDrug extends Model
{
    protected $guarded = [];
}
