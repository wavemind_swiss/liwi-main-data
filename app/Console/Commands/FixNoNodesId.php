<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class FixNoNodesId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'json:fix_no_node_id {dry-run=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will take every failedjson and add the missing nodes id if needed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('dry-run') == 1) {
            $this->info('Dry Run');
        }
        $i = 0;
        $json_success_files = Storage::files('json_failure');

        foreach ($json_success_files as $json) {
            $filename = array_slice(explode('/', $json), -1)[0];
            $data = json_decode(Storage::get($json, true), true);

            if (array_key_exists('nodes', $data)) {
                foreach ($data['nodes'] as $id => &$node) {
                    if (!array_key_exists('id', $node)) {
                        if ($this->argument('dry-run') == 0) {
                            $node['id'] = $id;
                            $newJsonString = json_encode($data);
                            file_put_contents(storage_path('app/' . $json), $newJsonString);
                            Log::info('File ' . $filename . ' updated');
                        }
                        $i++;
                    }
                }
            }
        }
        if ($this->argument('dry-run') == 0) {
            $this->info($i . ' missing nodes id updated');
        } else {
            $this->info($i . ' missing nodes id would have been updated');
        }
    }
}
