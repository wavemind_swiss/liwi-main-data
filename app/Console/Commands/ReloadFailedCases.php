<?php

namespace App\Console\Commands;

use App\Jobs\ProcessCaseJson;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ReloadFailedCases extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cases:reload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $failedFiles = Storage::files(config('medal.storage.json_failure_dir'));
        $fileCount = count($failedFiles);
        if ($fileCount == 0) {
            $this->info('No cases to reload');

            return;
        }

        $this->info("Reloading $fileCount files...");
        foreach ($failedFiles as $failedJson) {
            $filename = array_slice(explode('/', $failedJson), -1)[0];
            $job_found = DB::table('jobs')->where('payload', 'LIKE', "%$filename%")->exists();
            if (!$job_found) {
                ProcessCaseJson::dispatch(config('medal.storage.json_failure_dir'), $filename);
            }
        }
        Log::info($fileCount . ' failed case(s) reloaded.');
    }
}
