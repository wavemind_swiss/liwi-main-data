<?php

namespace App\Console\Commands;

use App\MedicalCase;
use Illuminate\Console\Command;

class ReprocessConsent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'consent:reprocess {dry-run=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This commmand will get set the consent to true to every medical cases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('dry-run') == 1) {
            $this->info('Dry Run');
            $to_update = MedicalCase::where('consent', false)->count();
            $this->info("$to_update mcs will be updated");
        } else {
            MedicalCase::where('consent', false)->query()->update(['consent' => 1]);
        }
    }
}
