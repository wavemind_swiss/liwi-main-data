<?php

namespace App\Console\Commands;

use App\Jobs\ProcessCaseJson;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class ReprocessZip extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zip:reprocess {dry-run=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will take every zip file and reprocess them if they are not already in the json_success folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $total = 0;
        if ($this->argument('dry-run') == 1) {
            $this->info('Dry Run');
        }

        $zips = Storage::files(Config::get('medal.storage.cases_zip_dir'));
        $extractDir = Config::get('medal.storage.json_extract_dir');
        Storage::makeDirectory($extractDir);

        foreach ($zips as $zip) {
            $current_zip = new ZipArchive;
            $res = $current_zip->open(Storage::disk('local')->path($zip));
            if ($res === true) {
                for ($i = 0; $i < $current_zip->numFiles; $i++) {
                    $jsonName = $current_zip->getNameIndex($i);
                    if (!Storage::disk('local')->exists('json_success/' . $jsonName)) {
                        if (!Storage::disk('local')->exists('json_failure/' . $jsonName)) {
                            if ($this->argument('dry-run') == 0) {
                                $file_is_extracted = $current_zip->extractTo(Storage::disk('local')->path($extractDir), $jsonName);
                                if ($file_is_extracted) {
                                    if (!DB::table('jobs')->where('payload', 'LIKE', "%$jsonName%")->exists()) {
                                        if ($this->argument('dry-run') == 0) {
                                            ProcessCaseJson::dispatch($extractDir, $jsonName);
                                        }
                                        $total++;
                                    }
                                } else {
                                    Log::error("Couldn't extract $jsonName.");
                                }
                            }
                        }
                    }
                }
                $current_zip->close();
            }
        }

        if ($this->argument('dry-run') == 0) {
            $this->info($total . ' json have been reprocessed');
        } else {
            $this->info($total . ' json would have been reprocessed');
        }
    }
}
