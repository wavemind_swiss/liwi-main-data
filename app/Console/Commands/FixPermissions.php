<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;

class FixPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will fix all the permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $permissions = [
            'Reset_Own_Password',
            'Access_Follow_Up_Panel',
            'Access_Duplicates_Panel',
            'Access_Facilities_Panel',
            'Access_Health_Facilities_Panel',
            'Access_Devices_Panel',
            'Access_Medical_Staff_Panel',
            'Access_Patients_Panel',
            'Access_Medical_Cases_Panel',
            'Access_Diagnoses_Panel',
            'Access_Drugs_Panel',
            'Access_Export_Panel',
            'Access_Profile_Panel',
            'Access_Reset_Own_Password_Panel',
            'Access_Admin_Corner_Panel',
            'Access_ADMIN_PANEL',
            'Create_User',
            'Delete_User',
            'Manage_Roles',
            'Manage_Users',
            'View_Patients',
            'Manage_Patients',
            'Manage_Patients_Merge_Duplicates',
            'View_Medical_Cases',
            'Manage_Medical_Cases',
            'View_Follow_Ups',
            'View_Questions',
            'View_Health_Facilities',
            'Manage_Health_Facilities',
            'See_Sensitive_Data',
            'Export',
            'See_Logs',
            'Manage_Medical_Staff',
            'Manage_Devices',
            'View_Medical_Staff',
            'View_Devices',
            'Access_Failed_Json_Panel',
        ];

        foreach ($permissions as $permission) {
            Permission::firstOrCreate([
                'name' => $permission,
            ]);
        }

        $this->fixPermission('Administrator', [
            'Access_Admin_Corner_Panel',
            'Access_Reset_Own_Password_Panel',
            'Access_Profile_Panel',
            'See_Logs',
            'Manage_Roles',
            'Access_ADMIN_PANEL',
            'Create_User',
            'Delete_User',
            'Access_Failed_Json_Panel',
        ]);

        $this->fixPermission('Data Manager', [
            'Access_Reset_Own_Password_Panel',
            'Access_Profile_Panel',
            'Access_Medical_Cases_Panel',
            'Access_Patients_Panel',
            'Access_Export_Panel',
            'Access_Drugs_Panel',
            'Access_Diagnoses_Panel',
            'Access_Health_Facilities_Panel',
            'Access_Facilities_Panel',
            'Access_Devices_Panel',
            'Access_Medical_Staff_Panel',
            'Access_Duplicates_Panel',
            'Access_Follow_Up_Panel',
            'Reset_Own_Password',
            'Manage_Patients',
            'Manage_Patients_Merge_Duplicates',
            'Manage_Medical_Cases',
            'View_Follow_Ups',
            'View_Questions',
            'Manage_Health_Facilities',
            'Manage_Medical_Staff',
            'Manage_Devices',
            'Export',
            'See_Sensitive_Data',
            'Access_Failed_Json_Panel',
        ]);

        $this->fixPermission('Project Viewer', [
            'Access_Reset_Own_Password_Panel',
            'Access_Profile_Panel',
            'Access_Medical_Cases_Panel',
            'Access_Patients_Panel',
            'Access_Export_Panel',
            'Reset_Own_Password',
            'View_Patients',
            'View_Medical_Cases',
            'View_Follow_Ups',
            'View_Questions',
            'View_Health_Facilities',
            'View_Medical_Staff',
            'View_Devices',
            'Export',
        ]);

        $this->fixPermission('Statistician', [
            'Access_Reset_Own_Password_Panel',
            'Access_Profile_Panel',
            'Access_Medical_Cases_Panel',
            'Access_Patients_Panel',
            'Access_Export_Panel',
            'Reset_Own_Password',
            'View_Patients',
            'View_Medical_Cases',
            'Export',
            'See_Sensitive_Data',
        ]);

        $this->fixPermission('Logistician', [
            'Access_Reset_Own_Password_Panel',
            'Access_Profile_Panel',
            'Access_Health_Facilities_Panel',
            'Access_Facilities_Panel',
            'Access_Devices_Panel',
            'Access_Medical_Staff_Panel',
            'Access_Export_Panel',
            'Reset_Own_Password',
            'Manage_Health_Facilities',
            'Manage_Devices',
            'Manage_Medical_Staff',
            'Export',
            'See_Sensitive_Data',
        ]);
    }

    private function addPermissionToRole($permission_name, $role)
    {
        $permission = DB::table('permissions')->where('name', '=', $permission_name)->first();
        $role_has_permission = DB::table('role_has_permissions')->where('permission_id', $permission->id)->where('role_id', $role->id)->first();

        if ($role_has_permission) {
            return;
        }

        DB::table('role_has_permissions')->insert([
            'permission_id' => $permission->id,
            'role_id' => $role->id,
        ]);

        Log::info('Fixed role ' . $role->name . '.');
        $this->info('Fixed role ' . $role->name . '.');
    }

    private function fixPermission($name, $permission_names)
    {
        $role = DB::table('roles')->where('name', '=', $name)->first();

        if (!$role) {
            return;
        }

        foreach ($permission_names as $permission_name) {
            $this->addPermissionToRole($permission_name, $role);
        }
    }
}
