<?php

namespace App\Console\Commands;

use App\MedicalCase;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ReprocessDateOfBirthConsultation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'consultation_dob:reprocess {dry-run=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This commmand will get every JSONs and reprocess the date of birth';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('dry-run') == 1) {
            $this->info('Dry Run');
        }

        $patients_to_update = 0;
        $patients_to_update_manually = 0;
        $json_success_files = Storage::files('json_success');
        $chunked_jsons = array_chunk($json_success_files, 100);
        foreach ($chunked_jsons as $key => $chunked_json) {
            $chunked_patients_to_update = 0;
            $chunked_patients_to_update_manually = 0;
            $chunked_patients_to_update_middle_manually = 0;
            foreach ($chunked_json as $json) {
                $filename = array_slice(explode('/', $json), -1)[0];
                $caseData = json_decode(Storage::get("json_success/$filename"), true);
                if ($caseData === null) {
                    $this->info('empty json');
                    continue;
                }
                $patient_json = $caseData['patient'];
                if (empty($patient_json)) {
                    $this->info('missing patient data');
                    continue;
                }

                $medical_case_id = $caseData['id'];
                $medical_case = MedicalCase::where('local_medical_case_id', $medical_case_id)->first();
                $c_json_dob = CarbonImmutable::parse(intdiv($patient_json['birth_date'], 1000));
                $c_json_dob_tz = CarbonImmutable::parse(intdiv($patient_json['birth_date'], 1000))->setTimezone(config('app.timezone'));
                $startDate = $c_json_dob->endOfDay();
                $endDate = $startDate->subHours(6);
                $startDate = $c_json_dob->addHours(6);

                if (empty($medical_case)) {
                    $this->info('missing medical case');
                    continue;
                }

                if ($c_json_dob_tz->isMidnight()) {
                    if ($this->argument('dry-run') == 0) {
                        $medical_case->update(['birthdate' => $c_json_dob_tz->format('Y-m-d')]);
                    }

                    $patients_to_update++;
                    $chunked_patients_to_update++;
                } elseif ($c_json_dob_tz->between($endDate, $startDate, true)) {

                    if ($this->argument('dry-run') == 0) {
                        $medical_case->update(['birthdate' => $c_json_dob_tz->format('Y-m-d')]);
                    }

                    $chunked_patients_to_update_middle_manually++;
                } elseif ($c_json_dob->isSameDay($c_json_dob_tz)) {
                    if ($this->argument('dry-run') == 0) {
                        $medical_case->update(['birthdate' => $c_json_dob_tz->format('Y-m-d')]);
                    }
                    $chunked_patients_to_update_middle_manually++;
                } elseif ($c_json_dob_tz->isNextDay($c_json_dob)) {
                    if ($this->argument('dry-run') == 0) {
                        $medical_case->update(['birthdate' => $c_json_dob_tz->format('Y-m-d')]);
                    }
                    $chunked_patients_to_update_middle_manually++;
                } else {
                    // bad shift or manually modified dob
                    $patients_to_update_manually++;
                    $chunked_patients_to_update_manually++;
                }
            }

            $right = 100 - $chunked_patients_to_update_manually - $chunked_patients_to_update_middle_manually - $chunked_patients_to_update;
            $corrected = $chunked_patients_to_update + $chunked_patients_to_update_middle_manually;
            if ($this->argument('dry-run') == 0) {
                $this->info("Chunk " . ($key + 1) . "/" . count($chunked_jsons));
                $this->info("$corrected date of birth fixed");
            } else {
                $this->info("Chunk " . ($key + 1) . "/" . count($chunked_jsons));
                $this->info("$chunked_patients_to_update% of dob to automate");
                $this->info("$chunked_patients_to_update_middle_manually% of dob to check but should be automated");
                $this->info("$chunked_patients_to_update_manually% of dob to manually fix");
                $this->info("$right% right dob");
            }
        }
    }
}
