<?php

namespace App\Console\Commands;

use App\MedicalCase;
use App\Node;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class PopulateCorrectedDobColum extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dob:populate_corrected {dry-run=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This commmand will get every JSONs and populate the new corrected dob column';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('dry-run') == 1) {
            $this->info('Dry Run');
        }

        $json_success_files = Storage::files('json_success');
        $chunked_jsons = array_chunk($json_success_files, 100);
        foreach ($chunked_jsons as $key => $chunked_json) {
            foreach ($chunked_json as $json) {
                $filename = array_slice(explode('/', $json), -1)[0];
                $caseData = json_decode(Storage::get("json_success/$filename"), true);
                if ($caseData === null) {
                    $this->info('empty json');
                    continue;
                }
                $patient_json = $caseData['patient'];
                if (empty($patient_json)) {
                    $this->info('missing patient data');
                    continue;
                }

                $medical_case_id = $caseData['id'];
                $medical_case = MedicalCase::where('local_medical_case_id', $medical_case_id)->first();
                if (empty($medical_case)) {
                    $this->info('missing medical case');
                    continue;
                }
                if (!isset($caseData['createdAt'])) {
                    $this->info('missing createdAt in json');
                    continue;
                }
                $c_json_dob = CarbonImmutable::parse(intdiv($patient_json['birth_date'], 1000))->setTimezone(config('app.timezone'));
                $age_in_day_nodes_id = Node::where('label', 'like', 'Age in days')->pluck('id')->toArray();
                $created_at = Carbon::createFromTimestampMs($caseData['createdAt']);
                $age_in_days = $medical_case->medical_case_answers->whereIn('node_id', $age_in_day_nodes_id)->first();

                $patient = $medical_case->patient;
                if (!$patient) {
                    $this->info('missing patient in database');
                    continue;
                }

                $corrected_patient_birthdate = $c_json_dob;

                if ($age_in_days && $age_in_days->value !== '' && !empty($medical_case->birthdate)) {
                    if ($c_json_dob->diffInDays($medical_case->consultation_date) != $age_in_days->value && $c_json_dob->hour >= 0 && $c_json_dob->hour < 6) {
                        $corrected_patient_birthdate = $corrected_patient_birthdate->subDay();
                    }
                } else {
                    if ($c_json_dob->hour >= 0 && $c_json_dob->hour < 5) {
                        $corrected_patient_birthdate = $corrected_patient_birthdate->subDay();
                    }
                }

                if ($age_in_days) {
                    if ($age_in_days->value === '') {
                        if ($this->argument('dry-run') == 0) {
                            $medical_case->update(['blankcase' => true]);
                        }
                        if (empty($medical_case->birthdate)) {
                            $corrected_age_in_days = $corrected_patient_birthdate->diffInDays($created_at);
                        } else {
                            $corrected_age_in_days = $created_at->diffInDays($medical_case->birthdate);
                        }
                    } else {
                        $corrected_age_in_days = $age_in_days->value;
                    }
                } else {
                    $corrected_age_in_days = $corrected_patient_birthdate->diffInDays($created_at);
                    if ($this->argument('dry-run') == 0) {
                        $medical_case->update(['blankcase' => true]);
                    }
                    if ($this->argument('dry-run') == 1) {
                        $this->info($filename);
                    }
                }

                if ($this->argument('dry-run') == 0) {
                    $medical_case->update(['corrected_bd5ageindays' => $corrected_age_in_days]);
                    $patient->update(['corrected_birthdate' => $corrected_patient_birthdate->format('Y-m-d')]);
                }
                if ($this->argument('dry-run') == 1) {
                    $this->info("patient_birthdate" . $patient->birthdate);
                    $this->info("medical_case_birthdate" . $medical_case->birthdate);
                    $this->info("corrected_patient_birthdate $corrected_patient_birthdate");
                    if ($age_in_days && $age_in_days->value !== '') {
                        $this->info("age_in_days" . $age_in_days->value);
                    }
                    $this->info("corrected_age_in_days $corrected_age_in_days");
                }
            }
            if ($this->argument('dry-run') == 0) {
                $this->info("Chunk " . ($key + 1) . "/" . count($chunked_jsons));
            } else {
                $this->info("Chunk " . ($key + 1) . "/" . count($chunked_jsons));
            }
        }
    }
}
