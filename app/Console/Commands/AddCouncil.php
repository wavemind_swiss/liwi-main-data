<?php

namespace App\Console\Commands;

use App\HealthFacility;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AddCouncil extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hf:seed_coucil';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (config('app.study_id') === 'Dynamic Tanzania') {
            // mapping from a health facility to its council
            $group_id_to_council = [
                235 => 'Mbeya CC',
                241 => 'Mbeya CC',
                146 => 'Mbeya CC',
                150 => 'Mbeya CC',
                244 => 'Mbeya DC',
                248 => 'Mbeya DC',
                152 => 'Mbeya DC',
                71 => 'Ifakara TC',
                261 => 'Ifakara TC',
                142 => 'Ifakara TC',
                239 => 'Ulanga DC',
                252 => 'Ulanga DC',
                72 => 'Ulanga DC',
                242 => 'Mbeya CC',
                250 => 'Mbeya CC',
                245 => 'Mbeya DC',
                73 => 'Mbeya DC',
                249 => 'Mbeya DC',
                246 => 'Mbeya DC',
                237 => 'Mlimba DC',
                255 => 'Mlimba DC',
                143 => 'Ulanga DC',
                253 => 'Ifakara TC',
                240 => 'Ulanga DC',
                254 => 'Ulanga DC',
                256 => 'Ulanga DC',
                148 => 'Mbeya CC',
                151 => 'Mbeya CC',
                247 => 'Mbeya DC',
                264 => 'Ifakara TC',
                266 => 'Mlimba DC',
                262 => 'Ifakara TC',
                238 => 'Mlimba DC',
                265 => 'Mlimba DC',
                236 => 'Mlimba DC',
                257 => 'Mlimba DC',
                69 => 'Mbeya CC',
                153 => 'Mbeya DC',
                243 => 'Ifakara TC',
                258 => 'Mlimba DC',
                259 => 'Mlimba DC',
                263 => 'Ulanga DC',
            ];

            foreach ($group_id_to_council as $group_id => $council) {
                // fetch health facility line from its group_id
                $health_facility = HealthFacility::where('group_id', $group_id)->first();

                if (!empty($health_facility)) {
                    // match with its council
                    $council = $group_id_to_council[$group_id];

                    // update council
                    $health_facility->update([
                        'council' => $council,
                    ]);

                    $this->info("Updated health facility $health_facility->group_id with council \"$council\"");
                    Log::info("Updated health facility $health_facility->group_id with council \"$council\"");
                }
            }
        }
    }
}
