<?php

namespace App\Console\Commands;

use App\Diagnosis;
use App\Drug;
use App\Node;
use App\Services\Http;
use App\Version;
use Illuminate\Console\Command;

class CompareCreatorLabels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'labels:compare';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will create files to compare labels between creator and data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $headers = [
            'Creator Diagnosis Label',
            'medAL-Data Diagnosis Label',
            'Creator Drugs Label',
            'medAL-Data Drugs Label',
            'Creator Nodes Label',
            'medAL-Data Nodes Label',
        ];
        $diagnosis_file = fopen(storage_path('app/diagnosis.csv'), 'a+');
        $drugs_file = fopen(storage_path('app/drugs.csv'), 'a+');
        $nodes_file = fopen(storage_path('app/nodes.csv'), 'a+');

        fwrite($diagnosis_file, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));
        fwrite($drugs_file, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));
        fwrite($nodes_file, $bom = (chr(0xEF) . chr(0xBB) . chr(0xBF)));
        fputcsv($diagnosis_file, [$headers[0], $headers[1]]);
        fputcsv($drugs_file, [$headers[2], $headers[3]]);
        fputcsv($nodes_file, [$headers[4], $headers[5]]);

        Version::all()->each(function ($version) use ($diagnosis_file, $drugs_file, $nodes_file) {
            $data = Http::get(config('medal.urls.creator_algorithm_url') . $version->medal_c_id);
            $data = json_decode($data['content'], true);
            $versionData = $data['medal_r_json'];
            $french_algo = $version->study === 'TIMCI Senegal';
            $language = $french_algo ? 'fr' : 'en';

            // Diagnosis
            foreach ($versionData['final_diagnoses'] as $diag_medal_c_id => $creator_diag) {
                if (is_array($creator_diag['label'])) {
                    $creator_diag_label = trim($creator_diag['label'][$language]);
                } else {
                    $creator_diag_label = trim($creator_diag['label']);
                }
                $data_diag = Diagnosis::where('medal_c_id', $diag_medal_c_id)->first();
                if ($data_diag) {
                    $data_label = trim($data_diag->label);
                    if (strtolower($creator_diag_label) !== strtolower($data_label)) {
                        fputcsv($diagnosis_file, [$creator_diag_label, $data_label]);
                    }
                }

                // Drugs
                foreach ($creator_diag['drugs'] as $drug_medal_c_id => $creator_drug) {
                    $drugData = $versionData['health_cares'][$drug_medal_c_id];
                    if (is_array($creator_diag['label'])) {
                        $creator_drug_label = trim($drugData['label'][$language]);
                    } else {
                        $creator_drug_label = trim($drugData['label']);
                    }
                    $data_drug = Drug::where('medal_c_id', $drug_medal_c_id)->first();
                    if ($data_drug) {
                        $data_drug_label = trim($data_drug->label);
                        if (strtolower($creator_drug_label) !== strtolower($data_drug_label)) {
                            fputcsv($drugs_file, [$creator_drug_label, $data_drug_label]);
                        }
                    }
                }
            }

            // Nodes
            foreach ($versionData['nodes'] as $node_medal_c_id => $creator_node) {
                if (is_array($creator_diag['label'])) {
                    $creator_node_label = trim($creator_node['label'][$language]);
                } else {
                    $creator_node_label = trim($creator_node['label']);
                }
                $data_node = Node::where('medal_c_id', $node_medal_c_id)->first();
                if ($data_node) {
                    $data_node_label = trim($data_node->label);
                    if (strtolower($creator_node_label) !== strtolower($data_node_label)) {
                        fputcsv($nodes_file, [$creator_node_label, $data_node_label]);
                    }
                }
            }
        });
    }
}
