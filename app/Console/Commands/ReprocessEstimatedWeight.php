<?php

namespace App\Console\Commands;

use App\MedicalCase;
use App\MedicalCaseAnswer;
use App\Node;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ReprocessEstimatedWeight extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weight:reprocess {dry-run=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This commmand will get every JSONs and reprocess the estimated/measured weight';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('dry-run') == 1) {
            $this->info('Dry Run');
        }

        $nodes_to_update = 0;
        $json_success_files = Storage::files('json_success');

        foreach ($json_success_files as $json) {
            $filename = array_slice(explode('/', $json), -1)[0];
            $caseData = json_decode(Storage::get("json_success/$filename"), true);
            if ($caseData === null) {
                continue;
            }
            $nodes = $caseData['nodes'];
            if (empty($nodes)) {
                continue;
            }

            $medical_case_id = $caseData['id'];
            $medical_case = MedicalCase::where('local_medical_case_id', $medical_case_id)->first();
            if (empty($medical_case)) {
                continue;
            }

            foreach ($nodes as $node) {
                if (array_key_exists('estimableValue', $node)) {
                    $value_to_add = $node['estimableValue'] === 'estimated' ? '_estimated' : '_measured';
                    if ($this->argument('dry-run') == 0) {
                        $db_node = Node::where('medal_c_id', $node['id'])->first();
                        $medical_case_answer = MedicalCaseAnswer::where('medical_case_id', $medical_case->id)->where('node_id', $db_node->id)->first();
                        if (!empty($medical_case_answer->value)) {
                            //For RW only
                            if (strpos($medical_case_answer->value, ' estimated') !== false || strpos($medical_case_answer->value, ' measured') !== false) {
                                $medical_case_answer->update([
                                    'value' => str_replace(' ', '_', $medical_case_answer->value),
                                ]);
                            } elseif ($medical_case_answer->value === '_estimated' || $medical_case_answer->value === '_measured') {
                                $medical_case_answer->update([
                                    'value' => '',
                                ]);
                            } else {
                                // Be sure that this fix has not been applied yet
                                if (strpos($medical_case_answer->value, 'estimated') === false && strpos($medical_case_answer->value, 'measured') === false) {
                                    $medical_case_answer->update([
                                        'value' => $medical_case_answer->value . $value_to_add,
                                    ]);
                                }
                            }
                        }
                    }
                    $nodes_to_update++;
                }
            }
        }

        if ($this->argument('dry-run') == 0) {
            $this->info($nodes_to_update . ' nodes updated');
        } else {
            $this->info($nodes_to_update . ' nodes would have been updated');
        }
    }
}
