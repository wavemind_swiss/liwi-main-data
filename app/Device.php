<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Device.
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property string $type
 * @property string|null $mac_address
 * @property string|null $model
 * @property string|null $brand
 * @property string|null $os
 * @property string|null $os_version
 * @property string $redirect
 * @property int $status
 * @property int $user_id
 * @property int|null $health_facility_id
 * @property int|null $oauth_client_id
 * @property string|null $oauth_client_secret
 * @property string|null $last_seen
 * @property-read \App\HealthFacility|null $health_facility
 * @method static \Illuminate\Database\Eloquent\Builder|Device newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Device newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Device query()
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereHealthFacilityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereLastSeen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereMacAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereOauthClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereOauthClientSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereOs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereOsVersion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereRedirect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereUserId($value)
 * @mixin \Eloquent
 */
class Device extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'type',
        'mac_address',
        'model',
        'brand',
        'os',
        'os_version',
        'status',
        'redirect',
        'oauth_client_id',
    ];

    protected $guarded = [];

    public function health_facility()
    {
        return $this->belongsTo('App\HealthFacility');
    }

    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    public function auto_diagnostics()
    {
        return $this->hasMany(AutoDiagnostic::class);
    }
}
