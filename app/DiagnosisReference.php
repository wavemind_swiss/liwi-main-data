<?php

namespace App;

use App\Enums\ClinicianChoice;
use Illuminate\Database\Eloquent\Model;

/**
 * App\DiagnosisReference.
 *
 * @property int $id
 * @property int $diagnosis_id
 * @property int $medical_case_id
 * @property \App\Enums\ClinicianChoice|null $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CustomDrug[] $custom_drugs
 * @property-read int|null $custom_drugs_count
 * @property-read \App\Diagnosis|null $diagnoses
 * @property-read \App\Diagnosis $diagnosis
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DrugReference[] $drug_references
 * @property-read int|null $drug_references_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ManagementReference[] $management_references
 * @property-read int|null $management_references_count
 * @property-read \App\MedicalCase $medical_case
 * @method static \Illuminate\Database\Eloquent\Builder|DiagnosisReference newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DiagnosisReference newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DiagnosisReference query()
 * @method static \Illuminate\Database\Eloquent\Builder|DiagnosisReference whereDiagnosisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiagnosisReference whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiagnosisReference whereMedicalCaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DiagnosisReference whereStatus($value)
 * @mixin \Eloquent
 */
class DiagnosisReference extends Model
{
    protected $table = 'diagnosis_references';

    public $timestamps = false;

    protected $guarded = [];

    /**
     * @param $value
     * @return ClinicianChoice|null
     */
    public function getStatusAttribute($value): ?ClinicianChoice
    {
        return ClinicianChoice::coerce($value);
    }

    /**
     * get or store diagnosis.
     * @params $id
     * @return $diagnoses
     */
    public static function getDiagnoses($id)
    {
        $references = self::where('medical_case_id', $id)->get();
        $diagnoses = [];
        foreach ($references as $reference) {
            $diagnosis = Diagnosis::find($reference->diagnosis_id);
            $med_diag = (object) [
                'agreed' => $reference->status->is(ClinicianChoice::AGREED()),
                'proposed' => !$reference->status->is(ClinicianChoice::ADDITIONAL()),
                'diagnosis_medal_c_id' => $diagnosis->medal_c_id,
                'label' => $diagnosis->label,
                'version' => Version::find($diagnosis->versiod_id),
            ];
            array_push($diagnoses, $med_diag);
        }

        return $diagnoses;
    }

    /**
     * Make diagnosis relation.
     * @return one to many drub references retionship
     */
    public function drug_references()
    {
        return $this->hasMany(DrugReference::class, 'diagnosis_id', 'id');
    }

    public function custom_drugs()
    {
        return $this->hasMany(CustomDrug::class, 'diagnosis_id', 'id');
    }

    /**
     * Make diagnoses relation.
     * @return one to one drub Diagnosis
     */
    public function diagnoses()
    {
        return $this->hasOne(Diagnosis::class, 'id', 'diagnosis_id');
    }

    public function diagnosis()
    {
        return $this->belongsTo(Diagnosis::class, 'diagnosis_id');
    }

    /**
     * Make diagnosis relation.
     * @return one to many management references retionship
     */
    public function management_references()
    {
        return $this->hasMany(ManagementReference::class, 'diagnosis_id', 'id');
    }

    public function medical_case()
    {
        return $this->belongsTo(MedicalCase::class, 'medical_case_id');
    }
}
