<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ManagementReference.
 *
 * @property int $id
 * @property int $diagnosis_id
 * @property int $management_id
 * @property-read \App\Management|null $managements
 * @method static \Illuminate\Database\Eloquent\Builder|ManagementReference newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ManagementReference newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ManagementReference query()
 * @method static \Illuminate\Database\Eloquent\Builder|ManagementReference whereDiagnosisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ManagementReference whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ManagementReference whereManagementId($value)
 * @mixin \Eloquent
 */
class ManagementReference extends Model
{
    public $table = 'management_references';

    public $timestamps = false;

    protected $guarded = [];

    /**
     * Make diagnoses relation.
     * @return one to one drub Diagnosis
     */
    public function managements()
    {
        return $this->hasOne('App\Management', 'id', 'management_id');
    }
}
