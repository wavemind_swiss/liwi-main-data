<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Node.
 *
 * @property int $id
 * @property int $medal_c_id
 * @property string $reference
 * @property string $label
 * @property string $type
 * @property string $category
 * @property string $priority
 * @property string|null $stage
 * @property string|null $description
 * @property string $formula
 * @property int $answer_type_id
 * @property int $algorithm_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property bool|null $is_identifiable
 * @property string|null $display_format
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Answer[] $answers
 * @property-read int|null $answers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @method static \Illuminate\Database\Eloquent\Builder|Node newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Node newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Node query()
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereAlgorithmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereAnswerTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereDisplayFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereFormula($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereIsIdentifiable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereMedalCId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereStage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Node whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Node extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $guarded = [];

    /**
     * Create a relation with answers.
     * @return relation
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }
}
