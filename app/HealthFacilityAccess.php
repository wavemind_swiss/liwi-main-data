<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\HealthFacilityAccess.
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property bool $access
 * @property string|null $end_date
 * @property int $creator_version_id
 * @property string $version_name
 * @property int $medal_r_json_version
 * @property bool $is_arm_control
 * @property int $health_facility_id
 * @property int|null $medal_c_algorithm_id
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess query()
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess whereAccess($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess whereCreatorVersionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess whereHealthFacilityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess whereIsArmControl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess whereMedalCAlgorithmId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess whereMedalRJsonVersion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HealthFacilityAccess whereVersionName($value)
 * @mixin \Eloquent
 */
class HealthFacilityAccess extends Model
{
    protected $attributes = [
        'access' => true,
    ];

    protected $fillable = [
        'medal_r_json_version',
        'creator_version_id',
        'medal_c_algorithm_id',
    ];
}
