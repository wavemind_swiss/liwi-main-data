<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Drug.
 *
 * @property int $id
 * @property int $medal_c_id
 * @property string $type
 * @property string $label
 * @property string|null $description
 * @property int|null $diagnosis_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property bool|null $is_anti_malarial
 * @property bool|null $is_antibiotic
 * @property string|null $duration
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Formulation[] $formulations
 * @property-read int|null $formulations_count
 * @method static \Illuminate\Database\Eloquent\Builder|Drug newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Drug newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Drug query()
 * @method static \Illuminate\Database\Eloquent\Builder|Drug whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drug whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drug whereDiagnosisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drug whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drug whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drug whereIsAntiMalarial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drug whereIsAntibiotic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drug whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drug whereMedalCId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drug whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Drug whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Drug extends Model
{
    protected $guarded = [];

    /**
     * Create a relation with answers.
     * @return relation
     */
    public function formulations()
    {
        return $this->hasMany('App\Formulation', 'drug_id', 'id');
    }
}
