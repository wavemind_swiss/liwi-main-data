<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use IU\PHPCap\PhpCapException;
use IU\PHPCap\RedCapProject;
use OwenIt\Auditing\Contracts\Auditable;

// use Illuminate\Support\Collection;

/**
 * Class MedicalCase.
 *
 * @property int $id
 * @property ExcludedDiagnosesList|null $excluded_diagnoses_list
 * @mixin Builder
 * @property int $version_id
 * @property int $patient_id
 * @property string|null $local_medical_case_id
 * @property bool|null $consent
 * @property bool|null $isEligible
 * @property int|null $group_id
 * @property bool $redcap
 * @property string|null $consultation_date
 * @property string|null $closedAt
 * @property bool|null $force_close
 * @property bool|null $mc_redcap_flag
 * @property bool $duplicate
 * @property string|null $app_version
 * @property string|null $git_version
 * @property-read \Illuminate\Database\Eloquent\Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read int|null $audits_count
 * @property-read \Illuminate\Database\Eloquent\Collection|CustomDiagnosis[] $custom_diagnoses
 * @property-read int|null $custom_diagnoses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Diagnosis[] $diagnoses
 * @property-read int|null $diagnoses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|DiagnosisReference[] $diagnoses_references
 * @property-read int|null $diagnoses_references_count
 * @property-read HealthFacility|null $facility
 * @property-read mixed $facility_name
 * @property-read \Illuminate\Database\Eloquent\Collection|MedicalCaseAnswer[] $medical_case_answers
 * @property-read int|null $medical_case_answers_count
 * @property-read Patient $patient
 * @property-read Version $version
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase query()
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereAppVersion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereClosedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereConsent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereConsultationDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereDuplicate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereForceClose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereGitVersion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereIsEligible($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereLocalMedicalCaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereMcRedcapFlag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereRedcap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalCase whereVersionId($value)
 */
class MedicalCase extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $timestamps = false;

    protected $guarded = [];

    public function isRedcapFlagged(): bool
    {
        // TODO : return value of redcap flag in the database
        return false;
    }

    /**
     * Scope a query to only include non dropped mcs.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeNotDropped($query)
    {
        return $query->where('is_drop', false);
    }

    /**
     * Make follow up.
     * @param MedicalCase $medical_case
     * @return FollowUp
     */
    public static function makeFollowUp($medical_case)
    {
        $follow_up = new FollowUp($medical_case);

        return $follow_up;
    }

    public function listUnfollowed()
    {
        $caseFollowUpCollection = new Collection();
        foreach (self::where('redcap', false)->get() as $medicalcase) {
            $followUp = self::makeFollowUp($medicalcase);
            // find the patient related to this followup
            $caseFollowUpCollection->add($followUp);
        }

        return $caseFollowUpCollection;
    }

    public function getAnsweredQuestions($case_id)
    {
        $case = $this::find($case_id);
        $answers_answered = [];
        $case->medical_case_answers->each(function ($case_answer) use (&$answers_answered) {
            if ($case_answer->answer || $case_answer->value) {
                // array_push($answers_answered,$case_answer);
                $answers_answered[$case_answer->node_id] = $case_answer;
            }
        });

        return $answers_answered;
    }

    public function removeFollowUp($id)
    {
        $case = $this->find($id);
        $project = new RedCapProject(Config::get('redcap.identifiers.api_url_followup'), Config::get('redcap.identifiers.api_token_followup'));
        $case_id = (array) $case->local_medical_case_id;
        try {
            $project->deleteRecords($case_id);
            $message = "Case Id '{$case_id[0]}' Has been removed from Redcap Folloup";
        } catch (PhpCapException $e) {
            // return  new RedCapApiServiceException("Failed to remove Record : '{$case->local_medical_case_id}'", 0, $e);
            $message = "Case Id '{$case_id[0]}' Has Not been removed from Redcap Folloup, May be its not in redcap Or Network Issues";
        }
        $case->duplicate = 1;
        $case->save();

        return $message;
    }

    /**
     * making a relationship to patient.
     * @return one to one patient relationship
     */
    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    /**
     * making a relationship to patient.
     * @return one to one patient relationship
     */
    public function version()
    {
        return $this->belongsTo(Version::class);
    }

    public function facility()
    {
        return $this->belongsTo(HealthFacility::class, 'group_id', 'group_id');
    }

    /**
     * Make medical case answers relation.
     * @return one to many medical cases retionship
     */
    public function medical_case_answers()
    {
        return $this->hasMany(MedicalCaseAnswer::class);
    }

    /**
     * Make diagnosis relation.
     * @return one to many medical cases retionship
     */
    public function diagnoses_references()
    {
        return $this->hasMany(DiagnosisReference::class);
    }

    /**
     * @return BelongsToMany
     */
    public function diagnoses(): BelongsToMany
    {
        return $this->belongsToMany(Diagnosis::class)
            ->withPivot('status')
            ->withTimestamps();
    }

    /**
     * Make diagnosis relation.
     * @return one to many medical cases retionship
     */
    public function custom_diagnoses()
    {
        return $this->hasMany(CustomDiagnosis::class);
    }

    /**
     * Make activity relation.
     * @return one to many medical cases retionship
     */
    public function activities()
    {
        return $this->hasMany(Activity::class, 'medical_case_id', 'id');
    }

    public function excluded_diagnoses_list(): HasOne
    {
        return $this->hasOne(ExcludedDiagnosesList::class);
    }
}
