<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Management.
 *
 * @property int $id
 * @property int $medal_c_id
 * @property string $type
 * @property string $label
 * @property string|null $description
 * @property int|null $diagnosis_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Management newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Management newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Management query()
 * @method static \Illuminate\Database\Eloquent\Builder|Management whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Management whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Management whereDiagnosisId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Management whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Management whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Management whereMedalCId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Management whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Management whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Management extends Model
{
    protected $table = 'managements';

    protected $guarded = [];
}
