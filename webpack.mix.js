const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js/app.js")
    .scripts(
        [
            "resources/js/jquery.js",
            "resources/js/custom.js",
            "resources/js/jquery.dataTables.min.js",
            "resources/js/dataTables.buttons.min.js",
            "resources/js/dataTables.select.min.js",
            "resources/js/buttons.html5.js",
            "node_modules/sweetalert2/dist/sweetalert2.all.min.js",
        ],
        "public/js/vendor.js"
    )

    .js("resources/js/chart.js", "public/js/chart.js")
    .js("resources/js/dpCase.js", "public/js/dpCase.js")
    .js("resources/js/highlight.js", "public/js/highlight.js")
    .js("resources/js/followup.js", "public/js/followup.js")
    .js("node_modules/popper.js/dist/popper.js", "public/js")

    .sass("resources/sass/app.scss", "public/css/custom.css")
    .styles(
        "resources/sass/jquery.dataTables.min.css",
        "public/css/jquery.dataTables.min.css"
    )

    .options({
        clearConsole: false,
    })
    .version();

if (!mix.inProduction()) {
    mix.webpackConfig({ devtool: "source-map" });
    mix.sourceMaps(false, "source-map");
}
