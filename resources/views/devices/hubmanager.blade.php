@extends('adminlte::page')

<link href="{{ asset('css/datatable.css') }}" rel="stylesheet">

@include('medicalCases.compareModal')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <div class="p-2 bd-highlight">
                                <span>
                                    <h3>Hub Manger</h3>
                                </span>
                            </div>
                        </div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                    @if (count($devices) > 0)
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">SN</th>
                                                    <th scope="col">Hub name</th>
                                                    <th scope="col">Up-to-date</th>
                                                    <th scope="col">Diagnostic recieved</th>
                                                    <th scope="col">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($devices as $device)
                                                    <tr>
                                                        <th scope="row">{{ $device->id }}</th>
                                                        <td>{{ $device->name }}</td>
                                                        @if ($device->is_uptodate)
                                                            <td><i class="fa fa-check" style="color:green !important" aria-hidden="true"></i></td>
                                                        @else
                                                            @if ($device->offset === null)
                                                                <td>No information</td>
                                                            @elseif ($device->offset >= 0)
                                                                <td><i class="fa fa-ban" style="color:red !important" aria-hidden="true"></i> {{$device->offset}} commit{{$device->offset > 1 ? 's' : ''}} behind</td>
                                                            @else
                                                                <td><i class="fa fa-ban" style="color:red !important" aria-hidden="true"></i> More than 30 commits behind</td>
                                                            @endif
                                                        @endif
                                                        @if ($device->auto_diagnostics_count > 0)
                                                            <td><i class="fa fa-check" style="color:green !important" aria-hidden="true"></i></td>
                                                        @else
                                                            <td><i class="fa fa-ban" style="color:red !important" aria-hidden="true"></i></td>
                                                        @endif
                                                        <td>
                                                            @if ($device->offset !== null)
                                                                <a href={{route('diagnostic.show', [$device->id])}} style="cursor: pointer;" class='btn btn-sm btn-outline-dark'>Show</a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <span>
                                            <h4>No hub is not up-to-date</h4>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $(".table").DataTable({});
        });
    </script>
@stop
