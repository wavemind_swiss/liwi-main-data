@extends('adminlte::page')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="p-2 d-flex justify-content-between">
                        <div class="p-2 bd-highlight">
                            <span>
                                <h4 lass="card-title">Recent Errors of {{$device->name}}</h4>
                            </span>
                        </div>
                        <div class="ms-auto pt-2 pr-5 bd-highlight">
                            <a onclick="history.back()" class="btn btn-outline-dark" style="cursor: pointer;"> Back</a>
                        </div>
                    </div>
                    @include('partials.recentDiagnosticErrors')
                </div>
            </div>
        </div>
    </div>
</div>
@stop
