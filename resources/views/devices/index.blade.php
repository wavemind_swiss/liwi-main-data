@extends('adminlte::page')

 <!-- The contents of the layout/app.bade.php are included so that the view components work correctly -->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>
      Devices
    </title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
</head>


@section('content')

<body>
    <div id="app" class="colorGrey">
    <devices :devices_data="{{$devices}}"
              devices_route="{{route("devices.index")}}"
              health_facilities_route="{{route("health-facilities.index")}}"></devices>
    </div>
</body>

@stop
