@extends('adminlte::page')

@section('content')
  <div class="container-fluid">
    @if ($errors->any())
      <div class="col-md-12">
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      </div>
    @endif
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <div class="p-2 d-flex justify-content-between">
              <div class="p-2 bd-highlight">
                <span>
                  {{-- <h2>Hub Diagnostics</h2> --}}
                  <h3>Current Hub Status</h3>
                  <h5 class="card-title pt-2">Device {{ $device->name }}</h5>
                  @if (isset($json_diagnostic['last_patient']) && $json_diagnostic['last_patient']['status'])
                    @if ($json_diagnostic['last_patient']['data']['date'])
                      <p class="pt-2 card-text">Last Patient Created:
                        {{ date('Y-m-d H:i:s', strtotime($json_diagnostic['last_patient']['data']['date'])) }}</p>
                    @else
                      <p class="pt-2 card-text">No patient</p>
                    @endif
                  @endif
                </span>
              </div>
              <div class="ms-auto pt-2 pr-5 bd-highlight">
                <a onclick="history.back()" class="btn btn-outline-dark" style="cursor: pointer;"> Back</a>
              </div>
            </div>
            @if ($json_diagnostic)
              @include('partials.diagnosticGit')
          </div>
        </div>
        <div class="card">
          <div class="card-body">
            @include('partials.diagnosticInfo')
          </div>
        </div>
      @else
        @include('partials.noInfo')
        @endif
      </div>
    </div>
  </div>
@stop
