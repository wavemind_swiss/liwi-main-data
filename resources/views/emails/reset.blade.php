@extends('layouts.app')
{!! NoCaptcha::renderJs() !!}
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-body">
                <div class="card-header">
                  <span>Please set the new password</span>
                </div>
                <div class="card-body">
                    {{ Form::open(['route' => ['register.makePassword']]) }}
                      @if ($errors->any())
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                      @endif
                      <div class="form-group row">
                        {{Form::label('password', 'Password', array('class' => 'col-md-4 col-form-label text-md-right'))}}
                        <div class="col-md-6">
                          {{Form::password('password', null, array('autofocus'=>'autofocus','class'=>'form-control','required'=>'required'))}}

                        </div>
                      </div>
                      <div class="form-group row">
                        {{Form::label('Confirm Password', 'Confirm Password', array('class' => 'col-md-4 col-form-label text-md-right'))}}
                        <div class="col-md-6">
                          {{Form::password('confirm_password', null, array('autofocus'=>'autofocus','class'=>'form-control','required'=>'required'))}}

                        </div>
                      </div>
                      <div class="form-group row mb-0 mt-2 mb-2">
                        <div class="col-md-6 offset-md-4">
                            {!! NoCaptcha::display() !!}
                        </div>
                      </div>
                      <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                          {{Form::submit('Reset', array('class' => 'btn btn-outline-dark'))}}
                        </div>
                      </div>
                    {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
