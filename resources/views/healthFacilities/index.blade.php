@extends('adminlte::page')

 <!-- The contents of the layout/app.bade.php are included so that the view components work correctly -->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>
      Health Facilities
    </title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>


@section('content')

<body>
    <div id="app" class="colorGrey">
        <health-facilities :health_facilities="{{$healthFacilities}}"
                           health_facilities_route="{{route("health-facilities.index")}}"
                           devices_route="{{route("devices.index")}}"></health-facilities>
    </div>
</body>

@stop
