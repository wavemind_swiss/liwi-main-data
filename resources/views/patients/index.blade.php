@extends('adminlte::page')

<link href="{{ asset('css/datatable.css') }}" rel="stylesheet">
@include('medicalCases.compareModal')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header d-flex">
            <span>
              <h3>Patients</h3>
            </span>
            <div class="ml-auto p-2">
            </div>
          </div>
          <div class="card-body">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif
            <div class="row">
              <div class="col-md-12">
                <table class="table">
                  <thead>
                    <tr>
                      <th></th>
                      <th scope="col">SN</th>
                      <th scope="col">Patient Uid</th>
                      <th scope="col">First Name</th>
                      <th scope="col">Middle Name</th>
                      <th scope="col">Last Name</th>
                      <th scope="col">Birthdate</th>
                      <th scope="col">Status</th>
                      <th scope="col">Dropped</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @for ($i = 0; $i < 6; $i++)
                      <tr>
                        <td></td>
                        <td scope="row"></td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                      </tr>
                    @endfor
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="dropForAnalysis">
    <div role="document" class="modal-dialog modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">
            <h5>Flag <b id="name"></b> as “Drop for analysis”</h5>
          </h4>
          <button type="button" class="close"><span>×</span></button>
        </div>
        <div class="modal-body">
          <div>
            <div class="card">
              <div class="card-header">
                <div>
                  <form id="linked" action="/medical-cases" method="POST">
                    @csrf
                  </form>
                  <h6 class="card-subtitle mb-2">
                    <span id="mc_length"></span><a href='javascript:;' id="href_mc"><span id="mc_string"></span></a>
                    will be flagged
                  </h6>
                </div>
                <button type="button" id="confirm" class="float-right btn btn-danger">Confirm</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer"></div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    $("#dropForAnalysis").on("hidden.bs.modal", function() {
      clear();
    });

    $(".close").click(function() {
      clear();
    });

    function clear() {
      $("#name").text('');
      $("#mc_length").text('');
      $("#mc_string").text('');
      $("#href_mc").removeAttr("onClick")
      $("#href_mc").attr("href", 'javascript:;')
      $('#confirm').off('click');
      $('#dropForAnalysis').modal('hide');
    }

    function prepareFordrop(data, patient) {
      var mc_string = patient.medical_cases.length > 1 ? ' medical cases' : ' medical case'
      $("#name").text(`${patient.first_name} ${patient.last_name}`);
      $("#mc_length").text(`${patient.medical_cases.length} linked`);
      $("#mc_string").text(mc_string);
      if (patient.medical_cases.length > 0) {
        $("#href_mc").attr('onClick', 'event.preventDefault(); linked(JSON.parse(\'' + data.dataset.ids + '\'));')
      } else {
        $("#href_mc").removeAttr("href")
      }
      $("#confirm").click(function() {
        drop(patient.id);
      });
      $('#dropForAnalysis').modal('show');
    }

    function linked(mcs) {
      var form = $('#linked');
      for (var key in mcs) {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", `ids[${key}]`);
        hiddenField.setAttribute("value", mcs[key]);

        form.append(hiddenField);
      }
      form.submit();
    }

    function drop(patient_id) {
      $('#dropForAnalysis').modal('hide');
      $.ajax({
        url: "/patients/" + patient_id + "/drop",
        type: 'POST',
        dataType: 'json',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(response) {
          Swal.fire({
            title: "Success!",
            text: response.message,
            icon: "success",
            showClass: {
              popup: 'none'
            },
            didClose: function() {
              location.reload();
            }
          })
        },
        error: function(error) {
          var err = JSON.parse(error.responseText);
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            showClass: {
              popup: 'none'
            },
            text: err.message,
          })
        },
      });
    }

    $(function() {
      function bulkDrop(dt) {
        var selected = [];
        dt.rows({
          selected: true
        }).data().map((patient) => {
          selected.push(patient.local_patient_id);
        })

        $.ajax({
          url: "{{ route('patients.bulkDrop') }}",
          type: 'POST',
          dataType: 'json',
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {
            patients: selected
          },
          success: function(response) {
            Swal.fire({
              title: "Success!",
              text: response.message,
              icon: "success",
              showClass: {
                popup: 'none'
              },
              didClose: function() {
                location.reload();
              }
            })
          },
          error: function(error) {
            var err = JSON.parse(error.responseText);
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              showClass: {
                popup: 'none'
              },
              text: err.message,
            })
          },
        });
      }

      $(".table").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "{{ route('patients.getPatients') }}",
          type: 'POST',
          headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
          data: function(d) {
            d.ids = @json($ids);
          },
        },
        dom: 'Bfrtip',
        select: {
          style: 'os',
          selector: 'td:first-child'
        },
        buttons: [{
            extend: 'selected',
            text: 'Bulk analysis drop',
            action: function(e, dt, button, config) {
              Swal.fire({
                title: 'Are you sure?',
                text: dt.rows({
                  selected: true
                }).data().length + " patients will be flagged as “Drop for analysis”",
                icon: 'warning',
                confirmButtonColor: '#d33',
                confirmButtonText: 'Confirm'
              }).then((result) => {
                if (result.isConfirmed) {
                  bulkDrop(dt);
                }
              });
            }
          },
          'selectAll',
          'selectNone'
        ],
        columns: [{
            data: null,
            defaultContent: '',
            className: 'select-checkbox',
            orderable: false,
            searchable: false
          },
          {
            data: "id",
          },
          {
            data: "local_patient_id",
          },
          {
            data: "first_name",
          },
          {
            data: "middle_name",
          },
          {
            data: "last_name",
          },
          {
            data: "birthdate",
          },
          {
            data: "status",
            name: "merged",
          },
          {
            data: "is_drop",
          },
          {
            data: "show",
            orderable: false,
          },
        ],
      });
    });
  </script>
@stop
