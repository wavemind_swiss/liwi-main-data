@extends('adminlte::page')

@section('content')
  <link href="{{ asset('css/radiobutton.css') }}" rel="stylesheet">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif
            <div class="row justify-content-center">
              <form action="/patients/merge" method="POST" name="form">
                @csrf
                <table class="table">
                  <thead>
                    <th scope="col">Demographics</th>
                    <th scope="col">First Patient</th>
                    <th scope="col">Second Patient</th>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Patient Id:</td>
                      <td>
                        <label id="label_id">{{ $first_patient->local_patient_id }}</label>
                      </td>
                      <td>
                        <label id="label_id">{{ $second_patient->local_patient_id }}</label>
                      </td>
                    </tr>
                    <tr>
                    <tr>
                      <td>Village</td>
                      <td>
                        <label id="label_id">{{ $fp_village }}</label>
                      </td>
                      <td>
                        <label id="label_id">{{ $sp_village }}</label>
                      </td>
                    </tr>
                    <tr>
                    <tr>
                      <td>Caregiver first name</td>
                      <td>
                        <label id="label_id">{{ $fp_first_name_caregiver }}</label>
                      </td>
                      <td>
                        <label id="label_id">{{ $sp_first_name_caregiver }}</label>
                      </td>
                    </tr>
                    <tr>
                    <tr>
                      <td>Caregiver last name</td>
                      <td>
                        <label id="label_id">{{ $fp_last_name_caregiver }}</label>
                      </td>
                      <td>
                        <label id="label_id">{{ $sp_last_name_caregiver }}</label>
                      </td>
                    </tr>
                    <tr>
                      <td>First Name:</td>
                      <td>
                        <label class="container">{{ $first_patient->first_name }}
                          <input type="radio" name="first_name" value="{{ $first_patient->first_name }}" checked>
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$first_patient->first_name}}</label>
                        <div class="check"></div> --}}
                      </td>
                      <td>
                        <label class="container">{{ $second_patient->first_name }}
                          <input type="radio" name="first_name" value="{{ $second_patient->first_name }}">
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$second_patient->first_name}}</label>
                        <div class="check"></div> --}}
                      </td>
                    </tr>
                    <tr>
                      <td>Middle Name:</td>
                      <td>
                        <label class="container">{{ $first_patient->middle_name }}
                          <input type="radio" name="middle_name" value="{{ $first_patient->middle_name }}" checked>
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$first_patient->first_name}}</label>
                        <div class="check"></div> --}}
                      </td>
                      <td>
                        <label class="container">{{ $second_patient->middle_name }}
                          <input type="radio" name="middle_name" value="{{ $first_patient->middle_name }}">
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$second_patient->first_name}}</label>
                        <div class="check"></div> --}}
                      </td>
                    </tr>
                    <tr>
                      <td>Last Name:</td>
                      <td>
                        <label class="container">{{ $first_patient->last_name }}
                          <input type="radio" name="last_name" value="{{ $first_patient->last_name }}" checked>
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$first_patient->last_name}}</label>
                        <div class="check"></div> --}}
                      </td>
                      <td>
                        <label class="container">{{ $second_patient->last_name }}
                          <input type="radio" name="last_name" value="{{ $second_patient->last_name }}">
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$second_patient->last_name}}</label>
                        <div class="check"></div> --}}
                      </td>
                    </tr>
                    <tr>
                      <td>BirthDate:</td>
                      <td>
                        <label class="container">{{ $first_patient->birthdate }}
                          <input type="radio" name="birthdate" value="{{ $first_patient->birthdate }}" checked>
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$first_patient->birthdate}}</label>
                        <div class="check"></div> --}}
                      </td>
                      <td>
                        <label class="container">{{ $second_patient->birthdate }}
                          <input type="radio" name="birthdate" value="{{ $second_patient->birthdate }}">
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$second_patient->birthdate}}</label>
                        <div class="check"></div> --}}
                      </td>
                    </tr>
                    <tr>
                      <td>Weight:</td>
                      <td>
                        <label class="container">{{ $first_patient->weight }}
                          <input type="radio" name="weight" value="{{ $first_patient->weight }}" checked>
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$first_patient->weight}}</label>
                        <div class="check"></div> --}}
                      </td>
                      <td>
                        <label class="container">{{ $second_patient->weight }}
                          <input type="radio" name="weight" value="{{ $second_patient->weight }}">
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$second_patient->weight}}</label>
                        <div class="check"></div> --}}
                      </td>
                    </tr>
                    <tr>
                      <td>Gender:</td>
                      <td>
                        <label class="container">{{ $first_patient->gender }}
                          <input type="radio" name="gender" value="{{ $first_patient->gender }}" checked>
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$first_patient->gender}}</label>
                        <div class="check"></div> --}}
                      </td>
                      <td>
                        <label class="container">{{ $second_patient->gender }}
                          <input type="radio" name="gender" value="{{ $second_patient->gender }}">
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$second_patient->gender}}</label>
                        <div class="check"></div> --}}
                      </td>
                    </tr>
                    <tr>
                      <td>Other Id:</td>
                      <td>
                        <label class="container">{{ $first_patient->other_id }}
                          <input type="radio" name="other_id" value="{{ $first_patient->other_id }}" checked>
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$first_patient->group_id}}</label>
                        <div class="check"></div> --}}
                      </td>
                      <td>
                        <label class="container">{{ $second_patient->group_id }}
                          <input type="radio" name="other_id" value="{{ $second_patient->other_id }}">
                          <span class="checkmark"></span>
                        </label>
                        {{-- <label id="label_id">{{$second_patient->group_id}}</label>
                        <div class="check"></div> --}}
                      </td>
                    </tr>
                    <tr>
                      <td>Related Ids:</td>
                      <td>
                        <ul>
                          @foreach ($first_patient->related_ids as $id)
                            <li>{{ $id }}</li>
                          @endforeach
                        </ul>
                      </td>
                      <td>
                        <ul>
                          @foreach ($second_patient->related_ids as $id)
                            <li>{{ $id }}</li>
                          @endforeach
                        </ul>
                      </td>
                    </tr>
                    <tr>
                      <td>Number of medical Cases:</td>
                      <td>
                        <label>{{ $first_patient->medical_cases()->count() }}</label>
                      </td>
                      <td>
                        <label>{{ $second_patient->medical_cases()->count() }}</label>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <input id="patient_id" type="text" name="firstp_id" value="{{ $first_patient->id }}" hidden>
                <input id="patient_id" type="text" name="secondp_id" value="{{ $second_patient->id }}" hidden>
                @if (!empty(app('request')->input('criteria')))
                  @if (strpos(app('request')->input('criteria'), '|') !== false)
                    @foreach (explode('|', app('request')->input('criteria')) as $crit)
                      <input id="criteria" type="text" name="searchCriteria[]" value="{{ $crit }}"
                        hidden>
                    @endforeach
                  @else
                    <input id="criteria" type="text" name="searchCriteria[]"
                      value="{{ app('request')->input('criteria') }}" hidden>
                  @endif
                @endif
                <div class="row ">
                  <div class="float-right">
                    <input type="submit" name="merge" id="merge" value="Merge patients" />
                    <input type="submit" name="Keep" id="Keep" value="Keep both patients">
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop
