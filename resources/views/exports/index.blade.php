@extends('adminlte::page')
@section('content')
  <div class="container-fluid">
    <div class="col-md-5">
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      <div class="card">
        <div class="card-header">Download Anonymized Export</div>
        <div class="card-body">
          @forelse($files as $file)
            @if ($file->getFilename() != 'full_export_separated.zip')
              <div class="col-md-10 mb-3">
                <a class="btn btn-outline-secondary"
                  href="{{ route('exports.download', ['file' => $file->getFilename()]) }}">
                  {{ date('d/m/Y', $file->getMTime()) }}
                  {{ $file->getFilename() }}
                </a>
              </div>
            @endif
          @empty
            <p>No files available</p>
          @endforelse
        </div>
      </div>
      @can('Access_Full_Data_Export')
        <div class="card">
          <div class="card-header">Download Full Export</div>
          <div class="card-body">
            @if ($full_data_set)
              <div class="col-md-10 mb-3">
                <a class="btn btn-outline-secondary"
                  href="{{ route('exports.download', ['file' => $file->getFilename()]) }}">
                  {{ date('d/m/Y', $file->getMTime()) }}
                  {{ $file->getFilename() }}
                </a>
              </div>
            @else
              <p>No file available</p>
            @endif
          </div>
        </div>
      </div>
    @endcan
  </div>
@stop
