@extends('adminlte::page')

<link href="{{ asset('css/datatable.css') }}" rel="stylesheet">

@include('medicalCases.compareModal')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex ">
                        <span>
                            <h3>Diagnoses</h3>
                        </span>
                        <div class="ml-auto p-2">
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Case ID</th>
                                            <th scope="col">Patient ID</th>
                                            <th scope="col">Diagnosis</th>
                                            <th scope="col">Came From</th>
                                            <th scope="col">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @for ($i = 0; $i < 6; $i++)
                                            <tr>
                                                <th scope="row"></th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $(".table").DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('diagnoses.getDiagnoses') }}",
                columns: [{
                        data: "id",
                    },
                    {
                        data: "local_medical_case_id",
                        name: "medical_case.local_medical_case_id",
                    },
                    {
                        data: "local_patient_id",
                        name: "medical_case.patient.local_patient_id",
                    },
                    {
                        data: "diagnosis_label",
                        name: "diagnosis.label",
                    },
                    {
                        data: "facility_name",
                        name: "medical_case.facility.name",
                    },
                    {
                        data: "status",
                        name: "status",
                    },
                ],
            });

        });
    </script>
@stop
