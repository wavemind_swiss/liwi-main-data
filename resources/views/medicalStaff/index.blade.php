@extends('adminlte::page')

 <!-- The contents of the layout/app.bade.php are included so that the view components work correctly -->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>
      {{-- {{ config('app.name', 'Laravel') }} --}}
      Medical Staff
    </title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
</head>


@section('content')

<body>
    <div id="app" class="colorGrey">
    <medical-staff :medical_staff_data="{{$medical_staff}}"
                   :medical_staff_roles_data="{{$medical_staff_roles}}"
                    medical_staff_route="{{route("medical-staff.index")}}"
                    health_facilities_route="{{route("health-facilities.index")}}"
              ></medical-staff>
    </div>
</body>

@stop
