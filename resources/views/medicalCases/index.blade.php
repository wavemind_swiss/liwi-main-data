@extends('adminlte::page')

<link href="{{ asset('css/datatable.css') }}" rel="stylesheet">

@include('medicalCases.compareModal')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header d-flex ">
            <span>
              <h3>Medical Cases</h3>
            </span>
          </div>
          <div class="card-body">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif
            <div class="row">
              <div class="col-md-12">
                <table class="table">
                  <thead>
                    <tr>
                      <th></th>
                      <th scope="col">ID</th>
                      <th scope="col">Case id</th>
                      <th scope="col">Belongs to patient id</th>
                      <th scope="col">Date Created</th>
                      <th scope="col">Facility</th>
                      <th scope="col">App version</th>
                      <th scope="col">Dropped</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @for ($i = 0; $i < 6; $i++)
                      <tr>
                        <td></td>
                        <td scope="row"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    @endfor
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(function() {
      function bulkDrop(dt) {
        var selected = [];

        dt.rows({
          selected: true
        }).data().map((mc) => {
          selected.push(mc.local_medical_case_id);
        })

        $.ajax({
          url: "{{ route('medical-cases.bulkDrop') }}",
          type: 'POST',
          dataType: 'json',
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: {
            ids: selected
          },
          success: function(response) {
            Swal.fire({
              title: "Success!",
              text: response.message,
              icon: "success",
              showClass: {
                popup: 'none'
              },
              didClose: function() {
                location.reload();
              }
            })
          },
          error: function(error) {
            var err = JSON.parse(error.responseText);
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              showClass: {
                popup: 'none'
              },
              text: err.message,
            })
          },
        });
      }

      $(".table").DataTable({
        processing: true,
        serverSide: true,
        "ajax": {
          url: "{{ route('medical-cases.getMedicalCases') }}",
          type: 'POST',
          headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
          data: function(d) {
            d.ids = @json($ids);
          },
        },
        dom: 'Bfrtip',
        select: {
          style: 'os',
          selector: 'td:first-child'
        },
        buttons: [{
            extend: 'selected',
            text: 'Bulk analysis drop',
            action: function(e, dt, button, config) {
              Swal.fire({
                title: 'Are you sure?',
                text: dt.rows({
                  selected: true
                }).data().length + " medical case will be flagged as “Drop for analysis”",
                icon: 'warning',
                confirmButtonColor: '#d33',
                confirmButtonText: 'Confirm'
              }).then((result) => {
                if (result.isConfirmed) {
                  bulkDrop(dt);
                }
              });
            }
          },
          'selectAll',
          'selectNone'
        ],
        columns: [{
            data: null,
            defaultContent: '',
            className: 'select-checkbox',
            orderable: false,
            searchable: false
          },
          {
            data: "id",
          },
          {
            data: "local_medical_case_id",
          },
          {
            data: "local_patient_id",
            name: "patient.local_patient_id",
          },
          {
            data: "consultation_date",
          },
          {
            data: "facility",
            name: "facility.name",
          },
          {
            data: "app_version",
          },
          {
            data: "is_drop",
          },
          {
            data: "show",
            orderable: false,
          },
        ],
      });
    });

    function drop(medical_case_id) {
      Swal.fire({
        title: 'Are you sure?',
        text: "This medical case will be flagged as “Drop for analysis”",
        icon: 'warning',
        confirmButtonColor: '#d33',
        confirmButtonText: 'Confirm'
      }).then((result) => {
        if (result.isConfirmed) {
          $.ajax({
            url: "/medical-cases/" + medical_case_id + "/drop",
            type: 'POST',
            dataType: 'json',
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
              Swal.fire({
                title: "Success!",
                text: response.message,
                icon: "success",
                showClass: {
                  popup: 'none'
                },
                didClose: function() {
                  location.reload();
                }
              })
            },
            error: function(error) {
              var err = JSON.parse(error.responseText);
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                showClass: {
                  popup: 'none'
                },
                text: err.message,
              })
            },
          });
        }
      })
    }
  </script>
@stop
