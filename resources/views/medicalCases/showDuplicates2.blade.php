@extends('adminlte::page')

@include('medicalCases.compareModal')

@section('content')
<script src="{{ asset('js/dpCase.js') }}" defer></script>
<div class="container-fluid">
  <div class="row justify-content-center">
    {{-- <div> --}}
    <div class="col-md-12">
      {{-- <div> --}}
      <div class="card">
        <div class="card-header"><button class="btn btn-outline-dark float-right" onclick="compareMedicalCases()">Compare Medical Cases</button></div>
        <div class="card-body">
          @if (session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
          @endif
          <div class="row justify-content-center">
            <div class="modal" tabindex="-1" id="markRow" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Mark Medical Case</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <p id="display">You want to mark this MedicalCase?.This will inturn remove its follow up from Redcap  </p>
                  </div>
                  <div class="modal-footer">
                    <form action="{{route('medical-cases.deduplicate_redcap')}}" method="POST">
                      @csrf
                      <input id="medicalcase_id" type="text" name="medicalc_id"  hidden>
                      <button type="submit" class="btn btn-primary" >Save changes</button>
                    </form>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-center mt-3">
            <div class="col-md-12">
              {{-- <form action="{{route('medical-cases.searchDuplicates')}}" method="POST" id="searchform" class="input-group mb-3">
                @csrf
                <div class="input-group-prepend">
                  <button class="btn btn-outline-secondary" type="Submit">Search</button>
                </div>
                <select class="custom-select" name="search" form="searchform">
                  <option selected>Choose...</option>
                  <option value="patient_id">Patient Id</option>
                  <option value="version_id">Version Id</option>
                </select>
              </form> --}}
              <table class="table table-hover table-bordered">
                <thead class="thead-light">
                  <th scope="col">Sn</th>
                  {{-- <th scope="col">Medical Case Id</th> --}}
                  <th scope="col">local_medical_case_id</th>
                  <th scope="col">local_patient_id</th>
                  <th scope="col">Consultation Date</th>
                  <th scope="col">HF</th>
                  {{-- <th scope="col">caregiver Phone</th> --}}
                  {{-- <th scope="col">Refer Followup consultation</th> --}}
                  <th scope="col">Mark</th>
                  <th scope="col">Flag</th>
                </thead>
                <tbody>

                </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @stop
