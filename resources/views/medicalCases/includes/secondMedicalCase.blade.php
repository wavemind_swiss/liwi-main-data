@if ($second_medical_case)
    <div class="card">
        <div class="card-color">
            <div class="card-header">
                <div class="d-flex justify-content-between">
                    <span class="font-weight-bold">Consultation date</span>
                    <span class="border-bottom">{{ $second_medical_case->consultation_date }}</span>
                </div>
            </div>
            <div class="card-header">
                <div class="d-flex justify-content-between">
                    <span class="font-weight-bold">Created at</span>
                    <span class="border-bottom">{{ $second_medical_case->created_at }}</span>
                </div>
            </div>
            <div class="card-header">
                <div class="d-flex justify-content-between">
                    <span class="font-weight-bold">Patient name</span>
                    <span class="border-bottom">
                        {{ $second_medical_case->patient->first_name }}
                        {{ $second_medical_case->patient->last_name }}
                    </span>
                </div>
            </div>
            <div class="card-header ">
                <div class="d-flex justify-content-between">
                    <span class="font-weight-bold">Medical case ID</span>
                    <span class="border-bottom">{{ $second_medical_case->local_medical_case_id }}</span>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="card-header">No first Medical Case</div>
@endif
