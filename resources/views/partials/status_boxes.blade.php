
<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-blue">
      <div class="inner">
        <h3> {{$patientsCount}} </h3>

        <p>Registered Patients</p>
      </div>
      <div class="icon">
        <i class="fa fa-users"></i>
      </div>
      {{-- <a href="#" class="small-box-footer">
        View them <i class="fa fa-arrow-circle-right"></i>
      </a> --}}
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner">
        <h3> {{$medicalCasesCount}} </h3>

        <p>Total Medical cases</p>
      </div>
      <div class="icon">
        <i class="fa fa-clipboard"></i>
      </div>
      {{-- <a href="#" class="small-box-footer">
        View them <i class="fa fa-arrow-circle-right"></i>
      </a> --}}
    </div>
  </div>
  <!-- ./col -->
  
  <!-- ./col -->
  