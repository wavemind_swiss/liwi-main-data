<div>
  <h2 class="m-2 card-title">Last diagnostic sent</h2>
  @if (isset($json_diagnostic['sync_info']))
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">SN</th>
          <th scope="col">Date</th>
          <th scope="col">Medical cases</th>
          <th scope="col">Patients</th>
          <th scope="col">Closed Medical cases</th>
          <th scope="col">Not Synchronized Medical cases</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($json_diagnostic['sync_info'] as $key => $auto_diag)
          @if ($auto_diag['status'])
            <tr>
              <th scope="row">{{ $key + 1 }}</th>
              <td>{{ $auto_diag['date_received'] }}</td>
              <td>{{ $auto_diag['data']['case_count'] }}</td>
              <td>{{ $auto_diag['data']['patient_count'] }}</td>
              <td>{{ $auto_diag['data']['case_close'] }}</td>
              <td>{{ $auto_diag['data']['synchronized_false'] }}</td>
              <td>
                @if (!$auto_diag['no_error'])
                  <a href="{{ route('recent_errors.show', [
                      'file' => $auto_diag['path'],
                      'device' => $device,
                  ]) }}"
                    class="btn btn-outline-dark" style="cursor: pointer;">Errors</a>
                @else
                  No error
                @endif
              </td>
            </tr>
          @elseif (isset($auto_diag['data']['errorMessage']))
            <tr>
              <th scope="row">1</th>
              <td>Error:</td>
              <td colspan="5">There Was an error {{ $auto_diag['data']['errorMessage'] }}</td>
              <td></td>
            </tr>
          @else
            <tr>
              <th scope="row">1</th>
              <td colspan="5">There was an error fetching the result</td>
            </tr>
          @endif
        @endforeach
      </tbody>
    </table>
  @else
    @include('partials.noInfo')
  @endif
</div>
