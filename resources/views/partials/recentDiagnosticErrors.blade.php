@if (isset($json_diagnostic['recent_errors']))
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">SN</th>
                <th scope="col">Date</th>
                <th scope="col">Level</th>
                <th scope="col">Message</th>
            </tr>
        </thead>
        <tbody>
            @if (isset($json_diagnostic['recent_errors']['data']['errorMessage']))
                <tr>
                    <th scope="row">X</th>
                    <td>Error</td>
                    <td>There was an error</td>
                    <td>{{$json_diagnostic['recent_errors']['data']['errorMessage']}}</td>
                </tr>
            @else
                @foreach ($json_diagnostic['recent_errors']['data'] as $key => $error)
                    <tr>
                        <th scope="row">{{$key+ 1}}</th>
                        <td>{{$error['date']}}</td>
                        <td>{{$error['level']}}</td>
                        <td style="word-break:break-all;">
                            <div class="header" style="overflow: hidden; height: 30px; cursor: zoom-in;">{!! nl2br(e($error['text'] ."\n".$error['stack'])) !!}</div>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
@else
    <span class="font-weight-bold p-2" >No diagnostic found</span>
@endif

<script type="application/javascript">
    $(function() {
        $('.header').click(function() {
            var selection = window.getSelection();
            if(selection.toString().length === 0) {
                if(this.style.overflow == 'hidden') {
                    $('.header').css({
                            'overflow': 'hidden',
                            'height': '30px',
                            'cursor':'zoom-in'
                    });
                    $(this).css({
                        'overflow': 'auto',
                        'height': 'auto',
                        'cursor':'zoom-out'
                    });
                } else {
                    $(this).css({
                        'overflow': 'hidden',
                        'height': '30px',
                        'cursor':'zoom-in'
                    });
                }
            }
        });
    });
</script>
