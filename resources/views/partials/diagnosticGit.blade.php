@if (isset($json_diagnostic['git_info']))
    <div class="col p-2">
        @if($json_diagnostic['git_info']['status'])
            <table class="table table-striped">
                <thead>
                    <tr>
                    <th scope="col">Hub Commit</th>
                    <th scope="col">Hub Branch</th>
                    <th scope="col">Up-to-date</th>
                    <th scope="col">Commits behind</th>
                    <th scope="col">Current Commit</th>
                    <th scope="col">Current Commit Date</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="col">{{$json_diagnostic['git_info']['data']['commit_hash']}}</td>
                        <td scope="col">{{$json_diagnostic['git_info']['data']['git_branch']}}</td>
                    @if (isset($json_diagnostic['git_info']['data']['hub_up_to_date']))
                        @if ($json_diagnostic['git_info']['data']['hub_up_to_date'])
                            <td><i class="fa fa-check" style="color:green !important" aria-hidden="true"></i></td>
                        @else
                            <td><i class="fa fa-ban" style="color:red !important" aria-hidden="true"></i></td>
                        @endif
                    @else
                        <td scope="col">No information</td>
                    @endif
                    @if (isset($json_diagnostic['git_info']['data']['hub_commit_position']))
                        <td scope="col">{{$json_diagnostic['git_info']['data']['hub_commit_position']}}</td>
                    @else
                        <td scope="col">No information</td>
                    @endif
                    @if (isset($json_diagnostic['git_info']['data']['branch_current_commit']))
                        <td scope="col">{{$json_diagnostic['git_info']['data']['branch_current_commit']}}</td>
                    @else
                        <td scope="col">No information</td>
                    @endif
                    @if(isset($json_diagnostic['git_info']['data']['branch_commit_time']))
                        <td scope="col">{{$json_diagnostic['git_info']['data']['branch_commit_time']}}</td>
                    @else
                        <td scope="col">No information</td>
                    @endif
                </tbody>
            </table>
        @elseif (isset($json_diagnostic['git_info']['data']['errorMessage']))
            <span class="h4 p-2">There was an error {{$json_diagnostic['git_info']['data']['errorMessage']}}</span>
        @else
            <span class="h4 p-2">There was an error fetching the result</span>
        @endif
    </div>
@else
    @include('partials.noInfo')
@endif
