@extends('adminlte::page')

<link href="{{ asset('css/datatable.css') }}" rel="stylesheet">

@include('medicalCases.compareModal')

@section('content')
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header d-flex ">
            <span>
              <h3>Drugs</h3>
            </span>
            <div class="ml-auto p-2">
            </div>
          </div>
          <div class="card-body">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif
            <div class="row">
              <div class="col-md-12">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Case ID</th>
                      <th scope="col">Diagnosis</th>
                      <th scope="col">Custom diagnosis</th>
                      <th scope="col">Drug</th>
                      <th scope="col">Status</th>
                      <th scope="col">Description</th>
                    </tr>
                  </thead>
                  <tbody>
                    @for ($i = 0; $i < 6; $i++)
                      <tr>
                        <td scope="row"></td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                      </tr>
                    @endfor
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(function() {
      $(".table").DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('drugs.getDrugs') }}",
        columns: [{
            data: "id",
          },
          {
            data: "local_medical_case_id",
            name: "diagnosisReference.medical_case.local_medical_case_id",
          },
          {
            data: "diagnosis_label",
            name: "diagnosisReference.diagnoses.label",
          },
          {
            data: "custom_diagnosis_label",
            name: "customDiagnosisReference.label",
          },
          {
            data: "drug_label",
            name: "drug.label",
          },
          {
            data: "status.description",
            name: "drug_references.status",
          },
          {
            data: "formulation",
            name: "drug.formulations.description",
          },
        ],
      });

    });
  </script>
@stop
