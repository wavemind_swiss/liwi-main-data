$(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        type: "GET",
        url: "/medical-cases/duplicate2",
    }).done(function (result) {
        result.mcs.forEach(dpGroupParent);
    });

    function dpGroupParent(children, index) {
        markup = `<tr class='table-secondary'><td>For The ${
            index + 1
        }'s Duplicate<td></tr>`;
        tableBody = $("table tbody");
        tableBody.append(markup);
        children.forEach(dpGroupChild);
    }
    function dpGroupChild(child, index) {
        markup = `
        <tr>
        <th scope="row">${index + 1}</th>
        <td>${child.local_medical_case_id}</td>
        <td>${child.patient.local_patient_id}</td>
        <td>${child.consultation_date}</td>
        <td>${child.facility.name}</td>
        <td><input type="checkbox" class="messageCheckbox" value="${
            child.id
        }"></td>
        <td><a  class="btn btn-outline-primary" data-toggle="modal" data-target="#markRow" onclick="takeCaseId(${
            child.id
        })">Mark Duplicate</a></td>
        </tr>`;
        tableBody = $("table tbody");
        tableBody.append(markup);
    }
    function comparePatients() {
        let checkedValue = [];
        let inputElements = document.getElementsByClassName("messageCheckbox");
        for (let i = 0; inputElements[i]; ++i) {
            if (inputElements[i].checked) {
                checkedValue.push(inputElements[i].value);
            }
        }
        if (checkedValue.length == 2) {
            location.href = `/patients/compare/${checkedValue[0]}/${checkedValue[1]}`;
        } else {
            $("#modalCheckBox").modal("show");
        }
    }

    function compareMedicalCases() {
        let checkedValue = [];
        let inputElements = document.getElementsByClassName("messageCheckbox");
        for (let i = 0; inputElements[i]; ++i) {
            if (inputElements[i].checked) {
                checkedValue.push(inputElements[i].value);
            }
        }
        if (checkedValue.length == 2) {
            location.href = `/medical-cases/compare/${checkedValue[0]}/${checkedValue[1]}`;
        } else {
            $("#modalCheckBoxMedical").modal("show");
        }
    }

    function mergePatients() {
        let checkedValue = [];
        let inputElements = document.getElementsByClassName("messageCheckbox");
        for (let i = 0; inputElements[i]; ++i) {
            if (inputElements[i].checked) {
                checkedValue.push(inputElements[i].value);
            }
        }
        if (checkedValue.length == 2) {
            location.href = `/patients/merge/${checkedValue[0]}/${checkedValue[1]}`;
        } else {
            $("#modalCheckBoxMerge").modal("show");
        }
    }
});
