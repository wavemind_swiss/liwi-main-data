/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

import HealthFacilities from "./components/HealthFacilities.vue";
import Devices from "./components/Devices.vue";
import MedicalStaff from "./components/MedicalStaff.vue";

export const eventBus = new Vue();

/**
 * Notification plugin used
 */
import { TablePlugin } from "bootstrap-vue";
import Toasted from "vue-toasted";

Vue.use(Toasted);
Vue.use(TablePlugin);

Vue.toasted.register(
    "error_notification",
    (message) => {
        return message;
    },
    {
        theme: "bubble",
        type: "error",
        keepOnHover: true,
        duration: 2000,
        action: [
            {
                text: "Close",
                onClick: (e, toastObject) => {
                    toastObject.goAway(0);
                },
            },
        ],
    }
);

Vue.toasted.register(
    "success_notification",
    (message) => {
        return message;
    },
    {
        type: "success",
        theme: "bubble",
        keepOnHover: true,
        duration: 2000,
        action: [
            {
                text: "Close",
                onClick: (e, toastObject) => {
                    toastObject.goAway(0);
                },
            },
        ],
    }
);

const app = new Vue({
    el: "#app",
    components: {
        HealthFacilities,
        Devices,
        MedicalStaff,
    },
});
