<?php

use App\Enums\ClinicianChoice;

return [
    ClinicianChoice::class => [
        ClinicianChoice::AGREED => 'Agreed',
        ClinicianChoice::ADDITIONAL => 'Additional',
        ClinicianChoice::REFUSED => 'Refused',
    ],
];
