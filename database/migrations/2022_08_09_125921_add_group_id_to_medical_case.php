<?php

use App\MedicalCase;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Log;

class AddGroupIdToMedicalCase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        MedicalCase::query()->with(['patient'])->chunkById(50, function (Collection $medicalCases) {
            $medicalCases->each(function (MedicalCase $medicalCase) {
                if (!$medicalCase->patient->exists()) {
                    Log::error("Medical case {$medicalCase->id} has no patient");

                    return 0;
                }

                $medicalCase->update([
                    'group_id' => $medicalCase->patient->group_id,
                ]);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        MedicalCase::query()->update(['group_id' => null]);
    }
}
