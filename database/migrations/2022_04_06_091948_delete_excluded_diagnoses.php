<?php

use App\Console\Commands\FixSeqAfterImport;
use App\ExcludedDiagnosesList;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DeleteExcludedDiagnoses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Créer la table temp
        Schema::create('diagnosis_references_temp', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->boolean('agreed')->nullable();
            $table->boolean('additional')->nullable();
            $table->integer('diagnosis_id')->nullable();
            $table->integer('medical_case_id')->nullable();
            $table->timestamps();
            $table->boolean('excluded')->nullable()->default(false);
        });

        // Insérer tout les diag ref non excluded
        DB::statement('INSERT INTO diagnosis_references_temp (
            id,
            agreed,
            additional,
            diagnosis_id,
            medical_case_id,
            created_at,
            updated_at,
            excluded)
        SELECT
            id,
            agreed,
            additional,
            diagnosis_id,
            medical_case_id,
            created_at,
            updated_at,
            excluded
        FROM   diagnosis_references
        WHERE  excluded IS FALSE');

        // Désactiver les contraintes d'intégrités
        DB::statement('ALTER TABLE drug_references DROP CONSTRAINT drug_references_diagnosis_id_foreign');
        DB::statement('ALTER TABLE management_references DROP CONSTRAINT management_references_diagnosis_id_foreign');

        // Truncate la table diagnosis_references
        DB::table('diagnosis_references')->truncate();

        // Réimporter les données temps dans la table originale
        DB::statement('INSERT INTO diagnosis_references (
            id,
            agreed,
            additional,
            diagnosis_id,
            medical_case_id,
            created_at,
            updated_at,
            excluded)
        SELECT
            id,
            agreed,
            additional,
            diagnosis_id,
            medical_case_id,
            created_at,
            updated_at,
            excluded
        FROM   diagnosis_references_temp');

        // Delete la table temporaire
        Schema::dropIfExists('diagnosis_references_temp');

        // Réactiver les contraintes
        DB::statement(
            'ALTER TABLE drug_references ADD CONSTRAINT drug_references_diagnosis_id_foreign FOREIGN KEY (diagnosis_id)
            REFERENCES public.diagnosis_references (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION'
        );
        DB::statement(
            'ALTER TABLE management_references ADD CONSTRAINT management_references_diagnosis_id_foreign FOREIGN KEY (diagnosis_id)
            REFERENCES public.diagnosis_references (id) MATCH SIMPLE
            ON UPDATE NO ACTION
            ON DELETE NO ACTION'
        );

        // run php artisan db:fix_seq pour s'assurer le l'index de séquence
        Artisan::call(FixSeqAfterImport::class);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        ExcludedDiagnosesList::query()->chunkById(10, function (Collection $excludedDiagnosesList) {
            $diagnosisReferencesData = [];

            $excludedDiagnosesList->each(function (ExcludedDiagnosesList $excludedDiagnosesList) use (&$diagnosisReferencesData) {
                $newDiagnosesReferences = collect($excludedDiagnosesList->excluded_diagnoses)->map(function ($diagnosisId) use ($excludedDiagnosesList) {
                    return [
                        'agreed' => false,
                        'additional' => false,
                        'diagnosis_id' => $diagnosisId,
                        'medical_case_id' => $excludedDiagnosesList->medical_case_id,
                        'excluded' => true,
                    ];
                });

                $diagnosisReferencesData = array_merge($diagnosisReferencesData, $newDiagnosesReferences->toArray());
            });

            DB::table('diagnosis_references')->insert($diagnosisReferencesData);
        });
    }
}
