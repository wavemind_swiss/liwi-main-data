<?php

use App\Enums\ClinicianChoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RemoveOldStatusFieldsInDrugReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drug_references', function (Blueprint $table) {
            $table->dropColumn([
                'agreed',
                'additional',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drug_references', function (Blueprint $table) {
            $table->boolean('agreed')->nullable()->after('diagnosis_id');
            $table->boolean('additional')->nullable()->after('formulation_id');
        });

        DB::table('drug_references')->update([
            'agreed' => DB::raw("status IN ('" . ClinicianChoice::AGREED . "', '" . ClinicianChoice::ADDITIONAL . "')"),
            'additional' => DB::raw("status = '" . ClinicianChoice::ADDITIONAL . "'"),
        ]);
    }
}
