<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutoDiagnosticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_diagnostics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('device_id');
            $table->foreign('device_id')->references('id')->on('devices');
            $table->integer('commit_pos')->nullable()->default(null);
            $table->boolean('up_to_date')->nullable()->default(null);
            $table->string('path');
            $table->string('bitbucket_commit')->nullable()->default(null);
            $table->string('bitbucket_commit_time')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_diagnostics');
    }
}
