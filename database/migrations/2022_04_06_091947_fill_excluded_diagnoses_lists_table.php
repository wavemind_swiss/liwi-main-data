<?php

use App\DiagnosisReference;
use App\MedicalCase;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class FillExcludedDiagnosesListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        MedicalCase::query()->with(['diagnoses_references'])->chunkById(10, function (Collection $medicalCases) {
            $excludedDiagnosesListsData = collect();

            $medicalCases->each(function (MedicalCase $medicalCase) use (&$excludedDiagnosesListsData) {
                // Select excluded diagnosis references.
                $excludedDiagnosisReferences = $medicalCase->diagnoses_references->filter(function (DiagnosisReference $diagnosisReference) {
                    return $diagnosisReference->excluded;
                });

                if ($excludedDiagnosisReferences->isEmpty()) {
                    // Continue with next medical case because this one has no excluded diagnoses.
                    return 0;
                }

                // Keep diagnoses ids.
                $excludedDiagnosisIds = $excludedDiagnosisReferences->map(function (DiagnosisReference $diagnosisReference) {
                    return $diagnosisReference->diagnosis_id;
                });

                $excludedDiagnosesListsData->push([
                    'medical_case_id' => $medicalCase->id,
                    'excluded_diagnoses' => $excludedDiagnosisIds->values(),
                ]);
            });

            DB::table('excluded_diagnoses_lists')->insert($excludedDiagnosesListsData->toArray());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('excluded_diagnoses_lists')->truncate();
    }
}
