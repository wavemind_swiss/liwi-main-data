<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCorrectedDob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medical_cases', function (Blueprint $table) {
            $table->boolean('blankcase')->nullable()->default(false);
            $table->string('corrected_bd5ageindays')->nullable()->default(null);
        });
        Schema::table('patients', function (Blueprint $table) {
            $table->string('corrected_birthdate')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medical_cases', function (Blueprint $table) {
            $table->dropColumn(['corrected_bd5ageindays', 'blankcase']);
        });
        Schema::table('patients', function (Blueprint $table) {
            $table->dropColumn('corrected_birthdate');
        });
    }
}
