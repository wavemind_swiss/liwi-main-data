<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveUnusedFieldsInManagementReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('management_references', function (Blueprint $table) {
            $table->dropTimestamps();
            $table->dropColumn('agreed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('management_references', function (Blueprint $table) {
            $table->boolean('agreed')->nullable();
            $table->timestamps();
        });
    }
}
