<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveUnusedFieldsInDrugReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drug_references', function (Blueprint $table) {
            $table->dropColumn('formulationSelected');
            $table->dropTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drug_references', function (Blueprint $table) {
            $table->timestamps();
            $table->integer('formulationSelected')->nullable()->after('agreed');
        });
    }
}
