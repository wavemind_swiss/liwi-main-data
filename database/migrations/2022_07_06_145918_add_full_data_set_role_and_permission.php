<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddFullDataSetRoleAndPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->addPermissionAndRole('Data Manager Full Data Set', [
            'Access_Reset_Own_Password_Panel',
            'Access_Profile_Panel',
            'Access_Medical_Cases_Panel',
            'Access_Patients_Panel',
            'Access_Export_Panel',
            'Access_Drugs_Panel',
            'Access_Diagnoses_Panel',
            'Access_Health_Facilities_Panel',
            'Access_Facilities_Panel',
            'Access_Devices_Panel',
            'Access_Medical_Staff_Panel',
            'Access_Duplicates_Panel',
            'Access_Follow_Up_Panel',
            'Reset_Own_Password',
            'Manage_Patients',
            'Manage_Patients_Merge_Duplicates',
            'Manage_Medical_Cases',
            'View_Follow_Ups',
            'View_Questions',
            'Manage_Health_Facilities',
            'Manage_Medical_Staff',
            'Manage_Devices',
            'Export',
            'See_Sensitive_Data',
            'Access_Failed_Json_Panel',
            // New ones
            'Access_Full_Data_Export',
            'Download_Full_Data_Export',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permission_names = [
            'Access_Full_Data_Export',
            'Download_Full_Data_Export',
        ];
        $role = Role::where('name', 'Data Manager Full Data Set')->first();

        foreach ($permission_names as $permission_name) {
            $permission = Permission::where('name', '=', $permission_name)->first();
            DB::table('role_has_permissions')
                ->where('permission_id', $permission->id)
                ->where('role_id', $role->id)
                ->delete();
            $permission->delete();
        }
        $role->delete();
    }

    private function addPermissionToRole($permission_name, $role)
    {
        $permission = Permission::where('name', '=', $permission_name)->first();
        $role_has_permission = DB::table('role_has_permissions')
            ->where('permission_id', $permission->id)
            ->where('role_id', $role->id)
            ->first();

        if ($role_has_permission) {
            return;
        }

        DB::table('role_has_permissions')->updateOrInsert([
            'permission_id' => $permission->id,
            'role_id' => $role->id,
        ]);
    }

    private function addPermissionAndRole($name, $permission_names)
    {
        $role = Role::firstOrCreate([
            'name' => $name,
            'guard_name' => 'web',
        ]);

        foreach ($permission_names as $permission_name) {
            Permission::firstOrCreate([
                'name' => $permission_name,
            ]);
            $this->addPermissionToRole($permission_name, $role);
        }
        Log::info('Added new role ' . $role->name . '.');
    }
}
