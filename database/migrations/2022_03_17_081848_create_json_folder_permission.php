<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class CreateJsonFolderPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permission = Permission::firstOrCreate([
            'name' => 'Access_Failed_Json_Panel',
            'guard_name' => 'web',
        ]);

        $admin_role = DB::table('roles')->where('name', 'Administrator')->first();
        $data_role = DB::table('roles')->where('name', 'Data Manager')->first();

        DB::table('role_has_permissions')->insert([
            'permission_id' => $permission->id,
            'role_id' => $admin_role->id,
        ]);
        DB::table('role_has_permissions')->insert([
            'permission_id' => $permission->id,
            'role_id' => $data_role->id,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permission = Permission::where('name', 'Access_Failed_Json_Panel')->first();
        $admin_role = DB::table('roles')->where('name', 'Administrator')->first();
        $data_role = DB::table('roles')->where('name', 'Data Manager')->first();

        DB::table('role_has_permissions')
            ->where('permission_id', $permission->id)
            ->where('role_id', $admin_role->id)
            ->delete();

        DB::table('role_has_permissions')
            ->where('permission_id', $permission->id)
            ->where('role_id', $data_role->id)
            ->delete();

        $permission->delete();
    }
}
