<?php

use App\Enums\ClinicianChoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class PopulateStatusInDiagnosisReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('diagnosis_references')->update([
            'status' => DB::raw("CASE
                    WHEN additional = true THEN '" . ClinicianChoice::ADDITIONAL . "'
                    WHEN agreed = true THEN '" . ClinicianChoice::AGREED . "'
                    ELSE '" . ClinicianChoice::REFUSED . "'
                END"),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Nothing to do.
    }
}
