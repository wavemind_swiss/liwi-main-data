<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDiagnosisIdInCustomDrugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_drugs', function (Blueprint $table) {
            $table->integer('diagnosis_id')->nullable()->before('custom_diagnosis_id');
            $table->integer('custom_diagnosis_id')->nullable()->change();
        });

        Schema::table('drug_references', function (Blueprint $table) {
            $table->integer('custom_diagnosis_id')->nullable()->after('diagnosis_id');
            $table->integer('diagnosis_id')->nullable()->change();
        });

        DB::statement('ALTER TABLE custom_drugs ADD CONSTRAINT chk_custom_diagnosis_id_diagnosis_id CHECK ((custom_diagnosis_id IS NULL) <> (diagnosis_id IS NULL))');
        DB::statement('ALTER TABLE drug_references ADD CONSTRAINT chk_diagnosis_id_custom_diagnosis_id CHECK ((diagnosis_id IS NULL) <> (custom_diagnosis_id IS NULL))');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE custom_drugs DROP CONSTRAINT chk_custom_diagnosis_id_diagnosis_id');
        DB::statement('ALTER TABLE drug_references DROP CONSTRAINT chk_diagnosis_id_custom_diagnosis_id');

        Schema::table('custom_drugs', function (Blueprint $table) {
            $table->integer('custom_diagnosis_id')->nullable()->change();
            $table->dropColumn('diagnosis_id');
        });

        Schema::table('drug_references', function (Blueprint $table) {
            $table->integer('diagnosis_id')->nullable()->change();
            $table->dropColumn('custom_diagnosis_id');
        });
    }
}
