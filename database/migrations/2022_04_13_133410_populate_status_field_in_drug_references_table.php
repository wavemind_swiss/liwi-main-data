<?php

use App\Enums\ClinicianChoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class PopulateStatusFieldInDrugReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('drug_references')->update([
            'status' => DB::raw("CASE
                    WHEN additional = true THEN '" . ClinicianChoice::ADDITIONAL . "'
                    WHEN agreed = true THEN '" . ClinicianChoice::AGREED . "'
                    ELSE '" . ClinicianChoice::REFUSED . "'
                END"),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('drug_references')->update([
            'status' => null,
        ]);
    }
}
