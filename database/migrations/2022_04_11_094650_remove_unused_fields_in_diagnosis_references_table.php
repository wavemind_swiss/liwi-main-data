<?php

use App\Enums\ClinicianChoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RemoveUnusedFieldsInDiagnosisReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diagnosis_references', function (Blueprint $table) {
            $table->dropColumn([
                'agreed',
                'additional',
                'excluded',
            ]);

            $table->dropTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diagnosis_references', function (Blueprint $table) {
            $table->timestamps();

            $table->boolean('excluded')->after('updated_at')->nullable()->default(false);
            $table->boolean('agreed')->after('id')->nullable();
            $table->boolean('additional')->after('agreed')->nullable();
        });

        DB::table('diagnosis_references')->update([
            'excluded' => false,
            'agreed' => DB::raw("status IN ('" . ClinicianChoice::AGREED . "', '" . ClinicianChoice::ADDITIONAL . "')"),
            'additional' => DB::raw("status = '" . ClinicianChoice::ADDITIONAL . "'"),
        ]);
    }
}
