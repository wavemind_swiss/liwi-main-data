<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsDroppedToPatients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medical_cases', function (Blueprint $table) {
            $table->boolean('is_drop')->default(false);
        });
        Schema::table('patients', function (Blueprint $table) {
            $table->boolean('is_drop')->default(false);
        });
        Schema::table('health_facilities', function (Blueprint $table) {
            $table->boolean('is_drop')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medical_cases', function (Blueprint $table) {
            $table->dropColumn('is_drop');
        });
        Schema::table('patients', function (Blueprint $table) {
            $table->dropColumn('is_drop');
        });
        Schema::table('health_facilities', function (Blueprint $table) {
            $table->dropColumn('is_drop');
        });
    }
}
