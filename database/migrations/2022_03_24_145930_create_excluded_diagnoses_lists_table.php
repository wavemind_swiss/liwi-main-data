<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExcludedDiagnosesListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('excluded_diagnoses_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('medical_case_id')->unsigned()->unique();
            $table->foreign('medical_case_id')->references('id')->on('medical_cases');
            $table->jsonb('excluded_diagnoses')->default('[]');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('excluded_diagnoses_lists');
    }
}
